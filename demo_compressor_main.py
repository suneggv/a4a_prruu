# ==============================================================================
# ==  IMPORTS  =================================================================
# ==============================================================================

# ~~ module access from main ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
import sys
sys.path.append('./modules')


# ~~ debugging ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
from debugging import DEBUGGING # <-- this checks modules/debugging.py
if DEBUGGING:
    import os
    import types
    import numbers


# ~~ compression ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
import numpy as np
import math
from pydub import AudioSegment


# ~~ record / playback ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
import pyaudio
import time


# ~~ nparray_correct_byteorder(...) ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
import copy



# ==============================================================================
# ==  PARAMETERS  ==============================================================
# ==============================================================================

# ~~ diagnostic data ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

export_all = False # compress_mono


# ~~ pyaudio ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

chunk         = 1024            # Each chunk will consist of 1024 samples
sample_format = pyaudio.paInt16 # 16 bits per sample
channels      = 2               # Number of audio channels
fs            = 44100           # Record at 44100 samples per second

if DEBUGGING:
    assert type(chunk)         == int
    assert type(sample_format) == int
    assert type(channels)      == int
    assert type(fs)            == int


# ~~ compressor ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

B        = 1       # number of filterbank channels (scalar)
t_a      = 0.0001  # attack time                   (seconds)
t_r      = 0.3     # release time                  (seconds)
T_C_log  = 6.5     # compression threshold         (log)
T_L_log  = 8       # limiting threshold            (log)
R_b      = 2       # ratio per se of compression   (factor)
truncate = False

if DEBUGGING:
    assert type(      B)        == int
    assert type(      t_a)      == float
    assert type(      t_r)      == float
    assert isinstance(T_C_log,     numbers.Number)
    assert isinstance(T_L_log,     numbers.Number)
    assert            T_L_log    > T_C_log
    assert isinstance(R_b,         numbers.Number)
    assert            R_b       >= 0
    assert type(      truncate) == bool



# ==============================================================================
# ==  SUNK-LEVEL FUNCTIONS  ====================================================
# ==============================================================================

# ~~ sunk-level --> math operations ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

from compressor_sunk_level_functions import retrieve_max_audio_value
from compressor_sunk_level_functions import log_v_y_t
from compressor_sunk_level_functions import detect_level_mono
from compressor_sunk_level_functions import get_nparray_log_from_nparray
from compressor_sunk_level_functions import normalize_nparray_mono
from compressor_sunk_level_functions import compress_sample
from compressor_sunk_level_functions import fade_in_nparray_mono


# ~~ sunk-level --> converters ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

from compressor_sunk_level_functions import nparray_LR_2_nparrays_L_and_R
from compressor_sunk_level_functions import nparray_back_2_sample_format
from compressor_sunk_level_functions import nparray_correct_byteorder



# ==============================================================================
# ==  MID-LEVEL FUNCTIONS  =====================================================
# ==============================================================================

# ~~ mid-level --> converters ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

from compressor_mid_level_functions import bytes_stereo_2_nparray_mono
from compressor_mid_level_functions import nparray_mono_2_bytearray_mono


# ~~ mid-level --> audio procedures ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

from compressor_mid_level_functions import level_detector_nparray_mono
from compressor_mid_level_functions import compress_audio_nparray_mono



# ==============================================================================
# ==  HIGH-LEVEL FUNCTIONS  ====================================================
# ==============================================================================

# ~~ high-level --> turn non-DRC'd frames into yes-DRC'd frames ~~~~~~~~~~~~~~~~

from compressor_high_level_functions import compress_mono


# ~~ high-level --> pyaudio callback (thr 1 in --> comp / thr 2 comp --> out) ~~

from compressor_high_level_functions import callback_in_DRC_queue
from compressor_high_level_functions import callback_queue_out
from compressor_high_level_functions import pyaudio_DRC_stream_manager



# ==============================================================================
# ==  MAIN PROCEDURE  ==========================================================
# ==============================================================================

pyaudio_DRC_stream_manager( chunk,
                            sample_format,
                            channels,
                            fs,
                            B,
                            t_a,
                            t_r,
                            T_C_log,
                            T_L_log,
                            R_b,
                            truncate,
                            export_all
                          )
