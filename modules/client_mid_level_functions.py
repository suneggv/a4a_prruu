from debugging import DEBUGGING

import random
import pyaudio
import threading
if DEBUGGING:
    import types
    import os



# ==============================================================================
# ==  DIAGNOSTIC FUNCTIONS  ====================================================
# ==============================================================================

from diagnostic_functions import assert_fo_in
from diagnostic_functions import assert_fo_out
from diagnostic_functions import PrintException



# ==============================================================================
# ==  MID-LEVEL FUNCTIONS  =====================================================
# ==============================================================================

def get_header_header_sz( data_pad_str, 
                          timestamp,
                          sample_format        = pyaudio.paInt16, 
                          channels             = 2,
                          after_timestamp_char = '&',
                          data_header_end_str  = '__'
                        ):
    if DEBUGGING:
        assert type(data_pad_str)         == str
        assert len( data_pad_str)         == 8
        assert type(timestamp)            == int
        assert type(sample_format)        == int
        assert type(channels)             == int
        assert type(after_timestamp_char) == str
        assert len( after_timestamp_char) == 1
        assert type(data_header_end_str)  == str
        assert len( data_header_end_str)  == 2

    sample_width        = pyaudio.get_sample_size(sample_format) # byte width
    multiple            = channels * sample_width
    after_timestamp_str = after_timestamp_char * multiple
    header_sz_draft     = 10 + len(str(timestamp))
    after_timestamp_sz  = (multiple - header_sz_draft % multiple) % multiple
    header_sz           = header_sz_draft + after_timestamp_sz

    header = bytes(data_pad_str,                             'utf-8') + \
             bytes(str(timestamp),                           'utf-8') + \
             bytes(after_timestamp_str[:after_timestamp_sz], 'utf-8') + \
             bytes(data_header_end_str,                      'utf-8')

    out = (header, header_sz)
    if DEBUGGING:
        assert type(out)         == tuple
        assert type(out[0])      == bytes # header
        assert type(out[1])      == int   # header_sz
        assert out[1] % multiple == 0
    return out


def get_header_header_sz_from_datagram( datagram,
                                        data_header_end_str = '__'
                                      ): # hdr format --> __okay__TIMESTAMP&__
    if DEBUGGING:
        assert type(datagram)                == bytes
        assert len( datagram[8:])             > 0
        assert datagram[0:2].decode('utf-8') == '__'
        assert datagram[6:8].decode('utf-8') == '__'
        assert type(data_header_end_str)     == str
        assert len( data_header_end_str)     == 2

    for i in range(8,len(datagram)):
        if datagram[i:(i+1)].decode('utf-8') == data_header_end_str[0]:

            header_sz = i + 2 # i == len('__okay__TIMESTAMP'), just missing '__'
            out = (datagram[:header_sz], header_sz)
            if DEBUGGING:
                assert type(out)                    == tuple
                assert type(out[0])                 == bytes
                assert type(out[0].decode('utf-8')) == str
                assert type(out[1])                 == int
                assert len( out[0])                 == out[1]
            return out
    raise Exception("get_header_header_sz_from_datagram(...) failed" + \
                    " to find header (how did you even get here???)")


def get_header_sz_from_timestamp( timestamp,
                                  sample_format = pyaudio.paInt16, 
                                  channels      = 2
                                ): # header format --> smth__TIMESTAMP&&__
    if DEBUGGING:
        assert type(timestamp)     == int
        assert type(sample_format) == int
        assert type(channels)      == int

    sample_width       = pyaudio.get_sample_size(sample_format) # byte width
    multiple           = sample_width * channels
    header_sz_draft    = 10 + len(str(timestamp))
    after_timestamp_sz = (multiple - header_sz_draft % multiple) % multiple
    header_sz          = header_sz_draft + after_timestamp_sz

    if DEBUGGING:
        assert type(header_sz)      == int
        assert header_sz % multiple == 0
    return header_sz


def get_msg_sz_from_timestamp( datagram_sz, 
                               timestamp,
                               sample_format = pyaudio.paInt16, 
                               channels      = 2
                             ):
    if DEBUGGING:
        assert type(datagram_sz)   == int
        assert type(timestamp)     == int
        assert type(sample_format) == int
        assert type(channels)      == int

    msg_sz = datagram_sz - get_header_sz_from_timestamp(timestamp,
                                                        sample_format, 
                                                        channels)
    if DEBUGGING:
        assert type(msg_sz) == int
    return msg_sz


def get_timestamp_from_header( header,
                               after_timestamp_char = '&',
                               data_header_end_str  = '__'
                             ): # header format: __smth__TIMESTAMP&&&__
    if DEBUGGING:
        assert type(header)               == bytes
        assert type(after_timestamp_char) == str
        assert len( after_timestamp_char) == 1
        assert type(data_header_end_str)  == str
        assert len( data_header_end_str)  == 2

    for r in range(8, len(header)):
        next_char = header[r:r+1].decode('utf-8')

        if ( next_char == after_timestamp_char   or 
             next_char == data_header_end_str[0] ):

            timestamp_str = header[8:r].decode('utf_8')

            timestamp = int(timestamp_str)
            if DEBUGGING:
                assert type(timestamp) == int
                assert timestamp       >= 0
            return timestamp

    raise Exception("get_timestamp_from_header(...) failed to find timestamp")


# ~~ downloading ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

def retrieve_pidges_entry( pidges, 
                           timestamp, 
                           n_msgs_per_pidge
                         ):
    if DEBUGGING:
        assert type(pidges)           == list
        assert len( pidges)            > 0
        assert type(timestamp)        == int
        assert type(n_msgs_per_pidge) == int

    entry_timestamp = timestamp - timestamp % n_msgs_per_pidge
    timestamp_pidge = [tup for tup in pidges if tup[0] == entry_timestamp][0]
    idx = [i for i, val in enumerate(pidges) if val == timestamp_pidge][0]
    
    out = (idx, timestamp_pidge[1])
    if DEBUGGING:
        assert type(out)    == tuple
        assert type(out[0]) == int       # idx
        assert type(out[1]) == bytearray # pidge
        assert len( out[1])  > 0
    return out


def make_zeropad_pidges_entry( n_msgs_per_pidge, 
                               msg_sz, 
                               lowest_timestamp
                             ):
    if DEBUGGING:
        assert type(n_msgs_per_pidge) == int
        assert type(msg_sz)           == int
        assert type(lowest_timestamp) == int

    zp_pidges_entry = (lowest_timestamp, bytearray(msg_sz * n_msgs_per_pidge))
    if DEBUGGING:
        assert type(zp_pidges_entry)    == tuple
        assert type(zp_pidges_entry[0]) == int       # lowest_timestamp
        assert type(zp_pidges_entry[1]) == bytearray # zeropadded pidge
        assert len( zp_pidges_entry[1]) == msg_sz * n_msgs_per_pidge
        assert sum( zp_pidges_entry[1]) == 0
    return zp_pidges_entry


def make_zeropad_pidges( n_msgs_per_pidge,
                         n_entries_per_pidges,
                         datagram_sz,
                         sample_format, 
                         channels
                       ):
    if DEBUGGING:
        assert type(n_msgs_per_pidge)     == int
        assert type(n_entries_per_pidges) == int
        assert type(datagram_sz)          == int
        assert type(sample_format)        == int
        assert type(channels)             == int

    pidges = []
    for i in range(n_entries_per_pidges):
        lowest_timestamp = i * n_msgs_per_pidge
        msg_sz = get_msg_sz_from_timestamp(datagram_sz, 
                                           lowest_timestamp,
                                           sample_format, 
                                           channels
                                          )
        pidges.append( make_zeropad_pidges_entry( n_msgs_per_pidge, 
                                                  msg_sz, 
                                                  lowest_timestamp
                                                )
                     )
    if DEBUGGING:
        assert type(pidges) == list
        assert len( pidges) == n_entries_per_pidges
        assert type(pidges[0]) == tuple
        assert type(pidges[0][0]) == int
        assert type(pidges[0][1]) == bytearray
        assert len( pidges[-1][1])== n_msgs_per_pidge * msg_sz
        assert sum( pidges[0][1]) == 0
    return pidges


def update_pidges( pidges, 
                   lowest_timestamp, 
                   n_msgs_per_pidge,
                   datagram_sz,
                   sample_format, 
                   channels
                 ):
    if DEBUGGING:
        assert type(pidges)           == list
        assert len( pidges)            > 0
        assert type(pidges[0])        == tuple     # pidges entry
        assert type(pidges[0][0])     == int       # lowest_timestamp
        assert type(pidges[0][1])     == bytearray # pidge
        assert type(lowest_timestamp) == int
        assert type(n_msgs_per_pidge) == int
        assert type(datagram_sz)      == int
        assert type(sample_format)    == int
        assert type(channels)         == int

    msg_sz = get_msg_sz_from_timestamp(datagram_sz, 
                                       lowest_timestamp,
                                       sample_format, 
                                       channels
                                      )
    timestamp_pidge = make_zeropad_pidges_entry( n_msgs_per_pidge, 
                                                 msg_sz, 
                                                 lowest_timestamp
                                               )
    pidges.append(timestamp_pidge)


def edit_pidge( pidges, 
                idx_pidge, 
                start_position,
                end_position,
                msg 
              ):
    if DEBUGGING:
        assert type(pidges)         == list
        assert len( pidges)          > 0
        assert type(pidges[0])      == tuple     # pidges entry
        assert type(pidges[0][0])   == int       # lowest_timestamp
        assert type(pidges[0][1])   == bytearray # pidge
        assert type(idx_pidge)      == int
        assert type(start_position) == int
        assert type(end_position)   == int
        assert type(msg)            == bytes
        assert len( msg)             > 0

    i_c = idx_pidge
    s   = start_position
    e   = end_position
    pidges[i_c][1][s:e] = msg
    #      |    |  |
    #      |    |  select range inside pidge corresp. to data
    #      |    pidges entry --> (low_tmstp, pidge) 
    #      select correct entry in pidges list


# ~~ dumping pidges ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

def retrieve_last_nonempty_pidges_entry( pidges
                                       ):
    if DEBUGGING:
        assert type(pidges) == list
        assert len( pidges)  > 0

    idx = [i for i in range(len(pidges)) if sum(pidges[i][1]) != 0][-1]
    
    out = (idx, pidges[idx])
    if DEBUGGING:
        assert type(out)       == tuple
        assert type(out[0])    == int
        assert type(out[1])    == tuple     # pidges entry
        assert type(out[1][0]) == int       # lowest_timestamp
        assert type(out[1][1]) == bytearray # pidge
        assert len( out[1][1])  > 0
        assert sum( out[1][1]) != 0
    return out


def retrieve_data_from_last_pidge( pidge 
                                 ):
    if DEBUGGING:
        assert type(pidge) == bytearray
        assert len( pidge)  > 0

    idx_R = [i for i in range(len(pidge)) if pidge[i] != 0][-1] + 1
    
    out = pidge[:idx_R]
    if DEBUGGING:
        assert type(out) == bytearray
        assert len( out)  > 0
    return out


# ~~ scrambling ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

def make_scrambled_timestamp_sequence( n_datagrams
                                     ):
    if DEBUGGING:
        assert type(n_datagrams) == int
        assert n_datagrams        > 0

    integers   = [*range(161,767)] # 161-767: stretch of predictable utf-8 chars 
    timestamps = []
    while ( len(timestamps) < n_datagrams and
            len(integers)   > 0 ):
        timestamps.append( integers.pop( random.randint(0, len(integers)-1) ) )

    if DEBUGGING:
        assert type(timestamps)    == list
        assert ( len( timestamps)  == n_datagrams or
                 len( timestamps)  == len( [*range(161,767)] ) )
        assert type(timestamps[0]) == int
    return timestamps


def get_expected_timestamps_s2c( timestamps_c2s,
                                 n_msgs_per_pidge,
                                 n_entries_per_pidges
                               ):
    if DEBUGGING:
        assert type(timestamps_c2s)       == list     
        assert len( timestamps_c2s)        > 0
        assert type(timestamps_c2s[0])    == int
        assert type(n_msgs_per_pidge)     == int
        assert type(n_entries_per_pidges) == int

    timestamps_s2c_expected = []
    
    minimum_timestamp       = 0
    largest_timestamp       = (n_msgs_per_pidge * n_entries_per_pidges) - 1

    for i in range(len(timestamps_c2s)):

        if timestamps_c2s[i] >= minimum_timestamp:

            while timestamps_c2s[i] > largest_timestamp:
                minimum_timestamp += n_msgs_per_pidge
                largest_timestamp += n_msgs_per_pidge

            timestamps_s2c_expected.append(timestamps_c2s[i])

    timestamps_s2c_expected.sort()

    if DEBUGGING:
        assert type(timestamps_s2c_expected) == list
        assert len( timestamps_s2c_expected)  > 0
        assert len( timestamps_s2c_expected) <= len(timestamps_c2s)
    return timestamps_s2c_expected


def write_msg_line_for_scrambled_file( char,
                                       char_bytesize,
                                       line_sz = 72
                                     ):
    if DEBUGGING:
        assert type(char)          == str
        assert len( char)          == 1
        assert type(char_bytesize) == int
        assert type(line_sz)       == int
        assert      line_sz        >= 0

    line = ''

    if line_sz > 0:
        line_budget  = line_sz
        line_budget -= len('\n')
        line_pad_sz  = line_budget % char_bytesize
        line_budget -= line_pad_sz

        n_chars      = int(line_budget / char_bytesize)

        line_chars   = char * n_chars
        line_end     = ('\t' * line_pad_sz) + '\n'

        line = line_chars + line_end

    if DEBUGGING:
        assert type(line)                == str
        assert len(bytes(line, 'utf-8')) == line_sz
        if line_sz > 0:
            assert line[-1] == '\n'

        if line_sz > char_bytesize:
            assert line[0]  == char
            # there can be a region with padding, but it's not necessary

    return line


def write_msg_for_scrambled_file( msg_sz, 
                                  timestamp,
                                  stop_phrase = "__msg_end__"
                                ):
    if DEBUGGING:
        assert type(msg_sz)      == int
        assert type(timestamp)   == int
        assert type(stop_phrase) == str
        assert      msg_sz        > len(stop_phrase) + 1

    char           = chr(timestamp)
    char_bytesize  = len(bytes(char,'utf-8'))

    msg_budget     = msg_sz
    msg_budget    -= len(stop_phrase)
    msg_budget    -= len('\n')

    line_sz        = 72
    if DEBUGGING:
        assert msg_sz > line_sz

    full_line      = write_msg_line_for_scrambled_file(char, 
                                                       char_bytesize, 
                                                       line_sz)
    n_full_lines   = int(msg_budget / line_sz)
    msg_full_lines = full_line * n_full_lines

    msg_budget    -= len(bytes(msg_full_lines, 'utf-8'))
    msg_remainder  = write_msg_line_for_scrambled_file(char, 
                                                       char_bytesize, 
                                                       msg_budget)
    msg_end        = stop_phrase + '\n'

    msg = msg_full_lines + msg_remainder + msg_end
    if DEBUGGING:
        assert type(     msg)                        == str
        assert len(bytes(msg, 'utf-8'))              == msg_sz
        assert           msg[0]                      == chr(timestamp)
        assert           msg[-(len(stop_phrase)+1):] == stop_phrase + '\n'
    return msg


def write_scrambled_file( timestamps,
                          datagram_sz,
                          file_name     = "scrambled.txt",
                          stop_phrase   = "__msg_end__",
                          sample_format = pyaudio.paInt16,
                          channels      = 2
                        ):
    if DEBUGGING:
        assert type(timestamps)    == list
        assert type(timestamps[0]) == int
        assert type(datagram_sz)   == int
        assert type(file_name)     == str
        assert type(stop_phrase)   == str
        assert type(sample_format) == int
        assert type(channels)      == int

    msg_szs = None
    if DEBUGGING:
        msg_szs = 0

    fo_del = open(file_name, 'w')
    fo_del.write("")
    fo_del.close()

    fo_out = open(file_name, 'ab')

    for i in range(len(timestamps)):

        msg_sz = datagram_sz - get_header_sz_from_timestamp(timestamps[i],
                                                            sample_format,
                                                            channels
                                                           )
        if DEBUGGING:
            msg_szs += msg_sz

        msg = write_msg_for_scrambled_file(msg_sz, 
                                           timestamps[i], 
                                           stop_phrase
                                          )
        fo_out.write(bytes(msg,'utf-8'))

    fo_out.close()

    if DEBUGGING:
        assert os.path.getsize(file_name) == msg_szs


def schrodingers_file_parser( timestamps_s2c,
                              parser_stop,
                              file_name_s2c,
                              stop_phrase = "__msg_end__"
                            ):
    # This parses the file produced by the server line by line after scrambling.
    # In my attempts to make it stop assuming the file is finished before it is,
    # I managed to make the function keep reading lines indefinitely. Thing is,
    # without that, for some reason there are often long stretches of lines that
    # indicate that the EOF has been reached when it hasn't, so I can't change
    # that. The way this function works is that it reads the file, thinks it's
    # over a bunch of times, ignores that, keeps reading the file, freezes, and
    # then if it receives a request to stop it successfully gives the correct
    # result. IT WORKS. DO NOT TOUCH.
    if DEBUGGING:
        assert type(timestamps_s2c) == list
        assert len( timestamps_s2c) == 0
        assert type(parser_stop)    == threading.Event
        assert type(file_name_s2c)  == str
        assert type(stop_phrase)    == str

    added_timestamp = False

    fo_s2c    = open(file_name_s2c, 'r')
    curr_line = fo_s2c.readline()

    if len(curr_line) > 0:
        curr_char          = curr_line[0]
        curr_timestamp_s2c = ord(curr_char)
        timestamps_s2c.append(curr_timestamp_s2c)

        added_timestamp = True

    while not parser_stop.wait(0):
        if len(curr_line) > 0:

            if not added_timestamp:
                curr_char          = curr_line[0]
                curr_timestamp_s2c = ord(curr_char)
                timestamps_s2c.append(curr_timestamp_s2c)

                added_timestamp = True

            if curr_line[:len(stop_phrase)] == stop_phrase:
                added_timestamp = False

        curr_line = fo_s2c.readline()

    fo_s2c.close()

    if DEBUGGING:
        assert type(timestamps_s2c)    == list
        assert len( timestamps_s2c)     > 0
        assert type(timestamps_s2c[0]) == int
    return timestamps_s2c


# ~~ pyaudio ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

def client_callback_out( frames
                       ):
    if DEBUGGING:
        assert type(frames) == bytearray

    def callback( in_data,     # recorded data
                  frame_count, # number of frames
                  time_info,   # dictionary
                  status       # PaCallbackFlags
                ):
        if DEBUGGING:
            assert type(in_data)     == bytes
            assert type(frame_count) == int
            assert type(time_info)   == dict
            assert type(status)      == int

        frames.extend(in_data)

        out = (in_data, pyaudio.paContinue)
        if DEBUGGING:
            assert type(out)    == tuple
            assert type(out[0]) == bytes
            assert type(out[1]) == int
        return out

    if DEBUGGING:
        assert type(callback) == types.FunctionType
    return callback


def client_callback_in( playback_queue,
                        chunk,
                        channels,
                        sample_format
                      ):
    if DEBUGGING:
        assert type(playback_queue) == bytearray
        assert type(chunk)          == int
        assert type(channels)       == int
        assert type(sample_format)  == int

    def callback( in_data,     # recorded data (ie this is None bc input=False)
                  frame_count, # number of frames
                  time_info,   # dictionary
                  status       # PaCallbackFlags
                ):
        if DEBUGGING:
            assert type(in_data)     == types.NoneType
            assert type(frame_count) == int
            assert type(time_info)   == dict
            assert type(status)      == int

        out_data = bytearray()
        sz = chunk * channels * pyaudio.get_sample_size(sample_format)

        if len(playback_queue) >= sz:
            out_data = playback_queue[:sz]
            del playback_queue[:sz]

        else:
            out_data = bytes(sz)

        out = (bytes(out_data), pyaudio.paContinue)
        if DEBUGGING:
            assert type(out)    == tuple
            assert type(out[0]) == bytes
            assert type(out[1]) == int
        return out

    if DEBUGGING:
        assert type(callback) == types.FunctionType
    return callback


def remove_holes_from_timestamp_pidge( timestamp_pidge,
                                       datagram_sz, 
                                       n_msgs_per_pidge,
                                       sample_format = pyaudio.paInt16, 
                                       channels      = 2
                                     ):
    if DEBUGGING:
        assert type(timestamp_pidge)    == tuple
        assert type(timestamp_pidge[0]) == int       # lowest_timestamp
        assert type(timestamp_pidge[1]) == bytearray # pidge
        assert type(datagram_sz)        == int
        assert type(n_msgs_per_pidge)   == int
        assert type(sample_format)      == int
        assert type(channels)           == int

    timestamp, pidge = timestamp_pidge
    msg_sz = get_msg_sz_from_timestamp(datagram_sz, 
                                       timestamp,
                                       sample_format,
                                       channels
                                      )
    out = bytearray()

    for i in range(n_msgs_per_pidge):
        l = i     * msg_sz
        r = (i+1) * msg_sz
        if sum(pidge[l:r]) != 0:
            out.extend(pidge[l:r])

    if DEBUGGING:
        assert type(out) == bytearray
        # out can be an empty bytearray
    return out





















