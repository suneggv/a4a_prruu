# ==============================================================================
# ==  IMPORTS  =================================================================
# ==============================================================================

# ~~ debugging ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
from debugging import DEBUGGING
if DEBUGGING:
    import os
    import types
    import numbers


# ~~ compression ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
import numpy as np
import math
from pydub import AudioSegment


# ~~ record / playback ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
import pyaudio
import time


# ~~ nparray_correct_byteorder(...) ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
import sys
import copy



# ==============================================================================
# ==  SUNK-LEVEL FUNCTIONS  ====================================================
# ==============================================================================

# ~~ sunk-level --> math operations ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

def retrieve_max_audio_value( sample_format = pyaudio.paInt16 
                            ):
    if DEBUGGING:
        assert type(sample_format) == int

    sample_width_bytes = pyaudio.get_sample_size(sample_format)
    sample_width_bits  = sample_width_bytes * 8
    max_audio_value    = math.pow(2, (sample_width_bits-1) ) - 1

    if DEBUGGING:
        assert isinstance(max_audio_value,       numbers.Number)
        assert type(      sample_width_bytes) == int
        assert type(      sample_width_bits)  == int

    return (max_audio_value, sample_width_bytes, sample_width_bits)


def log_v_y_t( v_x_log_t, 
               T_C_log = 8, 
               T_L_log = 8.75, 
               R_b     = 6
             ):
    if DEBUGGING:
        assert     isinstance(     v_x_log_t,  numbers.Number)
        assert not issubclass(type(v_x_log_t), np.integer)
        assert     isinstance(     T_C_log,    numbers.Number)
        assert     isinstance(     T_L_log,    numbers.Number)
        assert     isinstance(     R_b,        numbers.Number)
        assert                     R_b      >= 0

    v_y_log_t = None
    if v_x_log_t < T_C_log: 
        v_y_log_t = v_x_log_t

    elif v_x_log_t < T_L_log:
        v_y_log_t = T_C_log + (v_x_log_t - T_C_log)/R_b

    else: # v_x_log_t > T_L_log
        v_y_log_t = T_C_log + (T_L_log - T_C_log)/R_b

    if DEBUGGING:
        assert type(v_y_log_t) == type(v_x_log_t)
    return v_y_log_t 


def detect_level_mono( x_now, # x_t_b[t]
                       v_old, # v_x_t_b[t-1]
                       beta_a,
                       beta_r,
                       T_C_log
                     ):
    if DEBUGGING:
        assert     isinstance(     x_now,     numbers.Number)
        assert not issubclass(type(x_now),    np.integer)
        assert     isinstance(     v_old,     numbers.Number)
        assert not issubclass(type(v_old),    np.integer)
        assert                     v_old    > 0
        assert     type(           beta_a) == float
        assert                     beta_a   > 0
        assert     type(           beta_r) == float
        assert                     beta_r   > 0
        assert     isinstance(     T_C_log,   numbers.Number)

    v_now = None
    if math.pow(x_now,2) >= v_old:
        v_now = beta_a*v_old + (1-beta_a)*math.pow(x_now,2)
    else:
        v_now = beta_r*v_old + (1-beta_r)*math.pow(x_now,2)    

    if DEBUGGING:
        assert type(v_now) == type(v_old)
        assert      v_now   > 0
    return v_now


def get_nparray_log_from_nparray( nparray 
                                ):
    if DEBUGGING:
        assert type(           nparray)           == np.ndarray
        assert len(            nparray)            > 0
        assert issubclass(type(nparray[0]),          np.floating)
        assert len([i for i in nparray if i > 0]) == len(nparray)

    nparray_log = np.zeros(len(nparray)).astype('float64')
    
    for t in range(1,len(nparray)):
        nparray_log[t] = math.log10(nparray[t])

    if DEBUGGING:
        assert type(nparray_log)    == np.ndarray
        assert len( nparray_log)    == len(nparray)
        assert type(nparray_log[0]) == type(nparray[0])
    return nparray_log


def get_max_compressed_value( T_C_log
                            ):
    if DEBUGGING:
        assert isinstance(T_C_log, numbers.Number)

    y_max_eff = int(math.pow(10, T_C_log / 2))
    if DEBUGGING:
        assert type(y_max_eff) == int
        assert      y_max_eff   > 0
    return y_max_eff


def normalize_nparray_mono ( nparray,
                             T_C_log,
                             sample_format = pyaudio.paInt16 
                           ):
    if DEBUGGING:
        assert type(      nparray)       == np.ndarray
        assert len(       nparray)        > 0
        assert isinstance(nparray[0],       numbers.Number)
        assert isinstance(T_C_log,          numbers.Number)
        assert type(      sample_format) == int

    max_audio_value, _, _ = retrieve_max_audio_value( sample_format )
    max_compressed_value  = get_max_compressed_value( T_C_log )

    out = nparray / max_compressed_value * max_audio_value
    if DEBUGGING:
        assert type(out)    == np.ndarray
        assert len( out)    == len(nparray)
        assert type(out[0]) == type(nparray[0])
    return out

def brickwall_factor( val,
                      max_audio_value
                    ):
    if DEBUGGING:
        assert isinstance(val,                numbers.Number)
        assert isinstance(max_audio_value,    numbers.Number)
        assert            max_audio_value   > 0

    tan_x_top = math.pi * val
    tan_x_bot = 4 * max_audio_value
    tan_x     = math.tan(tan_x_top / tan_x_bot)

    top = 0.5
    bot = 1.0 + math.pow(tan_x,2)

    factor = 0.5 + (top / bot)
    if DEBUGGING:
        assert     isinstance(     factor,  numbers.Number)
        assert not issubclass(type(factor), np.integer)
    return factor


def brickwall_nparray_mono( nparray,
                            sample_format = pyaudio.paInt16
                          ):
    if DEBUGGING:
        assert type(      nparray)       == np.ndarray
        assert len(       nparray)        > 0
        assert isinstance(nparray[0],       numbers.Number)
        assert type(      sample_format) == int

    max_val, _, _ = retrieve_max_audio_value(sample_format)

    nparray_out = copy.deepcopy(nparray)
    for i in range(len(nparray_out)):
        nparray_out[i] = nparray[i] * brickwall_factor(nparray[i], max_val)

    if DEBUGGING:
        assert type(nparray_out) == type(nparray)
    return nparray_out


def compress_sample( x, # x_t_b[t]
                     v_x, # v_x_t_b[t]
                     T_C_log,
                     T_L_log, 
                     R_b
                   ):
    if DEBUGGING:
        assert     isinstance(     x,        numbers.Number)
        assert not issubclass(type(x),       np.integer)
        assert     isinstance(     v_x,      numbers.Number)
        assert not issubclass(type(v_x),     np.integer)
        assert                     v_x     > 0
        assert     isinstance(     T_C_log,  numbers.Number)
        assert     isinstance(     T_L_log,  numbers.Number)
        assert                     T_L_log > T_C_log
        assert     isinstance(     R_b,      numbers.Number)
        assert                     R_b    >= 0

    log_v_y      = log_v_y_t( math.log10(v_x), T_C_log, T_L_log, R_b)
    #P           = (log_v_y - 1.0) / 2.0 # <-- FIND OUT WHY THIS EXPRESSION
    # y_to_x_ratio = math.pow(10,P) / v_x
    y_to_x_ratio = math.pow(10,log_v_y) / v_x
    y            = y_to_x_ratio * x 

    if DEBUGGING:
        assert type(y) == type(x)
    return y # y_t_b[t]


def fade_in_nparray_mono( nparray,
                          width,
                          power = 1
                        ):
    if DEBUGGING:
        assert     type(           nparray)  == np.ndarray
        assert     type(           width)    == int
        assert     len(            nparray)   > 0
        assert     isinstance(     nparray[0],  numbers.Number)
        assert not issubclass(type(nparray[0]), np.integer)
        assert     isinstance(     power,       numbers.Number)
        assert                     power      > 0

    if width > len(nparray):
        width = len(nparray)

    fade_factors = np.power(np.linspace(0, width, width) / width, power)
    ones_factors = np.ones(len(nparray)-width)
    all_factors  = np.concatenate((fade_factors, ones_factors))

    out = nparray * all_factors
    if DEBUGGING:
        assert type(out)    == np.ndarray
        assert len( out)    == len(nparray)
        assert type(out[0]) == type(nparray[0])
    return out


# ~~ sunk-level --> converters ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

def nparray_LR_2_nparrays_L_and_R( x_t_LR
                                 ):
    if DEBUGGING:
        assert type(      x_t_LR)     == np.ndarray
        assert len(       x_t_LR)      > 1
        assert len(       x_t_LR) % 2 == 0
        assert isinstance(x_t_LR[0],     numbers.Number)

    x_t_L_idx = [i for i in range(len(x_t_LR)) if i % 2 == 0] # L channel idxes
    x_t_R_idx = [i for i in range(len(x_t_LR)) if i % 2 == 1] # R channel idxes

    x_t_L = x_t_LR[x_t_L_idx]
    x_t_R = x_t_LR[x_t_R_idx]
    
    if DEBUGGING:
        assert type(      x_t_L)    == np.ndarray
        assert len(       x_t_L)    == len(x_t_LR) / 2
        assert isinstance(x_t_L[0],    numbers.Number)
        assert            x_t_L[0]  == x_t_LR[0]
        assert type(      x_t_R)    == np.ndarray
        assert len(       x_t_R)    == len(x_t_LR) / 2
        assert isinstance(x_t_R[0],    numbers.Number)
        assert            x_t_R[0]  == x_t_LR[1]
    return (x_t_L, x_t_R)


def nparray_back_2_sample_format( nparray,
                                  sample_format = pyaudio.paInt16
                                ):
    if DEBUGGING:
        assert type(      nparray)       == np.ndarray
        assert len(       nparray)        > 0
        assert isinstance(nparray[0],       numbers.Number)
        assert type(      sample_format) == int

    type_str = None
    if   sample_format == pyaudio.paCustomFormat:
        raise Exception("Custom sample formats not handled yet")

    elif sample_format == pyaudio.paUInt8:
        raise Exception("Sample format UInt8 not handled yet")

    elif sample_format == pyaudio.paFloat32:
        type_str = 'float'

    else:
        type_str = 'int'

    _, _, sample_width_bits = retrieve_max_audio_value(sample_format)
    sample_width_bits_str   = str(sample_width_bits)

    format_str = type_str + sample_width_bits_str

    nparray_int = nparray.astype(format_str)
    if DEBUGGING:
        assert type(nparray_int)               == np.ndarray
        assert len( nparray_int)               == len(nparray)
        assert      nparray_int.dtype.itemsize == np.dtype(format_str).itemsize
    return nparray_int


def nparray_correct_byteorder( nparray 
                             ):
    if DEBUGGING:
        assert type(      nparray)    == np.ndarray
        assert len(       nparray)     > 0
        assert isinstance(nparray[0],    numbers.Number)

    out = np.array([])
    if ( nparray.dtype.byteorder == '>' or 
        (nparray.dtype.byteorder == '=' and sys.byteorder == 'big')
       ):
        out = nparray.byteswap()
    else:
        out = copy.deepcopy(nparray)

    if DEBUGGING:
        assert type(      out)     == np.ndarray
        assert len(       out)     == len(nparray)
        assert isinstance(out[0],     numbers.Number)
    return out


def nparray_mono_2_nparray_stereo ( nparray_mono
                                  ):
    if DEBUGGING:
        assert type(      nparray_mono)    == np.ndarray
        assert len(       nparray_mono)     > 0
        assert isinstance(nparray_mono[0],    numbers.Number)

    nparray_stereo = np.stack((nparray_mono, nparray_mono), axis = 1).flatten()

    if DEBUGGING:
        assert type(      nparray_stereo)   == np.ndarray
        assert len(       nparray_stereo)   == 2 * len(nparray_mono)
        assert isinstance(nparray_stereo[0],   numbers.Number)
        assert            nparray_stereo[0] == nparray_mono[0] # L
        assert            nparray_stereo[1] == nparray_mono[0] # R (== L)
    return nparray_stereo
