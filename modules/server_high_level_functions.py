from debugging import DEBUGGING # <-- this checks modules/debugging.py
from server_compressor_imports import *

import socket
import subprocess
import os
import time
from datetime import datetime
import pyaudio



# ==============================================================================
# ==  DIAGNOSTIC FUNCTIONS  ====================================================
# ==============================================================================

from diagnostic_functions import assert_fo_in
from diagnostic_functions import assert_fo_out
from diagnostic_functions import PrintException



# ==============================================================================
# ==  MID-LEVEL FUNCTIONS  =====================================================
# ==============================================================================

# ~~ management of uploads from several simultaneous clients ~~~~~~~~~~~~~~~~~~~

from server_mid_level_functions import retrieve_fo_out
from server_mid_level_functions import delete_addr_fo_out_entry_at_idx


# ~~ syncing uploaded data server-side --> headers ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

from server_mid_level_functions import get_header_header_sz
from server_mid_level_functions import get_header_header_sz_from_datagram
from server_mid_level_functions import get_header_sz_from_timestamp
from server_mid_level_functions import get_msg_sz_from_timestamp
from server_mid_level_functions import get_timestamp_from_header


# ~~ syncing uploaded data server-side --> pidges list management ~~~~~~~~~~~~~~

from server_mid_level_functions import make_zeropad_pidges_entry
from server_mid_level_functions import add_zeropad_pidges_addr_tuple_to_list
from server_mid_level_functions import retrieve_pidges
from server_mid_level_functions import retrieve_pidges_entry
from server_mid_level_functions import edit_pidge
from server_mid_level_functions import delete_pidges_addr_tuple_from_list


# ~~ syncing uploaded data server-side --> largest timestamp management ~~~~~~~~

from server_mid_level_functions import add_largest_timestamp_addr_tuple_to_list
from server_mid_level_functions import retrieve_largest_timestamp
from server_mid_level_functions import update_largest_timestamp
from server_mid_level_functions import delete_l_tstp_addr_sublist_from_list


# ~~ dumping pidges ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

from server_mid_level_functions import retrieve_last_nonempty_pidges_entry
from server_mid_level_functions import retrieve_data_from_last_pidge


# ~~ pyaudio -- wire audio ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

from server_mid_level_functions import server_wire_callback

from server_mid_level_functions import wire_add_stream_addr_tuple_to_list
from server_mid_level_functions import retrieve_stream
from server_mid_level_functions import start_the_stream
from server_mid_level_functions import remove_holes_from_timestamp_pidge

from server_mid_level_functions import add_filled_addr_pair_to_list
from server_mid_level_functions import retrieve_filled_addr
from server_mid_level_functions import toggle_filled
from server_mid_level_functions import these_are_filled
from server_mid_level_functions import delete_filled_addr_pair_from_list

from server_mid_level_functions import add_comp_queue_addr_tuple_to_list
from server_mid_level_functions import retrieve_compress_queue
from server_mid_level_functions import delete_queue_addr_tuple_from_list

from server_mid_level_functions import add_timestamp_addr_pair_to_list
from server_mid_level_functions import retrieve_timestamp_out_addr_pair
from server_mid_level_functions import increment_timestamp_out
from server_mid_level_functions import delete_timestamp_addr_pair_from_list

from server_mid_level_functions import server_compress_callback
from server_mid_level_functions import comp_add_stream_addr_tuple_to_list



# ==============================================================================
# ==  HIGH-LEVEL FUNCTIONS  ====================================================
# ==============================================================================

# ~~ Print current working directory ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

def server_pwd( sock, 
                addr
              ):
    if DEBUGGING:
        assert type(sock) == socket.socket
        assert type(addr) == tuple
        assert len( addr)  > 0

    sock.sendto( bytes(os.getcwd(), 'utf-8'), addr )


# ~~ Change directory ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

def server_cd( sock, 
               addr, 
               cmd_str
             ):
    if DEBUGGING:
        assert type(sock)    == socket.socket
        assert type(addr)    == tuple
        assert len( addr)     > 0
        assert type(cmd_str) == str
        assert len( cmd_str)  > 0

    try:
        curr_dir = cmd_str[3:]
        os.chdir( os.path.join(os.getcwd(),curr_dir) )
        sock.sendto( bytes('SERVER: directory changed', 'utf-8'), addr )

    except:
        sock.sendto( bytes('SERVER: failed to change directory', 'utf-8'), 
                     addr
                   )


# ~~ List files in current working directory ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

def server_ls( sock, 
               addr
             ):
    if DEBUGGING:
        assert type(sock)   == socket.socket
        assert type(addr)   == tuple
        assert len( addr)    > 0

    process = subprocess.Popen( 'ls -l',
                                shell  = True,
                                stdout = subprocess.PIPE
                              )
    result = process.stdout.read().decode() + ' '
    sock.sendto( bytes(result, 'utf-8'), addr)


# ~~ Download file from the server onto the client ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

def server_file_transfer_s2c( sock, 
                              addr, 
                              cmd_str, 
                              datagram_sz,
                              sleep_time,
                              data_pad_str         = '__okay__',
                              sample_format        = pyaudio.paInt16, 
                              channels             = 2,
                              after_timestamp_char = '&',
                              data_header_end_str  = '__'
                            ):
    if DEBUGGING:
        assert type(sock)                 == socket.socket
        assert type(addr)                 == tuple
        assert len( addr)                  > 0
        assert type(cmd_str)              == str
        assert len( cmd_str)               > 0
        assert type(datagram_sz)          == int
        assert type(sleep_time)           == float
        assert type(data_pad_str)         == str
        assert len( data_pad_str)         == 8
        assert type(sample_format)        == int
        assert type(channels)             == int
        assert type(after_timestamp_char) == str
        assert len( after_timestamp_char) == 1
        assert type(data_header_end_str)  == str
        assert len( data_header_end_str)  == 2

    file_name = cmd_str[9:]

    try:
        fo_in = open(file_name, 'rb') # 'rb' --> mode = read only, binary

        # preparing first datagram
        timestamp         = 0
        data_header, header_sz = get_header_header_sz( data_pad_str,
                                                       timestamp,
                                                       sample_format,
                                                       channels,
                                                       after_timestamp_char,
                                                       data_header_end_str
                                                     )
        msg_sz   = datagram_sz - header_sz
        data_msg = fo_in.read(msg_sz)

        data = data_header + data_msg
        # DONE preparing first datagram

        # The client will attempt to decode the first 8 bytes of data
        # as utf-8 because it's watching out for '__fail__' 
        # and '__done__' flags. If the first 8 bytes aren't utf-8
        # compliant, the script will crash. So I need to pad the first
        # 8 bytes, and data is then equal to padding + message

        print('SERVER: successfully read the start of the file')

        while len(data) > header_sz: # ie while it's not the end of file

            sock.sendto(data, addr)
            print('SERVER: successfully sent datagram ', timestamp)
            time.sleep(sleep_time)

            timestamp += 1
            data_header, header_sz = get_header_header_sz( data_pad_str,
                                                           timestamp,
                                                           sample_format,
                                                           channels,
                                                           after_timestamp_char,
                                                           data_header_end_str
                                                         )
            msg_sz   = datagram_sz - header_sz
            data_msg = fo_in.read(msg_sz)
            print('SERVER: successfully read datagram ', timestamp)
            data = data_header + data_msg

        fo_in.close()
        sock.sendto( bytes('__done__', 'utf-8'), addr)

    except:
        if DEBUGGING:
            PrintException()
        sock.sendto( bytes('__fail__', 'utf-8'), addr)

# * I marked the two prints() with "do not comment" bc they serve to
#   stall the server a little bit, and give the client time to receive
#   the datagrams. Without those comments, the server sends datagrams
#   too fast, resulting in congestion, and therefore in data loss.


# ~~ Open file to begin transfer from client to server ~~~~~~~~~~~~~~~~~~~~~~~~~

def server_file_transfer_c2s_open( sock, 
                                   addr, 
                                   cmd_str, 
                                   fo_out_addr_list,
                                   ready_flag = '__redy__'
                                 ):
    if DEBUGGING:
        assert type(sock)             == socket.socket
        assert type(addr)             == tuple
        assert len( addr)              > 0
        assert type(cmd_str)          == str
        assert len( cmd_str)           > 0
        assert type(fo_out_addr_list) == list
        assert type(ready_flag)       == str
        assert len( ready_flag)       == 8
        # fo_out_addr_list can be empty

    now = datetime.now()
    curr_time = now.strftime("%H_%M_%S_%f")
    file_name = 'uploaded_' + curr_time + '_' + cmd_str[7:]
    fo_out = open( file_name, 'ab' ) # 'ab' --> mode = append, binary
    sock.sendto( bytes(ready_flag + file_name, 'utf-8'), addr)

    print('SERVER: ...uploading file. Please wait...')

    fo_out_addr_list.append((fo_out, addr))
    
    if DEBUGGING:
        assert type(  fo_out_addr_list)        == list
        assert type(  fo_out_addr_list[-1])    == tuple
        assert_fo_out(fo_out_addr_list[-1][0])          # fo_out
        assert type(  fo_out_addr_list[-1][1]) == tuple # addr
        assert len(   fo_out_addr_list[-1][1])  > 0
    return fo_out_addr_list


# ~~ Pass datagram from client to server into corresponding pidge ~~~~~~~~~~~~~~

def server_file_transfer_c2s_copy( addr, 
                                   datagram_sz,
                                   timestamp, 
                                   n_msgs_per_pidge,
                                   data, 
                                   pidges_addr_list,
                                   sample_format = pyaudio.paInt16, 
                                   channels      = 2
                                 ):
    if DEBUGGING:
        assert type(addr)             == tuple
        assert len( addr)              > 0
        assert type(datagram_sz)      == int
        assert type(timestamp)        == int
        assert type(n_msgs_per_pidge) == int
        assert type(data)             == bytes
        assert len( data)              > 0
        assert type(pidges_addr_list) == list
        assert len( pidges_addr_list)  > 0
        assert type(sample_format)    == int
        assert type(channels)         == int

    try :
        idx_pidges, _  = retrieve_pidges( pidges_addr_list, addr )
        idx_pidge , _  = retrieve_pidges_entry( pidges_addr_list[idx_pidges][0], 
                                                timestamp, 
                                                n_msgs_per_pidge
                                              )
        msg_sz         = get_msg_sz_from_timestamp(datagram_sz, 
                                                   timestamp,
                                                   sample_format,
                                                   channels
                                                  )
        curr_msg_sz    = len(data) # within a pidge, every message will be the 
                                   # same size except the last message in the 
                                   # file, which can be smaller. This accounts 
                                   # for it 
        start_position = (timestamp % n_msgs_per_pidge) * msg_sz
        end_position   = start_position + curr_msg_sz
        edit_pidge( pidges_addr_list, 
                    idx_pidges, 
                    idx_pidge, 
                    start_position,
                    end_position,
                    data 
                  )
    except AssertionError:
        if DEBUGGING:
            PrintException()
        else:
            print("SERVER: How did you even get an assertion error?")
    except: # timestamp < min_timestamp
        print("SERVER: found timestamp smaller than the oldest pidge. Ignoring")


# ~~ Write oldest pidge to file ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

def server_file_transfer_c2s_write( pidges_addr_list,
                                    fo_out_addr_list,
                                    addr,
                                    datagram_sz,
                                    n_msgs_per_pidge,
                                    sample_format = pyaudio.paInt16, 
                                    channels      = 2
                                  ):
    if DEBUGGING:
        assert type(pidges_addr_list) == list
        assert len( pidges_addr_list)  > 0
        assert type(fo_out_addr_list) == list
        assert len( fo_out_addr_list)  > 0
        assert type(addr)             == tuple
        assert len( addr)              > 0
        assert type(datagram_sz)      == int
        assert type(n_msgs_per_pidge) == int
        assert type(sample_format)    == int
        assert type(channels)         == int

    idx_pidges, _ = retrieve_pidges( pidges_addr_list, addr )
    _, fo_out     = retrieve_fo_out( fo_out_addr_list, addr )

    timestamp_pidge = pidges_addr_list[idx_pidges][0].pop(0)
    holeless_pidge  = remove_holes_from_timestamp_pidge( timestamp_pidge, 
                                                         datagram_sz,
                                                         n_msgs_per_pidge,
                                                         sample_format,
                                                         channels
                                                       )
    fo_out.write(bytes(holeless_pidge))


# ~~ Append pidges entry filled with zeros to pidges list ~~~~~~~~~~~~~~~~~~~~~~

def update_pidges_addr_list( pidges_addr_list,
                             lowest_timestamp,
                             n_msgs_per_pidge,
                             datagram_sz,
                             addr,
                             sample_format = pyaudio.paInt16, 
                             channels      = 2
                           ):
    if DEBUGGING:
        assert type(pidges_addr_list) == list
        assert len( pidges_addr_list)  > 0
        assert type(lowest_timestamp) == int
        assert type(n_msgs_per_pidge) == int
        assert type(datagram_sz)      == int
        assert type(addr)             == tuple
        assert len( addr)              > 0
        assert type(sample_format)    == int
        assert type(channels)         == int

    msg_sz = get_msg_sz_from_timestamp(datagram_sz, 
                                       lowest_timestamp,
                                       sample_format,
                                       channels
                                      )
    timestamp_pidge = make_zeropad_pidges_entry( n_msgs_per_pidge, 
                                                 msg_sz, 
                                                 lowest_timestamp
                                               )
    idx_pidges, _ = retrieve_pidges( pidges_addr_list, addr )
    pidges_addr_list[idx_pidges][0].append(timestamp_pidge)


# ~~ Dump nonempty pidges to file ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

def server_file_transfer_c2s_dump( pidges_addr_list,
                                   fo_out_addr_list,
                                   n_msgs_per_pidge,
                                   datagram_sz,
                                   addr,
                                   sample_format = pyaudio.paInt16, 
                                   channels      = 2
                                 ):
    if DEBUGGING:
        assert type(pidges_addr_list) == list
        assert len( pidges_addr_list)  > 0
        assert type(fo_out_addr_list) == list
        assert len( fo_out_addr_list)  > 0
        assert type(n_msgs_per_pidge) == int
        assert type(datagram_sz)      == int
        assert type(addr)             == tuple
        assert len( addr)              > 0
        assert type(sample_format)    == int
        assert type(channels)         == int

    _, fo_out = retrieve_fo_out( fo_out_addr_list, addr )
    _, pidges = retrieve_pidges( pidges_addr_list, addr )
    idx, last_nonempty_timestamp_pidge = retrieve_last_nonempty_pidges_entry(
                                                                      pidges)

    for i in range(idx):
        timestamp_pidge = pidges.pop(0)
        holeless_pidge  = remove_holes_from_timestamp_pidge( timestamp_pidge, 
                                                             datagram_sz,
                                                             n_msgs_per_pidge,
                                                             sample_format,
                                                             channels
                                                           )
        fo_out.write(bytes(holeless_pidge))

    holeless_last_pidge = remove_holes_from_timestamp_pidge( 
                              last_nonempty_timestamp_pidge, 
                                                datagram_sz,
                                           n_msgs_per_pidge,
                                              sample_format,
                                                   channels)
    last_data = bytes(holeless_last_pidge)
    fo_out.write(last_data)


# ~~ Close uploaded (c --> s) file ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

def server_file_transfer_c2s_close( addr, 
                                    fo_out_addr_list
                                  ):
    if DEBUGGING:
        assert type(addr)             == tuple
        assert len( addr)              > 0
        assert type(fo_out_addr_list) == list
        assert len( fo_out_addr_list)  > 0

    idx, fo_out = retrieve_fo_out(fo_out_addr_list, addr)
    fo_out.close()
    
    out = delete_addr_fo_out_entry_at_idx(fo_out_addr_list, idx)
    if DEBUGGING:
        assert type(out) == list
        # out can be an empty list
    return out


# ~~ Wire audio (c --> s) for playback ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

def server_wire_c2s_write( pidges_addr_list,
                           playback_queue,
                           addr,
                           datagram_sz,
                           n_msgs_per_pidge,
                           sample_format = pyaudio.paInt16, 
                           channels      = 2
                         ):
    if DEBUGGING:
        assert type(pidges_addr_list)             == list
        assert len( pidges_addr_list)              > 0
        assert type(pidges_addr_list[0])          == tuple
        assert type(pidges_addr_list[0][0])       == list      # pidges 
        assert len( pidges_addr_list[0][0])        > 0
        assert type(pidges_addr_list[0][0][0])    == tuple     # pidges entry
        assert type(pidges_addr_list[0][0][0][0]) == int       # lowest_tmstmp
        assert type(pidges_addr_list[0][0][0][1]) == bytearray # 0-pad pidge
        assert len( pidges_addr_list[0][0][0][1])  > 0
        assert type(pidges_addr_list[0][1])       == tuple     # addr
        assert len( pidges_addr_list[0][1])        > 0
        assert type(playback_queue)               == bytearray
        assert type(addr)                         == tuple
        assert len( addr)                          > 0
        assert type(datagram_sz)                  == int
        assert type(n_msgs_per_pidge)             == int
        assert type(sample_format)                == int
        assert type(channels)                     == int

    idx, _   = retrieve_pidges(pidges_addr_list, addr)
    timestamp_pidge = pidges_addr_list[idx][0].pop(0)
    audio    = remove_holes_from_timestamp_pidge(timestamp_pidge, 
                                                 datagram_sz,
                                                 n_msgs_per_pidge,
                                                 sample_format,
                                                 channels
                                                )
    playback_queue.extend(audio)


# ~~ Dump nonempty audio pidges for playback ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

def server_wire_c2s_dump( pidges_addr_list,
                          stream_addr_list,
                          playback_queue,
                          n_msgs_per_pidge,
                          datagram_sz,
                          addr,
                          sample_format = pyaudio.paInt16, 
                          channels      = 2
                        ):
    if DEBUGGING:
        assert type(pidges_addr_list)             == list
        assert len( pidges_addr_list)              > 0
        assert type(pidges_addr_list[0])          == tuple
        assert type(pidges_addr_list[0][0])       == list      # pidges 
        assert len( pidges_addr_list[0][0])        > 0
        assert type(pidges_addr_list[0][0][0])    == tuple     # pidges entry
        assert type(pidges_addr_list[0][0][0][0]) == int       # lowest_tmstmp
        assert type(pidges_addr_list[0][0][0][1]) == bytearray # 0-pad pidge
        assert len( pidges_addr_list[0][0][0][1])  > 0
        assert type(pidges_addr_list[0][1])       == tuple     # addr
        assert len( pidges_addr_list[0][1])        > 0
        assert type(stream_addr_list)             == list
        assert len( stream_addr_list)              > 0
        assert type(stream_addr_list[0])          == tuple
        assert type(stream_addr_list[0][0])       == pyaudio.Stream # stream
        assert type(stream_addr_list[0][1])       == tuple          # addr
        assert len( stream_addr_list[0][1])        > 0
        assert type(playback_queue)               == bytearray
        assert type(n_msgs_per_pidge)             == int
        assert type(datagram_sz)                  == int
        assert type(addr)                         == tuple
        assert len( addr)                          > 0 
        assert type(sample_format)                == int
        assert type(channels)                     == int

    _, stream = retrieve_stream( stream_addr_list, addr )
    _, pidges = retrieve_pidges( pidges_addr_list, addr )
    idx, last_nonempty_timestamp_pidge = retrieve_last_nonempty_pidges_entry(
                                                                      pidges)
    _, last_nonempty_pidge = last_nonempty_timestamp_pidge
    for i in range(idx):
        timestamp_pidge = pidges.pop(0)
        audio = remove_holes_from_timestamp_pidge(timestamp_pidge,
                                                  datagram_sz,
                                                  n_msgs_per_pidge,
                                                  sample_format,
                                                  channels
                                                 )
        playback_queue.extend(audio)

    audio = remove_holes_from_timestamp_pidge(pidges[0],
                                              datagram_sz,
                                              n_msgs_per_pidge,
                                              sample_format,
                                              channels
                                             )
    playback_queue.extend(audio)


# ~~ Close wiring (c --> s) audio ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

def server_wire_c2s_close( addr, 
                           stream_addr_list
                         ):
    if DEBUGGING:
        assert type(addr)                   == tuple
        assert len( addr)                    > 0 
        assert type(stream_addr_list)       == list
        assert len( stream_addr_list)        > 0
        assert type(stream_addr_list[0])    == tuple
        assert type(stream_addr_list[0][0]) == pyaudio.Stream # stream
        assert type(stream_addr_list[0][1]) == tuple          # addr
        assert len( stream_addr_list[0][1])  > 0

    idx, stream = retrieve_stream( stream_addr_list, addr )
    stream.stop_stream()
    stream.close()
    stream_addr_list.pop(idx)

    if DEBUGGING:
        assert type(stream_addr_list) == list
        # stream_addr_list can be an empty list
    return stream_addr_list


# ~~ Wire audio (c --> s) for compression ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

def server_comp_c2s_write( pidges_addr_list,
                           compress_queue_addr_list,
                           addr,
                           datagram_sz,
                           n_msgs_per_pidge
                         ):
    if DEBUGGING:
        assert type(pidges_addr_list)               == list
        assert len( pidges_addr_list)                > 0
        assert type(pidges_addr_list[0])            == tuple
        assert type(pidges_addr_list[0][0])         == list      # pidges 
        assert len( pidges_addr_list[0][0])          > 0
        assert type(pidges_addr_list[0][0][0])      == tuple     # pidges entry
        assert type(pidges_addr_list[0][0][0][0])   == int       # lowest_tmstmp
        assert type(pidges_addr_list[0][0][0][1])   == bytearray # 0-pad pidge
        assert len( pidges_addr_list[0][0][0][1])    > 0
        assert type(pidges_addr_list[0][1])         == tuple     # addr
        assert len( pidges_addr_list[0][1])          > 0
        assert type(compress_queue_addr_list)       == list
        assert len( compress_queue_addr_list)        > 0
        assert type(compress_queue_addr_list[0])    == tuple
        assert type(compress_queue_addr_list[0][0]) == bytearray # comprss_queue
        assert type(compress_queue_addr_list[0][1]) == tuple     # addr
        assert type(addr)                           == tuple
        assert len( addr)                            > 0
        assert type(datagram_sz)                    == int
        assert type(n_msgs_per_pidge)               == int

    idx_pidge, _    = retrieve_pidges(pidges_addr_list, addr)
    idx_queue, _    = retrieve_compress_queue(compress_queue_addr_list, addr)
    timestamp_pidge = pidges_addr_list[idx_pidge][0].pop(0)
    audio    = remove_holes_from_timestamp_pidge(timestamp_pidge, 
                                                 datagram_sz,
                                                 n_msgs_per_pidge
                                                )
    compress_queue_addr_list[idx_queue][0].extend(audio)


# ~~ Dump nonempty audio pidges for compression ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

def server_comp_c2s_dump( pidges_addr_list,
                          stream_addr_list,
                          compress_queue_addr_list,
                          n_msgs_per_pidge,
                          datagram_sz,
                          addr
                        ):
    if DEBUGGING:
        assert type(pidges_addr_list)               == list
        assert len( pidges_addr_list)                > 0
        assert type(pidges_addr_list[0])            == tuple
        assert type(pidges_addr_list[0][0])         == list      # pidges 
        assert len( pidges_addr_list[0][0])          > 0
        assert type(pidges_addr_list[0][0][0])      == tuple     # pidges entry
        assert type(pidges_addr_list[0][0][0][0])   == int       # lowest_tmstmp
        assert type(pidges_addr_list[0][0][0][1])   == bytearray # 0-pad pidge
        assert len( pidges_addr_list[0][0][0][1])    > 0
        assert type(pidges_addr_list[0][1])         == tuple     # addr
        assert len( pidges_addr_list[0][1])          > 0
        assert type(stream_addr_list)               == list
        assert len( stream_addr_list)                > 0
        assert type(stream_addr_list[0])            == tuple
        assert type(stream_addr_list[0][0])         == pyaudio.Stream # stream
        assert type(stream_addr_list[0][1])         == tuple          # addr
        assert len( stream_addr_list[0][1])          > 0
        assert type(compress_queue_addr_list)       == list
        assert len( compress_queue_addr_list)        > 0
        assert type(compress_queue_addr_list[0])    == tuple
        assert type(compress_queue_addr_list[0][0]) == bytearray # comprss_queue
        assert type(compress_queue_addr_list[0][1]) == tuple     # addr
        assert type(n_msgs_per_pidge)               == int
        assert type(datagram_sz)                    == int
        assert type(addr)                           == tuple
        assert len( addr)                            > 0 

    _, stream = retrieve_stream( stream_addr_list, addr )
    _, pidges = retrieve_pidges( pidges_addr_list, addr )
    idx_queue, _    = retrieve_compress_queue(compress_queue_addr_list, addr)
    r, last_nonempty_timestamp_pidge = retrieve_last_nonempty_pidges_entry(
                                                                    pidges)
    _, last_nonempty_pidge = last_nonempty_timestamp_pidge
    for i in range(r):
        timestamp_pidge = pidges.pop(0)
        audio = remove_holes_from_timestamp_pidge(timestamp_pidge,
                                                  datagram_sz,
                                                  n_msgs_per_pidge
                                                 )
        compress_queue_addr_list[idx_queue][0].extend(audio)

    audio = remove_holes_from_timestamp_pidge(pidges[0],
                                              datagram_sz,
                                              n_msgs_per_pidge
                                             )
    compress_queue_addr_list[idx_queue][0].extend(audio)


# ~~ Print client address ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

def server_hello( sock, 
                  addr
                ):
    if DEBUGGING:
        assert type(sock) == socket.socket
        assert type(addr) == tuple
        assert len( addr)  > 0
    print('SERVER: connected to client #' + str(addr[1]))
    sock.sendto( bytes('SERVER: Hello, client #' + str(addr[1]), 'utf-8'), addr)


