# ==============================================================================
# ==  IMPORTS  =================================================================
# ==============================================================================

# ~~ module access from main ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
import sys
sys.path.append('./modules')


# ~~ debugging ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
from debugging import DEBUGGING
if DEBUGGING:
    import os
    import types
    import numbers


# ~~ compression ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
import numpy as np
import math
from pydub import AudioSegment


# ~~ record / playback ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
import pyaudio
import time


# ~~ nparray_correct_byteorder(...) ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
import copy



# ==============================================================================
# ==  SUNK-LEVEL FUNCTIONS  ====================================================
# ==============================================================================

# ~~ sunk-level --> math operations ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

from compressor_sunk_level_functions import retrieve_max_audio_value
from compressor_sunk_level_functions import log_v_y_t
from compressor_sunk_level_functions import detect_level_mono
from compressor_sunk_level_functions import get_nparray_log_from_nparray
from compressor_sunk_level_functions import normalize_nparray_mono
from compressor_sunk_level_functions import compress_sample
from compressor_sunk_level_functions import fade_in_nparray_mono


# ~~ sunk-level --> converters ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

from compressor_sunk_level_functions import nparray_LR_2_nparrays_L_and_R
from compressor_sunk_level_functions import nparray_back_2_sample_format
from compressor_sunk_level_functions import nparray_correct_byteorder



# ==============================================================================
# ==  MID-LEVEL FUNCTIONS  =====================================================
# ==============================================================================

# ~~ mid-level --> converters ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

from compressor_mid_level_functions import bytes_stereo_2_nparray_mono
from compressor_mid_level_functions import nparray_mono_2_bytearray_mono


# ~~ mid-level --> audio procedures ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

from compressor_mid_level_functions import level_detector_nparray_mono
from compressor_mid_level_functions import compress_audio_nparray_mono



# ==============================================================================
# ==  HIGH-LEVEL FUNCTIONS  ====================================================
# ==============================================================================

# ~~ high-level --> turn non-DRC'd frames into yes-DRC'd frames ~~~~~~~~~~~~~~~~

from compressor_high_level_functions import compress_mono


# ~~ high-level --> pyaudio callback (thr 1 in --> comp / thr 2 comp --> out) ~~

from compressor_high_level_functions import callback_in_DRC_queue
from compressor_high_level_functions import callback_queue_out
from compressor_high_level_functions import pyaudio_DRC_stream_manager
