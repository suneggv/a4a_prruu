from debugging import DEBUGGING
from server_compressor_imports import *

import socket
import pyaudio
if DEBUGGING:
    import types
    import numbers



# ==============================================================================
# ==  DIAGNOSTIC FUNCTIONS  ====================================================
# ==============================================================================

from diagnostic_functions import assert_fo_in
from diagnostic_functions import assert_fo_out
from diagnostic_functions import PrintException



# ==============================================================================
# ==  MID-LEVEL FUNCTIONS  =====================================================
# ==============================================================================

# ~~ management of uploads from several simultaneous clients ~~~~~~~~~~~~~~~~~~~

def retrieve_fo_out( fo_out_addr_list, 
                     addr
                   ):
    if DEBUGGING:
        assert type(fo_out_addr_list) == list
        assert len( fo_out_addr_list)  > 0 
        assert type(addr)             == tuple

    fo_out_addr = [tup for tup in fo_out_addr_list if tup[1] == addr][0]
    idx = [i for i, val in enumerate(fo_out_addr_list) if val == fo_out_addr][0]
    
    out = (idx, fo_out_addr[0])
    if DEBUGGING:
        assert type(  out)    == tuple
        assert type(  out[0]) == int
        assert_fo_out(out[1]) #  _io.BufferedWriter
    return out


def delete_addr_fo_out_entry_at_idx( fo_out_addr_list, 
                                     idx
                                   ):
    if DEBUGGING:
        assert type(fo_out_addr_list) == list
        assert len( fo_out_addr_list)  > 0
        assert type(idx)              == int

    fo_out_addr_list.pop(idx)

    if DEBUGGING:
        assert type(fo_out_addr_list) == list
        # fo_out_addr_list can be empty
    return fo_out_addr_list


# ~~ syncing uploaded data server-side --> headers ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

def get_header_header_sz( data_pad_str, 
                          timestamp,
                          sample_format        = pyaudio.paInt16, 
                          channels             = 2,
                          after_timestamp_char = '&',
                          data_header_end_str  = '__'
                        ):
    if DEBUGGING:
        assert type(data_pad_str)         == str
        assert len( data_pad_str)         == 8
        assert type(timestamp)            == int
        assert type(sample_format)        == int
        assert type(channels)             == int
        assert type(after_timestamp_char) == str
        assert len( after_timestamp_char) == 1
        assert type(data_header_end_str)  == str
        assert len( data_header_end_str)  == 2

    sample_width        = pyaudio.get_sample_size(sample_format) # byte width
    multiple            = channels * sample_width
    after_timestamp_str = after_timestamp_char * multiple
    header_sz_draft     = 10 + len(str(timestamp))
    after_timestamp_sz  = (multiple - header_sz_draft % multiple) % multiple
    header_sz           = header_sz_draft + after_timestamp_sz

    header = bytes(data_pad_str,                             'utf-8') + \
             bytes(str(timestamp),                           'utf-8') + \
             bytes(after_timestamp_str[:after_timestamp_sz], 'utf-8') + \
             bytes(data_header_end_str,                      'utf-8')

    out = (header, header_sz)
    if DEBUGGING:
        assert type(out)         == tuple
        assert type(out[0])      == bytes # header
        assert type(out[1])      == int   # header_sz
        assert out[1] % multiple == 0
    return out


def get_header_header_sz_from_datagram( datagram,
                                        data_header_end_str = '__'
                                      ): # hdr format --> __okay__TIMESTAMP&__
    if DEBUGGING:
        assert type(datagram)                == bytes
        assert len( datagram[8:])             > 0
        assert datagram[0:2].decode('utf-8') == '__'
        assert datagram[6:8].decode('utf-8') == '__'
        assert type(data_header_end_str)     == str
        assert len( data_header_end_str)     == 2

    for i in range(8,len(datagram)):
        if datagram[i:(i+1)].decode('utf-8') == data_header_end_str[0]:

            header_sz = i + 2 # i == len('__okay__TIMESTAMP'), just missing '__'
            out = (datagram[:header_sz], header_sz)
            if DEBUGGING:
                assert type(out)                    == tuple
                assert type(out[0])                 == bytes
                assert type(out[0].decode('utf-8')) == str
                assert type(out[1])                 == int
                assert len( out[0])                 == out[1]
            return out
    raise Exception("get_header_header_sz_from_datagram(...) failed" + \
                    " to find header (how did you even get here???)")


def get_header_sz_from_timestamp( timestamp,
                                  sample_format = pyaudio.paInt16, 
                                  channels      = 2
                                ): # header format --> smth__TIMESTAMP&&__
    if DEBUGGING:
        assert type(timestamp)     == int
        assert type(sample_format) == int
        assert type(channels)      == int

    sample_width       = pyaudio.get_sample_size(sample_format) # byte width
    multiple           = sample_width * channels
    header_sz_draft    = 10 + len(str(timestamp))
    after_timestamp_sz = (multiple - header_sz_draft % multiple) % multiple
    header_sz          = header_sz_draft + after_timestamp_sz

    if DEBUGGING:
        assert type(header_sz)      == int
        assert header_sz % multiple == 0
    return header_sz


def get_msg_sz_from_timestamp( datagram_sz, 
                               timestamp,
                               sample_format = pyaudio.paInt16, 
                               channels      = 2
                             ):
    if DEBUGGING:
        assert type(datagram_sz)   == int
        assert type(timestamp)     == int
        assert type(sample_format) == int
        assert type(channels)      == int

    msg_sz = datagram_sz - get_header_sz_from_timestamp(timestamp,
                                                        sample_format, 
                                                        channels)
    if DEBUGGING:
        assert type(msg_sz) == int
    return msg_sz


def get_timestamp_from_header( header,
                               after_timestamp_char = '&',
                               data_header_end_str  = '__'
                             ): # header format: __smth__TIMESTAMP&&&__
    if DEBUGGING:
        assert type(header)               == bytes
        assert type(after_timestamp_char) == str
        assert len( after_timestamp_char) == 1
        assert type(data_header_end_str)  == str
        assert len( data_header_end_str)  == 2

    for r in range(8, len(header)):
        next_char = header[r:r+1].decode('utf-8')

        if ( next_char == after_timestamp_char   or 
             next_char == data_header_end_str[0] ):

            timestamp_str = header[8:r].decode('utf_8')

            timestamp = int(timestamp_str)
            if DEBUGGING:
                assert type(timestamp) == int
                assert timestamp       >= 0
            return timestamp

    raise Exception("get_timestamp_from_header(...) failed to find timestamp")


# ~~ syncing uploaded data server-side --> pidges list management ~~~~~~~~~~~~~~

def make_zeropad_pidges_entry( n_msgs_per_pidge, 
                               msg_sz, 
                               lowest_timestamp
                             ):
    if DEBUGGING:
        assert type(n_msgs_per_pidge) == int
        assert type(msg_sz)           == int
        assert type(lowest_timestamp) == int

    zp_pidges_entry = (lowest_timestamp, bytearray(msg_sz * n_msgs_per_pidge))
    if DEBUGGING:
        assert type(zp_pidges_entry)    == tuple
        assert type(zp_pidges_entry[0]) == int       # lowest_timestamp
        assert type(zp_pidges_entry[1]) == bytearray # zeropadded pidge
        assert len( zp_pidges_entry[1]) == msg_sz * n_msgs_per_pidge
        assert sum( zp_pidges_entry[1]) == 0
    return zp_pidges_entry


def add_zeropad_pidges_addr_tuple_to_list( n_msgs_per_pidge,
                                           n_entries_per_pidges,
                                           datagram_sz,
                                           addr,
                                           pidges_addr_list,
                                           sample_format = pyaudio.paInt16, 
                                           channels      = 2
                                         ):
    if DEBUGGING:                                         
        assert type(n_msgs_per_pidge)     == int
        assert type(n_entries_per_pidges) == int
        assert type(datagram_sz)          == int
        assert type(addr)                 == tuple
        assert len( addr)                  > 0
        assert type(pidges_addr_list)     == list 
        # pidges_addr_list can be empty
        assert type(sample_format) == int
        assert type(channels)      == int

    pidges = []
    for i in range(n_entries_per_pidges):
        lowest_timestamp = i * n_msgs_per_pidge
        msg_sz = get_msg_sz_from_timestamp(datagram_sz, 
                                           lowest_timestamp,
                                           sample_format,
                                           channels
                                          )
        pidges.append( make_zeropad_pidges_entry( n_msgs_per_pidge, 
                                                  msg_sz, 
                                                  lowest_timestamp
                                                )
                     )
    pidges_addr_list.append( (pidges, addr) )

    if DEBUGGING:  
        assert type(pidges_addr_list)              == list 
        assert len( pidges_addr_list)               > 0
        assert type(pidges_addr_list[-1])          == tuple
        assert type(pidges_addr_list[-1][0])       == list      # pidges 
        assert len( pidges_addr_list[-1][0])       == n_entries_per_pidges
        assert type(pidges_addr_list[-1][0][0])    == tuple     # pidges entry
        assert type(pidges_addr_list[-1][0][0][0]) == int       # lowest_tmstmp
        assert type(pidges_addr_list[-1][0][0][1]) == bytearray # 0-pad pidge
        assert len( pidges_addr_list[-1][0][-1][1])== msg_sz * n_msgs_per_pidge
        assert sum( pidges_addr_list[-1][0][0][1]) == 0
        assert type(pidges_addr_list[-1][1])       == tuple     # addr
        assert len( pidges_addr_list[-1][1])        > 0
    return pidges_addr_list


def retrieve_pidges( pidges_addr_list, 
                     addr
                   ):
    if DEBUGGING:
        assert type(pidges_addr_list) == list
        assert len( pidges_addr_list)  > 0
        assert type(addr)             == tuple
        assert len( addr)              > 0

    pidges_addr = [tup for tup in pidges_addr_list if tup[1] == addr][0]
    idx = [i for i, val in enumerate(pidges_addr_list) if val == pidges_addr][0]
    
    out = (idx, pidges_addr[0])
    if DEBUGGING:
        assert type(out)          == tuple
        assert type(out[0])       == int       # idx 
        assert type(out[1])       == list      # pidges
        assert pidges_addr_list[ out[0] ][0] == out[1]
        assert len( out[1])        > 0
        assert type(out[1][0])    == tuple     # pidges entry
        assert type(out[1][0][0]) == int       # lowest_timestamp for the pidge
        assert type(out[1][0][1]) == bytearray # pidge
        assert len( out[1][0][1])  > 0
        # the pidge can be nonzero
    return out


def retrieve_pidges_entry( pidges, 
                           timestamp, 
                           n_msgs_per_pidge
                         ):
    if DEBUGGING:
        assert type(pidges)           == list
        assert len( pidges)            > 0
        assert type(timestamp)        == int
        assert type(n_msgs_per_pidge) == int

    entry_timestamp = timestamp - timestamp % n_msgs_per_pidge
    timestamp_pidge = [tup for tup in pidges if tup[0] == entry_timestamp][0]
    idx = [i for i, val in enumerate(pidges) if val == timestamp_pidge][0]
    
    out = (idx, timestamp_pidge[1])
    if DEBUGGING:
        assert type(out)    == tuple
        assert type(out[0]) == int       # idx
        assert type(out[1]) == bytearray # pidge
        assert len( out[1])  > 0
    return out


def edit_pidge( pidges_addr_list, 
                idx_pidges, 
                idx_pidge, 
                start_position, 
                end_position,
                data
              ):
    if DEBUGGING:
        assert type(pidges_addr_list)              == list
        assert len( pidges_addr_list)               > 0
        assert type(pidges_addr_list[-1])          == tuple
        assert type(pidges_addr_list[-1][0])       == list      # pidges 
        assert len( pidges_addr_list[-1][0])        > 0
        assert type(pidges_addr_list[-1][0][0])    == tuple     # pidges entry
        assert type(pidges_addr_list[-1][0][0][0]) == int       # lowest_tmstmp
        assert type(pidges_addr_list[-1][0][0][1]) == bytearray # 0-pad pidge
        assert len( pidges_addr_list[-1][0][0][1])  > 0
        assert type(pidges_addr_list[-1][1])       == tuple     # addr
        assert len( pidges_addr_list[-1][1])        > 0
        assert type(idx_pidges)                    == int
        assert type(idx_pidge)                     == int
        assert type(start_position)                == int
        assert type(end_position)                  == int
        assert type(data)                          == bytes
        assert len( data)                           > 0

    i_cs = idx_pidges
    i_c  = idx_pidge
    s    = start_position
    e    = end_position
    pidges_addr_list[i_cs][0][i_c][1][s:e] = data
    #                |     |  |    |  | 
    #                |     |  |    |  select range inside pidge corresp. to data
    #                |     |  |    pidges entry --> (low_tmstp, pidge) 
    #                |     |  select correct entry in pidges list
    #                |     pidges_addr_list entry --> (pidges, addr)
    #                select correct entry in pidges_addr_list


def delete_pidges_addr_tuple_from_list( pidges_addr_list, 
                                        addr 
                                      ):
    if DEBUGGING:
        assert type(pidges_addr_list) == list
        assert len( pidges_addr_list)  > 0
        assert type(addr)             == tuple
        assert len( addr)              > 0

    idx, _ = retrieve_pidges( pidges_addr_list, addr )
    pidges_addr_list.pop(idx)
    
    if DEBUGGING:
        assert type(pidges_addr_list) == list
        # pidges_addr_list can be empty
    return pidges_addr_list


# ~~ syncing uploaded data server-side --> largest timestamp management ~~~~~~~~

def add_largest_timestamp_addr_tuple_to_list( n_msgs_per_pidge, 
                                              n_entries_per_pidges,
                                              addr,
                                              largest_timestamps
                                            ):
    if DEBUGGING:
        assert type(n_msgs_per_pidge)     == int
        assert type(n_entries_per_pidges) == int
        assert type(addr)                 == tuple
        assert len( addr)                  > 0
        assert type(largest_timestamps)   == list
        # largest_timestamps can be empty

    largest_timestamp = n_msgs_per_pidge * n_entries_per_pidges - 1
    largest_timestamps.append([largest_timestamp ,addr])
    
    if DEBUGGING:
        assert type(largest_timestamps)        == list
        assert len( largest_timestamps)         > 0
        assert type(largest_timestamps[-1])    == list  # lgst_tmstmps entry
        assert type(largest_timestamps[-1][0]) == int   # largest_timestamp
        assert type(largest_timestamps[-1][1]) == tuple # addr
        assert len( largest_timestamps[-1][1])  > 0
    return largest_timestamps


def retrieve_largest_timestamp( largest_timestamps, 
                                addr 
                              ):
    if DEBUGGING:
        assert type(largest_timestamps) == list
        assert len( largest_timestamps)  > 0
        assert type(addr)               == tuple
        assert len( addr)                > 0

    l_tstp_addr = [lst for lst in largest_timestamps if lst[1] == addr][0]
    idx = [i for i,val in enumerate(largest_timestamps) if val==l_tstp_addr][0]
    
    out = (idx, l_tstp_addr[0])
    if DEBUGGING:
        assert type(out)    == tuple
        assert type(out[0]) == int   # idx
        assert type(out[1]) == int   # largest_timestamp
    return out


def update_largest_timestamp( new_largest_timestamp, 
                              addr, 
                              largest_timestamps
                            ):
    if DEBUGGING:
        assert type(new_largest_timestamp) == int
        assert type(addr)                  == tuple
        assert len( addr)                   > 0
        assert type(largest_timestamps)    == list
        assert len( largest_timestamps)     > 0

    idx, _ = retrieve_largest_timestamp( largest_timestamps, addr )
    largest_timestamps[idx][0] = new_largest_timestamp
    
    if DEBUGGING:
        assert type(largest_timestamps)         == list
        assert type(largest_timestamps[idx])    == list  # lgst_tmstmps entry
        assert type(largest_timestamps[idx][0]) == int   # largest_timestamp
        assert      largest_timestamps[idx][1]  == addr
    return largest_timestamps


def delete_l_tstp_addr_sublist_from_list( largest_timestamps, 
                                          addr
                                        ):
    if DEBUGGING:
        assert type(largest_timestamps)    == list
        assert len( largest_timestamps)     > 0
        assert type(addr)                  == tuple
        assert len( addr)                   > 0

    idx, _ = retrieve_largest_timestamp( largest_timestamps, addr )
    largest_timestamps.pop(idx)
    
    if DEBUGGING:
        assert type(largest_timestamps) == list
        # largest_timestamp can be empty
    return largest_timestamps


# ~~ dumping pidges ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

def retrieve_last_nonempty_pidges_entry( pidges
                                       ):
    if DEBUGGING:
        assert type(pidges) == list
        assert len( pidges)  > 0

    idx = [i for i in range(len(pidges)) if sum(pidges[i][1]) != 0][-1]
    
    out = (idx, pidges[idx])
    if DEBUGGING:
        assert type(out)       == tuple
        assert type(out[0])    == int
        assert type(out[1])    == tuple     # pidges entry
        assert type(out[1][0]) == int       # lowest_timestamp
        assert type(out[1][1]) == bytearray # pidge
        assert len( out[1][1])  > 0
        assert sum( out[1][1]) != 0
    return out


def retrieve_data_from_last_pidge( pidge 
                                 ):
    if DEBUGGING:
        assert type(pidge) == bytearray
        assert len( pidge)  > 0

    idx_R = [i for i in range(len(pidge)) if pidge[i] != 0][-1] + 1
    
    out = pidge[:idx_R]
    if DEBUGGING:
        assert type(out) == bytearray
        assert len( out)  > 0
    return out


# ~~ pyaudio -- wire audio ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

def server_wire_callback( playback_queue,
                          chunk,
                          channels,
                          sample_format
                        ):
    if DEBUGGING:
        assert type(playback_queue) == bytearray
        assert type(chunk)          == int
        assert type(channels)       == int
        assert type(sample_format)  == int

    def callback( in_data,     # recorded data (ie this is None bc input=False)
                  frame_count, # number of frames
                  time_info,   # dictionary
                  status       # PaCallbackFlags
                ):
        if DEBUGGING:
            assert type(in_data)     == types.NoneType
            assert type(frame_count) == int
            assert type(time_info)   == dict
            assert type(status)      == int

        out_data = bytearray()
        sz = chunk * channels * pyaudio.get_sample_size(sample_format)

        if len(playback_queue) >= sz:
            out_data = playback_queue[:sz]
            del playback_queue[:sz]

        else:
            out_data = bytes(sz) # pyaudio callback audio output works based on
                                 # a "file" paradigm. It checks the length of
                                 # the first output of the callback function
                                 # to determine if the end of a file has been
                                 # reached. Namely, if its length is smaller
                                 # than frame_count * channels * sample_width,
                                 # pyaudio decides that the eof has been
                                 # reached, and stops referring to the callback
                                 # function altogether. If data shows up later, 
                                 # it'll be ignored. out_data = bytes(sz) is a 
                                 # "hack" to fool pyaudio into thinking the 
                                 # "file" hasn't ended yet, and continue 
                                 # referring to the callback function.

        out = (bytes(out_data), pyaudio.paContinue)
        if DEBUGGING:
            assert type(out)    == tuple
            assert type(out[0]) == bytes
            assert type(out[1]) == int
        return out

    if DEBUGGING:
        assert type(callback) == types.FunctionType
    return callback


def wire_add_stream_addr_tuple_to_list( stream_addr_list, 
                                        addr,
                                        pa,
                                        playback_queue,
                                        chunk         = 1024,
                                        sample_format = pyaudio.paInt16,
                                        channels      = 2,
                                        fs            = 44100
                                      ):
    if DEBUGGING:
        assert type(stream_addr_list) == list
        # stream_addr_list can be empty
        assert type(addr)             == tuple
        assert len( addr)              > 0
        assert type(pa)               == pyaudio.PyAudio
        assert type(playback_queue)   == bytearray
        assert type(chunk)            == int
        assert type(sample_format)    == int
        assert type(channels)         == int
        assert type(fs)               == int

    stream = pa.open(format            = sample_format,
                     channels          = channels,
                     rate              = fs,
                     output            = True,
                     frames_per_buffer = chunk,
                     stream_callback   = server_wire_callback( playback_queue,
                                                               chunk,
                                                               channels,
                                                               sample_format
                                                             )
                    )
    stream_addr_list.append( (stream, addr) )

    if DEBUGGING:
        assert type(stream_addr_list)        == list
        assert len( stream_addr_list)         > 0
        assert type(stream_addr_list[-1])    == tuple
        assert type(stream_addr_list[-1][0]) == pyaudio.Stream # stream
        assert type(stream_addr_list[-1][1]) == tuple          # addr
        assert len( stream_addr_list[-1][1])  > 0
    return stream_addr_list


def retrieve_stream( stream_addr_list, 
                     addr
                   ):
    if DEBUGGING:
        assert type(stream_addr_list)       == list
        assert len( stream_addr_list)        > 0
        assert type(stream_addr_list[0])    == tuple
        assert type(stream_addr_list[0][0]) == pyaudio.Stream # stream
        assert type(stream_addr_list[0][1]) == tuple          # addr
        assert len( stream_addr_list[0][1])  > 0
        assert type(addr)                   == tuple
        assert len( addr)                    > 0

    stream_addr = [tup for tup in stream_addr_list if tup[1] == addr][0]
    idx = [i for i, val in enumerate(stream_addr_list) if val == stream_addr][0]
    
    out = (idx, stream_addr[0])
    if DEBUGGING:
        assert type(out)    == tuple
        assert type(out[0]) == int
        assert type(out[1]) == pyaudio.Stream
    return out


def start_the_stream( stream_addr_list, 
                      addr
                    ):
    if DEBUGGING:
        assert type(stream_addr_list)       == list
        assert len( stream_addr_list)        > 0
        assert type(stream_addr_list[0])    == tuple
        assert type(stream_addr_list[0][0]) == pyaudio.Stream # stream
        assert type(stream_addr_list[0][1]) == tuple          # addr
        assert len( stream_addr_list[0][1])  > 0
        assert type(addr)                   == tuple
        assert len( addr)                    > 0

    _, stream = retrieve_stream( stream_addr_list, addr)
    stream.start_stream()


def remove_holes_from_timestamp_pidge( timestamp_pidge,
                                       datagram_sz, 
                                       n_msgs_per_pidge,
                                       sample_format = pyaudio.paInt16, 
                                       channels      = 2
                                     ):
    if DEBUGGING:
        assert type(timestamp_pidge)    == tuple
        assert type(timestamp_pidge[0]) == int       # lowest_timestamp
        assert type(timestamp_pidge[1]) == bytearray # pidge
        assert type(datagram_sz)        == int
        assert type(n_msgs_per_pidge)   == int
        assert type(sample_format)      == int
        assert type(channels)           == int

    timestamp, pidge = timestamp_pidge
    msg_sz = get_msg_sz_from_timestamp(datagram_sz, 
                                       timestamp,
                                       sample_format,
                                       channels
                                      )
    out = bytearray()

    for i in range(n_msgs_per_pidge):
        l = i     * msg_sz
        r = (i+1) * msg_sz
        if sum(pidge[l:r]) != 0:
            out.extend(pidge[l:r])

    if DEBUGGING:
        assert type(out) == bytearray
        # out can be an empty bytearray
    return out


def add_filled_addr_pair_to_list( filled_addr_list,
                                  addr
                                ):
    if DEBUGGING:
        assert type(filled_addr_list) == list
        # filled_addr_list can be empty
        assert type(addr)             == tuple
        assert len( addr)              > 0

    pidges_filled = False
    filled_addr_list.append( [pidges_filled, addr] )

    if DEBUGGING:
        assert type(filled_addr_list)        == list
        assert len( filled_addr_list)         > 0
        assert type(filled_addr_list[-1])    == list
        assert type(filled_addr_list[-1][0]) == bool   # pidges_filled
        assert type(filled_addr_list[-1][1]) == tuple  # addr
    return filled_addr_list


def retrieve_filled_addr( filled_addr_list,
                          addr
                        ):
    if DEBUGGING:
        assert type(filled_addr_list) == list
        assert len( filled_addr_list)  > 0
        assert type(addr)             == tuple
        assert len( addr)              > 0

    filled_addr = [lst for lst in filled_addr_list if lst[1] == addr][0]
    idx = [i for i,val in enumerate(filled_addr_list) if val==filled_addr][0]
    
    out = (idx, filled_addr)
    if DEBUGGING:
        assert type(out)       == tuple
        assert type(out[0])    == int   # idx
        assert type(out[1])    == list # [pidges_filled, addr]
        assert type(out[1][0]) == bool  # pidges_filled
        assert type(out[1][1]) == tuple # addr 
        assert len( out[1][1])  > 0 
    return out


def toggle_filled( filled_addr_list,
                   addr
                 ):
    if DEBUGGING:
        assert type(filled_addr_list) == list
        assert len( filled_addr_list)  > 0
        assert type(addr)             == tuple
        assert len( addr)              > 0

    idx, filled_addr = retrieve_filled_addr( filled_addr_list, addr )
    pidges_filled, _ = filled_addr
    if pidges_filled:
        filled_addr_list[idx][0] = False
    else:
        filled_addr_list[idx][0] = True


def these_are_filled( filled_addr_list,
                      addr
                    ):
    if DEBUGGING:
        assert type(filled_addr_list) == list
        assert len( filled_addr_list)  > 0
        assert type(addr)             == tuple
        assert len( addr)              > 0

    _, filled_addr = retrieve_filled_addr( filled_addr_list, addr )

    pidges_filled, _ = filled_addr
    if DEBUGGING:
        assert type(pidges_filled) == bool
    return pidges_filled


def delete_filled_addr_pair_from_list( filled_addr_list,
                                       addr
                                     ):
    if DEBUGGING:
        assert type(filled_addr_list) == list
        assert len( filled_addr_list)  > 0
        assert type(addr)             == tuple
        assert len( addr)              > 0

    idx, _ = retrieve_filled_addr( filled_addr_list, addr )
    filled_addr_list.pop(idx)
    
    if DEBUGGING:
        assert type(filled_addr_list) == list
        # filled_addr_list can be empty
    return filled_addr_list


def add_comp_queue_addr_tuple_to_list( compress_queue_addr_list,
                                       addr
                                     ):
    if DEBUGGING:
        assert type(compress_queue_addr_list) == list
        # compress_queue_addr_list can be empty
        assert type(addr)                     == tuple
        assert len( addr)                      > 0

    compress_queue = bytearray()
    compress_queue_addr_list.append( (compress_queue, addr) )

    if DEBUGGING:
        assert type(compress_queue_addr_list)        == list
        assert len( compress_queue_addr_list)         > 0
        assert type(compress_queue_addr_list[-1])    == tuple
        assert type(compress_queue_addr_list[-1][0]) == bytearray # comp_queue
        assert type(compress_queue_addr_list[-1][1]) == tuple     # addr
    return compress_queue_addr_list


def retrieve_compress_queue( comp_queue_addr_list,
                             addr
                           ):
    if DEBUGGING:
        assert type(comp_queue_addr_list)       == list
        assert len( comp_queue_addr_list)        > 0
        assert type(comp_queue_addr_list[0])    == tuple
        assert type(comp_queue_addr_list[0][0]) == bytearray # comprss_queue
        assert type(comp_queue_addr_list[0][1]) == tuple     # addr
        assert type(addr)                       == tuple
        assert len( addr)                        > 0

    CQ_addr = [tup for tup in comp_queue_addr_list if tup[1] == addr][0]
    idx = [i for i, val in enumerate(comp_queue_addr_list) if val == CQ_addr][0]
    
    out = (idx, CQ_addr[0])
    if DEBUGGING:
        assert type(out)    == tuple
        assert type(out[0]) == int
        assert type(out[1]) == bytearray
    return out


def delete_queue_addr_tuple_from_list( compress_queue_addr_list,
                                       addr
                                     ):
    if DEBUGGING:
        assert type(compress_queue_addr_list)    == list
        assert len( compress_queue_addr_list)     > 0
        assert type(addr)                        == tuple
        assert len( addr)                         > 0

    idx, _ = retrieve_compress_queue( compress_queue_addr_list, addr )
    compress_queue_addr_list.pop(idx)
    
    if DEBUGGING:
        assert type(compress_queue_addr_list) == list
        # compress_queue_addr_list can be empty
    return compress_queue_addr_list


def add_timestamp_addr_pair_to_list( timestamp_out_addr_list,
                                     addr
                                   ):
    if DEBUGGING:
        assert type(timestamp_out_addr_list)   == list
        # timestamp_out_addr_list can be empty
        assert type(addr)                      == tuple
        assert len( addr)                       > 0

    timestamp_out = 0
    timestamp_out_addr_list.append([timestamp_out ,addr])
    
    if DEBUGGING:
        assert type(timestamp_out_addr_list)        == list
        assert len( timestamp_out_addr_list)         > 0
        assert type(timestamp_out_addr_list[-1])    == list  # [t, addr] pair
        assert len( timestamp_out_addr_list[-1])    == 2     # pair
        assert type(timestamp_out_addr_list[-1][0]) == int   # timestamp_out
        assert type(timestamp_out_addr_list[-1][1]) == tuple # addr
        assert len( timestamp_out_addr_list[-1][1])  > 0
    return timestamp_out_addr_list


def retrieve_timestamp_out_addr_pair ( timestamp_out_addr_list,
                                       addr
                                     ):
    if DEBUGGING:
        assert type(timestamp_out_addr_list) == list
        assert len( timestamp_out_addr_list)  > 0
        assert type(addr)                    == tuple
        assert len( addr)                     > 0

    t_addr = [lst for lst in timestamp_out_addr_list if lst[1] == addr][0]
    idx = [i for i,val in enumerate(timestamp_out_addr_list) if val==t_addr][0]
    
    out = (idx, t_addr)
    if DEBUGGING:
        assert type(out)       == tuple
        assert type(out[0])    == int   # idx
        assert type(out[1])    == list  # [timestamp_out, addr]
        assert type(out[1][0]) == int   # timestamp_out
        assert type(out[1][1]) == tuple # addr 
        assert len( out[1][1])  > 0 
    return out


def increment_timestamp_out ( timestamp_addr          = None,
                              timestamp_out_addr_list = None,
                              addr                    = None
                            ):
    if (not timestamp_addr == None and not timestamp_out_addr_list == None):
        raise Exception("increment_timestamp_out(...) expects either just" + \
                        " the [timestamp, addr] pair or just the timestamp"+ \
                        " list. Do not give the function both things at once")

    elif not timestamp_addr == None:
        if DEBUGGING:
            assert type(timestamp_addr)          == list # [timestamp_out, addr]
            assert len( timestamp_addr)          == 2
            assert type(timestamp_addr[0])       == int   # timestamp_out
            assert type(timestamp_addr[1])       == tuple # addr
            assert len( timestamp_addr[1])        > 0
            assert type(timestamp_out_addr_list) == types.NoneType
            assert type(addr)                    == types.NoneType

        timestamp_addr[0] += 1

    elif not timestamp_out_addr_list == None:
        if DEBUGGING:
            assert type(timestamp_addr)                == types.NoneType
            assert type(timestamp_out_addr_list)       == list
            assert type(timestamp_out_addr_list[0])    == list  # [t, addr] pair
            assert len( timestamp_out_addr_list[0])    == 2
            assert type(timestamp_out_addr_list[0][0]) == int   # timestamp_out
            assert type(timestamp_out_addr_list[0][1]) == tuple # addr
            assert len( timestamp_out_addr_list[0][1])  > 0
            assert type(addr)                          == tuple
            assert len( addr)                           > 0

        idx, _ = retrieve_timestamp_out_addr_pair(timestamp_out_addr_list, addr)
        timestamp_out_addr_list[idx][0] += 1

    else:
        raise Exception("no parameters given to increment_timestamp_out(...)")


def delete_timestamp_addr_pair_from_list ( timestamp_out_addr_list,
                                           addr
                                         ):
    if DEBUGGING:
        assert type(timestamp_out_addr_list) == list
        assert len( timestamp_out_addr_list)  > 0
        assert type(addr)                    == tuple
        assert len( addr)                     > 0

    idx, _ = retrieve_timestamp_out_addr_pair( timestamp_out_addr_list, addr )
    timestamp_out_addr_list.pop(idx)
    
    if DEBUGGING:
        assert type(timestamp_out_addr_list) == list
        # timestamp_out_addr_list can be empty
    return timestamp_out_addr_list


def add_v_last_boxed_addr_tuple_to_list( v_last_boxed_addr_list,
                                         addr
                                       ):
    if DEBUGGING:
        assert type(v_last_boxed_addr_list) == list
        #           v_last_boxed_addr_list can be empty
        assert type(addr)                   == tuple
        assert len( addr)                    > 0

    v_last_boxed_addr_list.append( ([None], addr) )
    if DEBUGGING:
        assert type(v_last_boxed_addr_list)          == list
        assert len( v_last_boxed_addr_list)           > 0
        assert type(v_last_boxed_addr_list[0])       == tuple # ([v_last], addr)
        assert type(v_last_boxed_addr_list[0][0])    == list  #  [v_last]
        assert ((type(      v_last_boxed_addr_list[0][0][0]==types.NoneType)) or 
                (isinstance(v_last_boxed_addr_list[0][0][0], numbers.Number)))
        assert type(v_last_boxed_addr_list[0][1])    == tuple #  addr
        assert len( v_last_boxed_addr_list[0][1])     > 0
    return v_last_boxed_addr_list


def retrieve_v_last_boxed( v_last_boxed_addr_list,
                           addr
                         ):
    if DEBUGGING:
        assert   type(      v_last_boxed_addr_list)       == list
        assert   len(       v_last_boxed_addr_list)        > 0
        assert   type(      v_last_boxed_addr_list[0])    == tuple
        assert   type(      v_last_boxed_addr_list[0][0]) == list  # [v_last]
        assert ((type(      v_last_boxed_addr_list[0][0][0]==types.NoneType)) or 
                (isinstance(v_last_boxed_addr_list[0][0][0], numbers.Number)))
        assert   type(      v_last_boxed_addr_list[0][1]) == tuple #  addr
        assert   len(       v_last_boxed_addr_list[0][1])  > 0
        assert   type(      addr)                         == tuple
        assert   len(       addr)                          > 0

    vlb_a = [t for t in v_last_boxed_addr_list if t[1] == addr][0]
    idx   = [i for i, v in enumerate(v_last_boxed_addr_list) if v == vlb_a][0]
    
    out = (idx, vlb_a[0])
    if DEBUGGING:
        assert     type(      out)    == tuple
        assert     type(      out[0]) == int  # idx
        assert     type(      out[1]) == list # v_last_boxed ie [v_last]
        assert ( ( type(      out[1]  == types.NoneType) ) or 
                 ( isinstance(out[1],    numbers.Number) ) )
    return out


def delete_v_last_addr_tuple_from_list( v_last_boxed_addr_list,
                                        addr
                                      ):
    if DEBUGGING:
        assert   type(      v_last_boxed_addr_list)       == list
        assert   len(       v_last_boxed_addr_list)        > 0
        assert   type(      v_last_boxed_addr_list[0])    == tuple
        assert   type(      v_last_boxed_addr_list[0][0]) == list  # [v_last]
        assert ((type(      v_last_boxed_addr_list[0][0][0]==types.NoneType)) or 
                (isinstance(v_last_boxed_addr_list[0][0][0], numbers.Number)))
        assert   type(      v_last_boxed_addr_list[0][1]) == tuple #  addr
        assert   len(       v_last_boxed_addr_list[0][1])  > 0
        assert   type(      addr)                         == tuple
        assert   len(       addr)                          > 0

    idx, _ = retrieve_v_last_boxed( v_last_boxed_addr_list, addr )
    v_last_boxed_addr_list.pop(idx)

    if DEBUGGING:
        assert type(v_last_boxed_addr_list) == list
        #           v_last_boxed_addr_list can be empty
    return v_last_boxed_addr_list


def server_compress_callback( compress_queue,
                              timestamp_addr,
                              v_last_boxed,
                              sock,
                              addr,
                              datagram_sz,
                              chunk,
                              channels,
                              sample_format,
                              fs,
                              B,
                              t_a,  
                              t_r,     
                              T_C_log,       
                              T_L_log,    
                              R_b,
                              data_pad_str         = '__comp__',
                              after_timestamp_char = '&',
                              data_header_end_str  = '__'
                            ):
    if DEBUGGING:
        assert     type(      compress_queue)       == bytearray
        assert     type(      timestamp_addr)       == list
        assert     len(       timestamp_addr)       == 2
        assert     type(      timestamp_addr[0])    == int   # timestamp_out
        assert     type(      timestamp_addr[1])    == tuple # addr
        assert     len(       timestamp_addr[1])     > 0
        assert     type(      v_last_boxed)         == list  # [v_last]
        assert ( ( type(      v_last_boxed[0]       == types.NoneType ) ) or 
                 ( isinstance(v_last_boxed[0],         numbers.Number ) ) )
        assert     type(      sock)                 == socket.socket
        assert     type(      addr)                 == tuple
        assert     len(       addr)                  > 0
        assert     type(      datagram_sz)          == int
        assert     type(      chunk)                == int
        assert     type(      channels)             == int
        assert     type(      sample_format)        == int
        assert     type(      fs)                   == int
        assert     type(      B)                    == int
        assert     type(      t_a)                  == float
        assert     type(      t_r)                  == float
        assert     isinstance(T_C_log,                 numbers.Number)
        assert     isinstance(T_L_log,                 numbers.Number)
        assert                T_L_log                > T_C_log
        assert     isinstance(R_b,                     numbers.Number)
        assert                R_b                   >= 0
        assert     type(      data_pad_str)         == str
        assert     len(       data_pad_str)         == 8
        assert     type(      after_timestamp_char) == str
        assert     len(       after_timestamp_char) == 1
        assert     type(      data_header_end_str)  == str
        assert     len(       data_header_end_str)  == 2

    def callback( in_data,     # recorded data (ie this is None bc input=False)
                  frame_count, # number of frames
                  time_info,   # dictionary
                  status       # PaCallbackFlags
                ):
        if DEBUGGING:
            assert type(in_data)     == types.NoneType
            assert type(frame_count) == int
            assert type(time_info)   == dict
            assert type(status)      == int

        if len(compress_queue) >= datagram_sz:

            timestamp_out = timestamp_addr[0]
            s2c_header, header_sz = get_header_header_sz( data_pad_str, 
                                                          timestamp_out,
                                                          sample_format, 
                                                          channels,
                                                          after_timestamp_char,
                                                          data_header_end_str
                                                        )
            msg_sz = datagram_sz - header_sz
            s2c_msg_bytearray, v_last_new = compress_mono( 
                           bytes(compress_queue[:msg_sz]),
                                          v_last_boxed[0],
                                                    chunk,
                                            sample_format, 
                                                 channels,       
                                                       fs,   
                                                        B,       
                                                      t_a,  
                                                      t_r,     
                                                  T_C_log,       
                                                  T_L_log,    
                                                      R_b,       
                                                    False,
                                                     None,
                                                     None)
            del compress_queue[:msg_sz]

            s2c_data = s2c_header + bytes(s2c_msg_bytearray)
            sock.sendto(s2c_data, addr)

            increment_timestamp_out(timestamp_addr)
            v_last_boxed[0] = v_last_new

        sz = chunk * channels * pyaudio.get_sample_size(sample_format)
        callback_out_data = bytes(sz)

        out = (callback_out_data, pyaudio.paContinue)
        if DEBUGGING:
            assert type(out)    == tuple
            assert type(out[0]) == bytes
            assert type(out[1]) == int
        return out

    if DEBUGGING:
        assert type(callback) == types.FunctionType
    return callback


def comp_add_stream_addr_tuple_to_list( stream_addr_list,
                                        compress_queue_addr_list,
                                        timestamp_out_addr_list,
                                        v_last_boxed_addr_list,
                                        sock,
                                        addr,
                                        datagram_sz,
                                        pa,
                                        chunk,
                                        sample_format,
                                        channels,
                                        fs,
                                        B,
                                        t_a,  
                                        t_r,     
                                        T_C_log,       
                                        T_L_log,    
                                        R_b,
                                        data_pad_str         = '__comp__',
                                        after_timestamp_char = '&',
                                        data_header_end_str  = '__'
                                      ):
    if DEBUGGING:
        assert type(stream_addr_list)               == list
        # stream_addr_list can be empty
        assert type(compress_queue_addr_list)       == list
        assert len( compress_queue_addr_list)        > 0
        assert type(compress_queue_addr_list[0])    == tuple
        assert type(compress_queue_addr_list[0][0]) == bytearray # comprss_queue
        assert type(compress_queue_addr_list[0][1]) == tuple     # addr
        assert type(timestamp_out_addr_list)        == list
        assert type(timestamp_out_addr_list[0])     == list  # [t, addr] pair
        assert len( timestamp_out_addr_list[0])     == 2
        assert type(timestamp_out_addr_list[0][0])  == int   # timestamp_out
        assert type(timestamp_out_addr_list[0][1])  == tuple # addr
        assert len( timestamp_out_addr_list[0][1])   > 0
        assert type(v_last_boxed_addr_list)         == list
        assert len( v_last_boxed_addr_list)          > 0
        assert type(v_last_boxed_addr_list[0])      == tuple
        assert type(v_last_boxed_addr_list[0][0])   == list  # [v_last]
        assert ((type(      v_last_boxed_addr_list[0][0][0]==types.NoneType)) or 
                (isinstance(v_last_boxed_addr_list[0][0][0], numbers.Number)))
        assert type(v_last_boxed_addr_list[0][1])   == tuple #  addr
        assert len( v_last_boxed_addr_list[0][1])    > 0
        assert type(sock)                           == socket.socket
        assert type(addr)                           == tuple
        assert len( addr)                            > 0
        assert type(datagram_sz)                    == int
        assert type(pa)                             == pyaudio.PyAudio
        assert type(chunk)                          == int
        assert type(sample_format)                  == int
        assert type(channels)                       == int
        assert type(fs)                             == int
        assert type(B)                              == int
        assert type(t_a)                            == float
        assert type(t_r)                            == float
        assert isinstance(T_C_log,                     numbers.Number)
        assert isinstance(T_L_log,                     numbers.Number)
        assert            T_L_log                    > T_C_log
        assert isinstance(R_b,                         numbers.Number)
        assert            R_b                       >= 0
        assert type(data_pad_str)                   == str
        assert len( data_pad_str)                   == 8
        assert type(after_timestamp_char)           == str
        assert len( after_timestamp_char)           == 1
        assert type(data_header_end_str)            == str
        assert len( data_header_end_str)            == 2

    _, compress_queue = retrieve_compress_queue(compress_queue_addr_list, addr)
    _, timestamp_addr = retrieve_timestamp_out_addr_pair( 
                                 timestamp_out_addr_list,
                                                    addr)
    idx, _  = retrieve_v_last_boxed( v_last_boxed_addr_list, addr)
    stream = pa.open(format            = sample_format,
                     channels          = channels,
                     rate              = fs,
                     output            = True,
                     frames_per_buffer = chunk,
                     stream_callback   = server_compress_callback( 
                                                   compress_queue,
                                                   timestamp_addr,
                                   v_last_boxed_addr_list[idx][0],
                                                             sock,
                                                             addr,
                                                      datagram_sz,
                                                            chunk,
                                                         channels,
                                                    sample_format,
                                                               fs,
                                                                B,
                                                              t_a,  
                                                              t_r,     
                                                          T_C_log,       
                                                          T_L_log,    
                                                              R_b,
                                                     data_pad_str,
                                             after_timestamp_char,
                                              data_header_end_str)
                    )
    stream_addr_list.append( (stream, addr) )

    if DEBUGGING:
        assert type(stream_addr_list)        == list
        assert len( stream_addr_list)         > 0
        assert type(stream_addr_list[-1])    == tuple
        assert type(stream_addr_list[-1][0]) == pyaudio.Stream # stream
        assert type(stream_addr_list[-1][1]) == tuple          # addr
        assert len( stream_addr_list[-1][1])  > 0
    return stream_addr_list










