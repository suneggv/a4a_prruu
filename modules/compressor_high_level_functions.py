# ==============================================================================
# ==  IMPORTS  =================================================================
# ==============================================================================

# ~~ module access from main ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
import sys
sys.path.append('./modules')


# ~~ debugging ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
from debugging import DEBUGGING
if DEBUGGING:
    import os
    import types
    import numbers


# ~~ compression ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
import numpy as np
import math
from pydub import AudioSegment


# ~~ record / playback ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
import pyaudio
import time


# ~~ nparray_correct_byteorder(...) ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
import copy



# ==============================================================================
# ==  SUNK-LEVEL FUNCTIONS  ====================================================
# ==============================================================================

# ~~ sunk-level --> math operations ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

from compressor_sunk_level_functions import retrieve_max_audio_value
from compressor_sunk_level_functions import log_v_y_t
from compressor_sunk_level_functions import detect_level_mono
from compressor_sunk_level_functions import get_nparray_log_from_nparray
from compressor_sunk_level_functions import normalize_nparray_mono
from compressor_sunk_level_functions import compress_sample
from compressor_sunk_level_functions import fade_in_nparray_mono


# ~~ sunk-level --> converters ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

from compressor_sunk_level_functions import nparray_LR_2_nparrays_L_and_R
from compressor_sunk_level_functions import nparray_back_2_sample_format
from compressor_sunk_level_functions import nparray_correct_byteorder
from compressor_sunk_level_functions import nparray_mono_2_nparray_stereo



# ==============================================================================
# ==  MID-LEVEL FUNCTIONS  =====================================================
# ==============================================================================

# ~~ mid-level --> converters ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

from compressor_mid_level_functions import bytes_stereo_2_nparray_mono
from compressor_mid_level_functions import nparray_mono_2_bytearray_mono
from compressor_mid_level_functions import nparray_mono_2_bytearray_stereo


# ~~ mid-level --> diagnostics ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

from compressor_mid_level_functions import export_nparray_mono_to_wav


# ~~ mid-level --> audio procedures ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

from compressor_mid_level_functions import level_detector_nparray_mono
from compressor_mid_level_functions import compress_audio_nparray_mono



# ==============================================================================
# ==  HIGH-LEVEL FUNCTIONS  ====================================================
# ==============================================================================

# ~~ high-level --> turn non-DRC'd frames into yes-DRC'd frames ~~~~~~~~~~~~~~~~

def compress_mono( frames_in,
                   v_last_old       = None,
                   chunk            = 1024,    
                   sample_format    = pyaudio.paInt16, 
                   channels         = 2,       
                   fs               = 44100,   
                   B                = 1,       
                   t_a              = 0.0001,  
                   t_r              = 0.3,     
                   T_C_log          = 8,       
                   T_L_log          = 8.75,    
                   R_b              = 6,       
                   truncate         = False,
                   x_diagnose_boxed = None,
                   y_diagnose_boxed = None
                 ):
    if DEBUGGING:
        assert     type(      frames_in)        == bytes
        assert     type(      chunk)            == int
        assert     len(       frames_in)         > chunk
        assert ( ( type(      v_last_old        == types.NoneType) ) or 
                 ( isinstance(v_last_old,          numbers.Number) ) )
        #                     chunk's assert is above
        assert     type(      sample_format)    == int
        assert     type(      channels)         == int
        assert     type(      fs)               == int
        assert     type(      B)                == int
        assert     type(      t_a)              == float
        assert     type(      t_r)              == float
        assert     isinstance(T_C_log,             numbers.Number)
        assert     isinstance(T_L_log,             numbers.Number)
        assert                T_L_log            > T_C_log
        assert     isinstance(R_b,                 numbers.Number)
        assert                R_b               >= 0
        assert     type(      truncate)         == bool
        assert ( ( type(      x_diagnose_boxed) == types.NoneType ) or
                 ( type(      x_diagnose_boxed) == list           ) )
        assert ( ( type(      y_diagnose_boxed) == types.NoneType ) or
                 ( type(      y_diagnose_boxed) == list           ) )

    beta_a = math.exp(-1/(t_a*fs))
    beta_r = math.exp(-1/(t_r*fs))
    x_t_b = bytes_stereo_2_nparray_mono( frames_in,
                                         chunk,
                                         sample_format,
                                         channels,
                                         fs,
                                         truncate
                                       )
    v_x_t_b, v_last_new = level_detector_nparray_mono( x_t_b,
                                                       beta_a,
                                                       beta_r,
                                                       T_C_log,
                                                       v_last_old,
                                                       sample_format
                                                     )
    y_t_b = compress_audio_nparray_mono( x_t_b,
                                         v_x_t_b,
                                         T_C_log,
                                         T_L_log,
                                         R_b,
                                         chunk,
                                         sample_format,
                                         truncate,
                                         v_last_old
                                       )
    frames_out_bytearray = nparray_mono_2_bytearray_stereo( y_t_b,
                                                            sample_format
                                                          )
    if x_diagnose_boxed is not None:
        
        if len(x_diagnose_boxed[0]) <= 10 * fs:
            x_diagnose_boxed[0] = np.append(x_diagnose_boxed[0], x_t_b)

    if y_diagnose_boxed is not None:
        
        if len(y_diagnose_boxed[0]) <= 10 * fs:
            y_diagnose_boxed[0] = np.append(y_diagnose_boxed[0], y_t_b)

    out = (frames_out_bytearray, v_last_new)
    if DEBUGGING:
        assert     type(           out)    == tuple
        assert     type(           out[0]) == bytearray # frames_out_bytearray
        if truncate:
            assert len(            out[0])  < len(frames_in) 
        else:
            assert len(            out[0]) == len(frames_in) 
        assert     isinstance(     out[1],    numbers.Number) # v_last_new
        assert not issubclass(type(out[1]),   np.integer)
        assert                     out[1]   > 0
    return out


# ~~ high-level --> pyaudio callback (thr 1 in --> comp / thr 2 comp --> out) ~~

def callback_in_DRC_queue( playback_queue,
                           x_diagnose_boxed,
                           y_diagnose_boxed,
                           v_last_boxed,
                           chunk         = 1024,    
                           sample_format = pyaudio.paInt16, 
                           channels      = 2,       
                           fs            = 44100,   
                           B             = 1,       
                           t_a           = 0.0001,  
                           t_r           = 0.3,     
                           T_C_log       = 8,       
                           T_L_log       = 8.75,    
                           R_b           = 6,       
                           truncate      = False,
                           export_all    = False
                         ):
    if DEBUGGING:
        assert     type(      playback_queue)              == bytearray
        assert     type(      x_diagnose_boxed)            == list
        assert     len(       x_diagnose_boxed)            == 1
        assert     type(      x_diagnose_boxed[0])         == np.ndarray
        assert     issubclass(x_diagnose_boxed[0].dtype.type, numbers.Number)
        assert not issubclass(x_diagnose_boxed[0].dtype.type, np.integer)
        assert     type(      fs)                          == int
        assert     len(       x_diagnose_boxed[0])         <= 10 * fs
        assert     type(      y_diagnose_boxed)            == list
        assert     len(       y_diagnose_boxed)            == 1
        assert     issubclass(y_diagnose_boxed[0].dtype.type, numbers.Number)
        assert not issubclass(y_diagnose_boxed[0].dtype.type, np.integer)
        assert     len(       y_diagnose_boxed[0])         <= 10 * fs
        assert     type(      v_last_boxed)                == list
        assert     len(       v_last_boxed)                == 1
        assert ( ( type(      v_last_boxed[0]              == types.NoneType))or 
                 ( isinstance(v_last_boxed[0],                numbers.Number)) )
        assert     type(      chunk)                       == int
        assert     type(      sample_format)               == int
        assert     type(      channels)                    == int
        #                     fs's assert written above
        assert     type(      B)                           == int
        assert     type(      t_a)                         == float
        assert     type(      t_r)                         == float
        assert     isinstance(T_C_log,                        numbers.Number)
        assert     isinstance(T_L_log,                        numbers.Number)
        assert                T_L_log                       > T_C_log
        assert     isinstance(R_b,                            numbers.Number)
        assert                R_b                          >= 0
        assert     type(      truncate)                    == bool
        assert     type(      export_all)                  == bool

    def callback( in_data,     # recorded data
                  frame_count, # number of frames
                  time_info,   # dictionary
                  status       # PaCallbackFlags
                ):
        if DEBUGGING:
            assert type(in_data)     == bytes
            assert type(frame_count) == int
            assert type(time_info)   == dict
            assert type(status)      == int

        frames_out_bytearray = None
        v_last_new           = None
        if export_all:
            frames_out_bytearray, v_last_new = compress_mono( in_data,
                                                              v_last_boxed[0],
                                                              chunk,
                                                              sample_format, 
                                                              channels,       
                                                              fs,   
                                                              B,       
                                                              t_a,  
                                                              t_r,     
                                                              T_C_log,       
                                                              T_L_log,    
                                                              R_b,       
                                                              truncate,
                                                              x_diagnose_boxed,
                                                              y_diagnose_boxed
                                                            )
        else:
            frames_out_bytearray, v_last_new = compress_mono( in_data,
                                                              v_last_boxed[0],
                                                              chunk,
                                                              sample_format, 
                                                              channels,       
                                                              fs,   
                                                              B,       
                                                              t_a,  
                                                              t_r,     
                                                              T_C_log,       
                                                              T_L_log,    
                                                              R_b,       
                                                              truncate,
                                                              None,
                                                              None
                                                            )

        v_last_boxed[0] = v_last_new
        playback_queue.extend(frames_out_bytearray)

        out = (in_data, pyaudio.paContinue)
        if DEBUGGING:
            assert type(out)    == tuple
            assert type(out[0]) == bytes
            assert type(out[1]) == int
        return out

    if DEBUGGING:
        assert type(callback) == types.FunctionType
    return callback


def callback_queue_out( playback_queue,
                        chunk         = 1024,    
                        sample_format = pyaudio.paInt16, 
                        channels      = 2
                      ):
    if DEBUGGING:
        assert type(playback_queue) == bytearray
        assert type(chunk)          == int
        assert type(sample_format)  == int
        assert type(channels)       == int

    def callback( in_data,     # recorded data
                  frame_count, # number of frames
                  time_info,   # dictionary
                  status       # PaCallbackFlags
                ):
        if DEBUGGING:
            assert type(in_data)     == types.NoneType
            assert type(frame_count) == int
            assert type(time_info)   == dict
            assert type(status)      == int

        out_data = bytearray()
        sz = chunk * channels * pyaudio.get_sample_size(sample_format)

        if len(playback_queue) >= sz:
            out_data = playback_queue[:sz]
            del playback_queue[:sz]

        else:
            out_data = bytes(sz)

        out = (bytes(out_data), pyaudio.paContinue)
        if DEBUGGING:
            assert type(out)    == tuple
            assert type(out[0]) == bytes
            assert len( out[0]) == sz
            assert type(out[1]) == int
        return out

    if DEBUGGING:
        assert type(callback) == types.FunctionType
    return callback


def pyaudio_DRC_stream_manager( chunk         = 1024,
                                sample_format = pyaudio.paInt16, 
                                channels      = 2,      
                                fs            = 44100,  
                                B             = 1,      
                                t_a           = 0.0001, 
                                t_r           = 0.3,    
                                T_C_log       = 8,      
                                T_L_log       = 8.75,   
                                R_b           = 6,      
                                truncate      = False,  
                                export_all    = False
                              ):
    if DEBUGGING:
        assert type(      chunk)         == int
        assert type(      sample_format) == int
        assert type(      channels)      == int
        assert type(      fs)            == int
        assert type(      B)             == int
        assert isinstance(t_a,              numbers.Number)
        assert isinstance(t_r,              numbers.Number)
        assert isinstance(T_C_log,          numbers.Number)
        assert isinstance(T_L_log,          numbers.Number)
        assert            T_L_log         > T_C_log
        assert isinstance(R_b,              numbers.Number)
        assert            R_b            >= 0
        assert type(      truncate)      == bool
        assert type(      export_all)    == bool

    p = pyaudio.PyAudio()

    # -- 1. Prepare input --> DRC --> queue audio environment ------------------

    v_last_boxed      = [None]
    playback_queue    = bytearray()
    x_diagnose_boxed  = [np.array([])]
    y_diagnose_boxed  = [np.array([])]
    stream_in         = p.open( format            = sample_format,
                                channels          = channels,
                                rate              = fs,
                                input             = True,
                                start             = False,
                                frames_per_buffer = chunk,
                                stream_callback   = callback_in_DRC_queue( 
                                                           playback_queue,
                                                         x_diagnose_boxed,
                                                         y_diagnose_boxed,
                                                             v_last_boxed,
                                                                    chunk,    
                                                            sample_format, 
                                                                 channels,       
                                                                       fs,   
                                                                        B,       
                                                                      t_a,  
                                                                      t_r,     
                                                                  T_C_log,       
                                                                  T_L_log,    
                                                                      R_b,       
                                                                 truncate,
                                                              export_all)
                      )

    # -- 2. Prepare queue --> output audio environment -------------------------

    stream_out = p.open( format            = sample_format,
                         channels          = channels,
                         rate              = fs,
                         output            = True,
                         start             = False,
                         frames_per_buffer = chunk,
                         stream_callback   = callback_queue_out( playback_queue,
                                                                 chunk,    
                                                                 sample_format, 
                                                                 channels
                                                               )
                       )

    # -- 3. Start streams and keep them running --------------------------------

    stream_in.start_stream()
    stream_out.start_stream()

    print("COMPRESSOR: Running. To stop, press Ctrl+c")

    while True:
        try:
            time.sleep(0.1)
        except KeyboardInterrupt:
            break

    # -- 4. Close streams and exit function ------------------------------------

    if export_all:
        export_nparray_mono_to_wav(x_diagnose_boxed[0],
                                   "x_10_sec.wav",
                                   sample_format,
                                   fs)
        export_nparray_mono_to_wav(y_diagnose_boxed[0],
                                   "y_10_sec.wav",
                                   sample_format,
                                   fs)

    stream_in.stop_stream()
    stream_in.close()

    stream_out.stop_stream()
    stream_out.close()

    p.terminate()
