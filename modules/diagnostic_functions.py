from debugging import DEBUGGING

if DEBUGGING:
    import sys
    import linecache



# ==============================================================================
# ==  DIAGNOSTIC FUNCTIONS  ====================================================
# ==============================================================================

def assert_fo_in( fo_in ): # reduces importation scope of private module _io
    if DEBUGGING:
        import _io
        assert type(fo_in) == _io.BufferedReader
    else:
        print('SERVER: How the hell did you get here?')


def assert_fo_out( fo_out ): # reduces importation scope of private module _io
    if DEBUGGING:
        import _io
        assert type(fo_out) == _io.BufferedWriter
    else:
        print('SERVER: How the hell did you get here?')


if DEBUGGING: 
    def PrintException(): # function credit: Apogentus @ stackoverflow.com
        exc_type, exc_obj, tb = sys.exc_info()
        if type(exc_obj) == AssertionError:
            exc_obj = 'assertion error'
        f                     = tb.tb_frame
        lineno                = tb.tb_lineno
        filename              = f.f_code.co_filename
        linecache.checkcache(filename)
        filename              = filename.split('/')[-1]
        line                  = linecache.getline(filename, lineno, f.f_globals)
        print('EXCEPTION IN:\n\t{}\n\tln. {} --> {}\n\n\t{}\n'.format( filename,
                                                                         lineno,
                                                                   line.strip(),
                                                                        exc_obj)
             )