# ==============================================================================
# ==  IMPORTS  =================================================================
# ==============================================================================

# ~~ debugging ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
from debugging import DEBUGGING
if DEBUGGING:
    import os
    import types
    import numbers


# ~~ compression ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
import numpy as np
import math
from pydub import AudioSegment


# ~~ record / playback ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
import pyaudio
import time


# ~~ nparray_correct_byteorder(...) ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
import sys
import copy



# ==============================================================================
# ==  SUNK-LEVEL FUNCTIONS  ====================================================
# ==============================================================================

# ~~ sunk-level --> math operations ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

from compressor_sunk_level_functions import retrieve_max_audio_value
from compressor_sunk_level_functions import log_v_y_t
from compressor_sunk_level_functions import detect_level_mono
from compressor_sunk_level_functions import get_nparray_log_from_nparray
from compressor_sunk_level_functions import get_max_compressed_value
from compressor_sunk_level_functions import normalize_nparray_mono
from compressor_sunk_level_functions import brickwall_factor
from compressor_sunk_level_functions import brickwall_nparray_mono
from compressor_sunk_level_functions import compress_sample
from compressor_sunk_level_functions import fade_in_nparray_mono


# ~~ sunk-level --> converters ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

from compressor_sunk_level_functions import nparray_LR_2_nparrays_L_and_R
from compressor_sunk_level_functions import nparray_back_2_sample_format
from compressor_sunk_level_functions import nparray_correct_byteorder
from compressor_sunk_level_functions import nparray_mono_2_nparray_stereo



# ==============================================================================
# ==  MID-LEVEL FUNCTIONS  =====================================================
# ==============================================================================

# ~~ mid-level --> converters ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

def bytes_stereo_2_nparray_mono( frames,
                                 chunk         = 1024,
                                 sample_format = pyaudio.paInt16,
                                 channels      = 2,
                                 fs            = 44100,
                                 truncate      = True
                               ):
    if DEBUGGING:
        assert type(frames)        == bytes
        assert sum( frames)        != 0
        assert type(chunk)         == int
        assert len( frames)         > chunk
        assert type(sample_format) == int
        assert type(channels)      == int
        assert type(fs)            == int
        assert type(truncate)      == bool

    sound = AudioSegment( data        = frames,
                          sample_width= pyaudio.get_sample_size(sample_format),
                          frame_rate  = fs,
                          channels    = channels
                        )

    x_t_LR   = np.array(sound.get_array_of_samples())    
    x_t_L, _ = nparray_LR_2_nparrays_L_and_R( x_t_LR )
    x_t_b    = x_t_L.astype('float64') # converted to float64 so the squares in 
                                       # the level detection have more room for 
                                       # manoeuver
    if truncate:
        x_t_b = x_t_b[chunk:]

    sample_width_bytes = None
    if DEBUGGING:
        _, sample_width_bytes, _ = retrieve_max_audio_value( sample_format )

    if DEBUGGING:
        assert     type(           x_t_b)  == np.ndarray
        assert len(x_t_b)==len(frames)/sample_width_bytes/2-int(truncate)*chunk
        assert     isinstance(     x_t_b[0],  numbers.Number)
        assert not issubclass(type(x_t_b[0]), np.integer)
    return x_t_b


def nparray_mono_2_bytearray_mono( y_t_b,
                                   sample_format = pyaudio.paInt16
                                 ):
    if DEBUGGING:
        assert     type(           y_t_b)         == np.ndarray
        assert     len(            y_t_b)          > 0
        assert     isinstance(     y_t_b[0],         numbers.Number)
        assert not issubclass(type(y_t_b[0]),        np.integer)
        assert     type(           sample_format) == int

    y_t_b_int     = nparray_back_2_sample_format( y_t_b, sample_format )
    y_t_b_int_out = nparray_correct_byteorder(    y_t_b_int )
    
    sample_width_bytes_out = None
    if DEBUGGING:
        _, sample_width_bytes_out, _ = retrieve_max_audio_value( sample_format )

    frames_out = bytearray(y_t_b_int_out.ravel().view('b').data.tobytes())
    if DEBUGGING:
        assert type(frames_out) == bytearray
        assert len( frames_out) == len(y_t_b) * sample_width_bytes_out * 1
        assert sum( frames_out) != 0
    return frames_out


def nparray_mono_2_bytearray_stereo( y_t_b,
                                     sample_format = pyaudio.paInt16
                                   ):
    if DEBUGGING:
        assert     type(           y_t_b)         == np.ndarray
        assert     len(            y_t_b)          > 0
        assert     isinstance(     y_t_b[0],         numbers.Number)
        assert not issubclass(type(y_t_b[0]),        np.integer)
        assert     type(           sample_format) == int

    y_t_b_int        = nparray_back_2_sample_format(  y_t_b, sample_format )
    y_t_b_int_LR     = nparray_mono_2_nparray_stereo( y_t_b_int )
    y_t_b_int_LR_out = nparray_correct_byteorder(     y_t_b_int_LR )
    
    sample_width_bytes_out = None
    if DEBUGGING:
        _, sample_width_bytes_out, _ = retrieve_max_audio_value( sample_format )
    
    frames_out = bytearray(y_t_b_int_LR_out.ravel().view('b').data.tobytes())
    if DEBUGGING:
        assert type(frames_out) == bytearray
        assert len( frames_out) == len(y_t_b) * sample_width_bytes_out * 2
    return frames_out


# ~~ mid-level --> diagnostics ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

def export_nparray_mono_to_wav( nparray,
                                file_name,
                                sample_format = pyaudio.paInt16,
                                fs            = 44100
                              ):
    if DEBUGGING:
        assert type(               nparray)       == np.ndarray
        assert type(               fs)            == int
        assert len(                nparray)       >= 10 * fs
        assert isinstance(         nparray[0],       numbers.Number)
        assert not issubclass(type(nparray[0]),      np.integer)
        assert type(               file_name)     == str
        assert len(                file_name)      > 4
        assert                     file_name[-4:]  == '.wav'
        assert type(               sample_format) == int
        #                          fs's assertion defined above

    file_nparray = nparray_back_2_sample_format(nparray, sample_format)
    file_audio   = AudioSegment( data         = file_nparray.tobytes(),
                                 sample_width = pyaudio.get_sample_size(  
                                                          sample_format),
                                 frame_rate   = fs,
                                 channels     = 1
                               )
    file_audio.export(file_name, format = "wav")


# ~~ mid-level --> audio procedures ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

def level_detector_nparray_mono( x_t_b,
                                 beta_a,
                                 beta_r,
                                 T_C_log,
                                 v_last_old    = None,
                                 sample_format = pyaudio.paInt16
                               ):
    if DEBUGGING:
        assert     type(           x_t_b)         == np.ndarray
        assert     len(            x_t_b)          > 0
        assert     isinstance(     x_t_b[0],         numbers.Number)
        assert not issubclass(type(x_t_b[0]),        np.integer)
        assert     type(           beta_a)        == float
        assert                     beta_a          > 0
        assert     type(           beta_r)        == float
        assert                     beta_r          > 0
        assert ( ( type(           v_last_old)    == types.NoneType ) or
                 ( type(           v_last_old)    == type(x_t_b[0]) ) )
        assert     type(           sample_format) == int

    v_x_t_b   = np.zeros(len(x_t_b)).astype('float64')
    if v_last_old is None:
        max_audio_value, _, _ = retrieve_max_audio_value( sample_format )
        v_x_t_b[0]            = math.pow(max_audio_value, 2)
    else:
        v_x_t_b[0] = v_last_old

    for t in range(1,len(x_t_b)):
        v_x_t_b[t] = detect_level_mono( x_t_b[t], 
                                        v_x_t_b[t-1], 
                                        beta_a, 
                                        beta_r,
                                        T_C_log
                                      )
    v_last_new = v_x_t_b[-1]

    if DEBUGGING:
        assert type(           v_x_t_b)           == type(x_t_b)
        assert len(            v_x_t_b)           == len( x_t_b)
        assert len([i for i in v_x_t_b if i > 0]) == len( v_x_t_b)
        assert type(           v_last_new)        == type(v_x_t_b[0])
        assert                 v_last_new          > 0
    return (v_x_t_b, v_last_new)


def compress_audio_nparray_mono( x_t_b,
                                 v_x_t_b,
                                 T_C_log       = 8,
                                 T_L_log       = 8.75,
                                 R_b           = 8,
                                 chunk         = 1024,
                                 sample_format = pyaudio.paInt16,
                                 truncate      = True,
                                 v_last_old    = None
                               ):
    if DEBUGGING:
        assert     type(           x_t_b)             == np.ndarray
        assert     len(            x_t_b)              > 0
        assert     isinstance(     x_t_b[0],             numbers.Number)
        assert not issubclass(type(x_t_b[0]),            np.integer)
        assert     type(           v_x_t_b)           == type(x_t_b)
        assert     len(            v_x_t_b)           == len( x_t_b)
        assert     len([i for i in v_x_t_b if i > 0]) == len( v_x_t_b)
        assert     type(           v_x_t_b[0])        == type(x_t_b[0])
        assert     isinstance(     T_C_log,              numbers.Number)
        assert     isinstance(     T_L_log,              numbers.Number)
        assert                     T_L_log             > T_C_log
        assert     isinstance(     R_b,                  numbers.Number)
        assert                     R_b                >= 0
        assert     type(           chunk)             == int
        assert     type(           sample_format)     == int
        assert     type(           truncate)          == bool
        assert ( ( type(           v_last_old)        == types.NoneType   ) or
                 ( type(           v_last_old)        == type(v_x_t_b[0]) ) )

    y_t_b  = np.zeros(len(x_t_b))

    for t in range(len(y_t_b)): # FUTURE WORK, PASS A FUNCTION AS PARAM FOR HERE 
        y_t_b[t] = compress_sample( x_t_b[t],
                                    v_x_t_b[t],
                                    T_C_log,
                                    T_L_log, 
                                    R_b
                                  )
    if truncate:
        y_t_b = y_t_b[chunk:]
    
    if v_last_old is None:
        y_t_b = fade_in_nparray_mono(y_t_b, chunk*2)

    y_t_b = normalize_nparray_mono( y_t_b, T_C_log, sample_format )
    y_t_b = brickwall_nparray_mono( y_t_b, sample_format )

    if DEBUGGING:
        assert type(y_t_b) == type(x_t_b)
        assert len( y_t_b) == len( x_t_b) - int(truncate) * chunk
    return y_t_b
