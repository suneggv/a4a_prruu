from debugging import DEBUGGING

import socket
import time
import random
import pyaudio
import threading



# ==============================================================================
# ==  DIAGNOSTIC FUNCTIONS  ====================================================
# ==============================================================================

from diagnostic_functions import assert_fo_in
from diagnostic_functions import assert_fo_out
from diagnostic_functions import PrintException



# ==============================================================================
# ==  MID-LEVEL FUNCTIONS  =====================================================
# ==============================================================================

from client_mid_level_functions import get_header_header_sz
from client_mid_level_functions import get_header_header_sz_from_datagram
from client_mid_level_functions import get_header_sz_from_timestamp
from client_mid_level_functions import get_msg_sz_from_timestamp
from client_mid_level_functions import get_timestamp_from_header


# ~~ downloading ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

from client_mid_level_functions import retrieve_pidges_entry
from client_mid_level_functions import make_zeropad_pidges_entry
from client_mid_level_functions import make_zeropad_pidges
from client_mid_level_functions import update_pidges
from client_mid_level_functions import edit_pidge


# ~~ dumping pidges ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

from client_mid_level_functions import retrieve_last_nonempty_pidges_entry
from client_mid_level_functions import retrieve_data_from_last_pidge


# ~~ scrambling ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

from client_mid_level_functions import make_scrambled_timestamp_sequence
from client_mid_level_functions import get_expected_timestamps_s2c
from client_mid_level_functions import write_msg_line_for_scrambled_file
from client_mid_level_functions import write_msg_for_scrambled_file
from client_mid_level_functions import write_scrambled_file
from client_mid_level_functions import schrodingers_file_parser


# ~~ pyaudio ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

from client_mid_level_functions import client_callback_out
from client_mid_level_functions import client_callback_in
from client_mid_level_functions import remove_holes_from_timestamp_pidge



# ==============================================================================
# ==  HIGH-LEVEL FUNCTIONS  ====================================================
# ==============================================================================

# ~~ Navigate the filesystem ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

def client_ls_pwd_cd( sock, 
                      datagram_sz
                    ):
    if DEBUGGING:
        assert type(sock)        == socket.socket
        assert type(datagram_sz) == int

    data = sock.recv(datagram_sz)
    print( data.decode('utf-8') )

# ~~ Download file from the server onto the client ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

def client_file_transfer_s2c_write( pidges, 
                                    fo_out
                                  ):
    if DEBUGGING:
        assert type(pidges)       == list
        assert len( pidges)        > 0
        assert type(pidges[0])    == tuple     # pidges entry
        assert type(pidges[0][0]) == int       # lowest_timestamp
        assert type(pidges[0][1]) == bytearray # pidge
        assert_fo_out(fo_out)      # _io.BufferedWriter

    _, pidge = pidges.pop(0)
    fo_out.write(bytes(pidge))


def client_file_transfer_s2c_copy( datagram_sz,
                                   timestamp,
                                   n_msgs_per_pidge,
                                   msg,
                                   pidges,
                                   sample_format = pyaudio.paInt16, 
                                   channels      = 2
                                 ):
    if DEBUGGING:
        assert type(datagram_sz)      == int
        assert type(timestamp)        == int
        assert type(n_msgs_per_pidge) == int
        assert type(msg)              == bytes
        assert type(pidges)           == list
        assert len( pidges)            > 0
        assert type(pidges[0])        == tuple     # pidges entry
        assert type(pidges[0][0])     == int       # lowest_timestamp
        assert type(pidges[0][1])     == bytearray # pidge
        assert type(sample_format)    == int
        assert type(channels)         == int

    idx_pidge, _  = retrieve_pidges_entry( pidges,
                                           timestamp,
                                           n_msgs_per_pidge
                                         )
    msg_sz         = get_msg_sz_from_timestamp(datagram_sz, 
                                               timestamp,
                                               sample_format, 
                                               channels
                                              )
    curr_msg_sz    = len(msg) # within a pidge, every message will be the 
                              # same size except the last message in the 
                              # file, which can be smaller. This accounts 
                              # for it 
    start_position = (timestamp % n_msgs_per_pidge) * msg_sz
    end_position   = start_position + curr_msg_sz
    edit_pidge( pidges, 
                idx_pidge, 
                start_position,
                end_position,
                msg )


def client_file_transfer_s2c_dump( pidges, 
                                   fo_out, 
                                   n_msgs_per_pidge, 
                                   datagram_sz
                                 ):
    if DEBUGGING:
        assert type(pidges)           == list
        assert len( pidges)            > 0
        assert type(pidges[0])        == tuple     # pidges entry
        assert type(pidges[0][0])     == int       # lowest_timestamp
        assert type(pidges[0][1])     == bytearray # pidge
        assert_fo_out(fo_out)          # _io.BufferedWriter
        assert type(n_msgs_per_pidge) == int
        assert type(datagram_sz)      == int

    idx, last_nonempty_timestamp_pidge = retrieve_last_nonempty_pidges_entry(
                                                                      pidges)
    _, last_nonempty_pidge = last_nonempty_timestamp_pidge

    for i in range(idx):
        _, pidge = pidges.pop(0)
        fo_out.write(bytes(pidge))

    last_data = bytes(retrieve_data_from_last_pidge(last_nonempty_pidge))
    fo_out.write(last_data)


def client_file_transfer_s2c( sock, 
                              cmd_str, 
                              datagram_sz,
                              n_msgs_per_pidge, 
                              n_entries_per_pidges,
                              sample_format, 
                              channels,
                              after_timestamp_char = '&',
                              data_header_end_str  = '__'
                            ):
    if DEBUGGING:
        assert type(sock)                 == socket.socket
        assert type(cmd_str)              == str
        assert type(datagram_sz)          == int
        assert type(n_msgs_per_pidge)     == int
        assert type(n_entries_per_pidges) == int
        assert type(sample_format)        == int
        assert type(channels)             == int
        assert type(after_timestamp_char) == str
        assert len( after_timestamp_char) == 1
        assert type(data_header_end_str)  == str
        assert len( data_header_end_str)  == 2

    datagram = sock.recv(datagram_sz) # recv one socket to confirm it's working
                                      # 2 cases:
                                      #     Awful --> server failed to download
                                      #     Great --> server is downloading

    if datagram[:8].decode('utf-8') == '__fail__':

        # Awful --> server failed to download
        print('SERVER: ...file not found...')

    else:

        # Great --> server is downloading
        print('CLIENT: Downloading the file now')

        # -- 1. Prepare "environment" for download 
        file_name         = 'downloaded_' + cmd_str[9:]
        minimum_timestamp = 0
        largest_timestamp = (n_msgs_per_pidge * n_entries_per_pidges) - 1
        pidges            = make_zeropad_pidges( n_msgs_per_pidge,
                                                 n_entries_per_pidges,
                                                 datagram_sz,
                                                 sample_format, 
                                                 channels
                                               )
        # -- 2. Open file
        fo_out = open( file_name, 'ab' ) # 'ab' --> mode = append, binary

        # -- 3. Receive datagrams, correctly place them, write if appropriate
        while datagram[:8].decode('utf-8') != '__done__':

            header, header_sz = get_header_header_sz_from_datagram(
                                                          datagram,
                                               data_header_end_str)
            timestamp = get_timestamp_from_header(header,
                                                  after_timestamp_char,
                                                  data_header_end_str)
            print("CLIENT: Received datagram w/ timestamp ", timestamp)

            if   timestamp < minimum_timestamp:
                print("CLIENT: Got timestamp lower than minimum. Ignoring.")

            else:
                while timestamp > largest_timestamp:
                    # 1. pop oldest pidge from pidges into file
                    client_file_transfer_s2c_write(pidges, fo_out)
                    # 2. add new pidge, w/ timestamp of largest_timestamp + 1
                    update_pidges( pidges, 
                                   largest_timestamp + 1, 
                                   n_msgs_per_pidge,
                                   datagram_sz,
                                   sample_format, 
                                   channels
                                 )
                    # 3. update value of largest_timestamp and minimum_timestamp
                    minimum_timestamp += n_msgs_per_pidge
                    largest_timestamp += n_msgs_per_pidge

                client_file_transfer_s2c_copy( datagram_sz,
                                               timestamp,
                                               n_msgs_per_pidge,
                                               datagram[header_sz:],
                                               pidges,
                                               sample_format, 
                                               channels
                                             )

            datagram = sock.recv(datagram_sz)

        # -- 4. Dump remaining pidges into file 
        client_file_transfer_s2c_dump( pidges, 
                                       fo_out, 
                                       n_msgs_per_pidge, 
                                       datagram_sz
                                     )

        # -- 5. close file
        fo_out.close()
        print('CLIENT: ...file downloaded...')



# ~~ Upload file from the client onto the server ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

def client_file_transfer_c2s( sock, 
                              cmd_str, 
                              datagram_sz, 
                              sleep_time,
                              transfer_data_pad_str = '__okay__',
                              transfer_stop_flag    = '__done__',
                              ready_flag            = '__redy__',
                              sample_format         = pyaudio.paInt16,
                              channels              = 2,
                              after_timestamp_char  = '&',
                              data_header_end_str   = '__'
                            ):
    if DEBUGGING:
        assert type(sock)                  == socket.socket
        assert type(cmd_str)               == str
        assert type(datagram_sz)           == int
        assert type(sleep_time)            == float
        assert type(transfer_data_pad_str) == str
        assert len( transfer_data_pad_str) == 8
        assert type(transfer_stop_flag)    == str
        assert len( transfer_stop_flag)    == 8
        assert type(ready_flag)            == str
        assert len( ready_flag)            == 8
        assert type(sample_format)         == int
        assert type(channels)              == int
        assert type(after_timestamp_char)  == str
        assert len( after_timestamp_char)  == 1
        assert type(data_header_end_str)   == str
        assert len( data_header_end_str)   == 2

    file_name_in  =  cmd_str[7:]
    file_name_out = 'NO FILE NAME :C'

    try:
        fo_in = open(file_name_in, 'rb') # 'rb' --> mode = read only, binary

        timestamp              = 0                          
        data_header, header_sz = get_header_header_sz(transfer_data_pad_str, 
                                                      timestamp,
                                                      sample_format, 
                                                      channels,
                                                      after_timestamp_char,
                                                      data_header_end_str
                                                     )
        msg_sz   = datagram_sz - header_sz
        data_msg = fo_in.read(msg_sz)
        data     = data_header + data_msg
        # DONE preparing first datagram

        sock.send(bytes(cmd_str,'utf-8')) # send cmd_str only once file is found

        while True: # wait for server's ack

            ack = sock.recv(datagram_sz)

            if ack[:8].decode('utf-8')[:8] == ready_flag:

                file_name_out = ack[8:].decode('utf-8')
                print('CLIENT: server is ready. Beginning upload')
                while len(data) > header_sz:

                    sock.send(data) # sending previous datagram
                    print('CLIENT: successfully sent datagram ', timestamp)
                    time.sleep(sleep_time)

                    timestamp += 1 # preparing new header
                    data_header, header_sz = get_header_header_sz(
                                            transfer_data_pad_str, 
                                                        timestamp,
                                                    sample_format, 
                                                         channels,
                                             after_timestamp_char,
                                              data_header_end_str)
                    msg_sz   = datagram_sz - header_sz
                    data_msg = fo_in.read(msg_sz)
                    data     = data_header + data_msg

                fo_in.close()
                sock.send( bytes(transfer_stop_flag, 'utf-8') )
                print('CLIENT: finished upload')
                break

            else:
                print('CLIENT: ...server is out of sync with the client...')
                break

    except KeyboardInterrupt:
        print('\nCLIENT: Interrupted upload')
        sock.send( bytes(transfer_stop_flag, 'utf-8') )
        file_name_out = '  INTERRUPT  '

    except AssertionError:
        if DEBUGGING:
            PrintException()
        else:
            print("SERVER: How did you even get an assertion error?")
    except:
        print('CLIENT: ...file not found...')
        if DEBUGGING:
            PrintException()

    if DEBUGGING:
        assert type(file_name_out) == str
        assert len( file_name_out)  > 0
    return file_name_out


# ~~ Upload (c --> s) the same file several times at random intervals ~~~~~~~~~~

def client_annoy( sock, 
                  cmd_str, 
                  datagram_sz,
                  transfer_data_pad_str = '__okay__',
                  transfer_stop_flag    = '__done__',
                  ready_flag            = '__redy__',
                  sample_format         = pyaudio.paInt16,
                  channels              = 2,
                  after_timestamp_char  = '&',
                  data_header_end_str   = '__'
                ):
    if DEBUGGING:
        assert type(sock)                  == socket.socket
        assert type(cmd_str)               == str
        assert type(datagram_sz)           == int
        assert type(transfer_data_pad_str) == str
        assert len( transfer_data_pad_str) == 8
        assert type(transfer_stop_flag)    == str
        assert len( transfer_stop_flag)    == 8
        assert type(ready_flag)            == str
        assert len( ready_flag)            == 8
        assert type(sample_format)         == int
        assert type(channels)              == int
        assert type(after_timestamp_char)  == str
        assert len( after_timestamp_char)  == 1
        assert type(data_header_end_str)   == str
        assert len( data_header_end_str)   == 2

    i = 0
    try:
        while True:
            time.sleep(random.uniform(0.1,1.5))
            print('uploading copy', i)
            the_cmd_str = 'upload ' + cmd_str[7:]
            file_name_out = client_file_transfer_c2s(sock, 
                                                     the_cmd_str, 
                                                     datagram_sz, 
                                                     random.uniform(0.05,0.2),
                                                     transfer_data_pad_str,
                                                     transfer_stop_flag,
                                                     ready_flag,
                                                     sample_format,
                                                     channels,
                                                     after_timestamp_char,
                                                     data_header_end_str
                                                    )
            if file_name_out == '  INTERRUPT  ':
                break
            print("CLIENT: Uploaded file called", file_name_out)
            i += 0
    except KeyboardInterrupt:
        print('\nCLIENT: Interrupted annoyance')


# ~~ Upload (c --> s) a scrambled file to server ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

def client_scramble( sock, 
                     cmd_str, 
                     datagram_sz,
                     n_msgs_per_pidge,
                     n_entries_per_pidges,
                     sleep_time,
                     transfer_data_pad_str = '__okay__',
                     transfer_stop_flag    = '__done__',
                     ready_flag            = '__redy__',
                     sample_format         = pyaudio.paInt16, 
                     channels              = 2,
                     after_timestamp_char  = '&',
                     data_header_end_str   = '__'
                   ):
    if DEBUGGING:
        assert type(sock)                  == socket.socket
        assert type(cmd_str)               == str
        assert type(datagram_sz)           == int
        assert type(n_msgs_per_pidge)      == int
        assert type(n_entries_per_pidges)  == int
        assert type(sleep_time)            == float
        assert type(transfer_data_pad_str) == str
        assert len( transfer_data_pad_str) == 8
        assert type(transfer_stop_flag)    == str
        assert len( transfer_stop_flag)    == 8
        assert type(ready_flag)            == str
        assert len( ready_flag)            == 8
        assert type(sample_format)         == int
        assert type(channels)              == int
        assert type(after_timestamp_char)  == str
        assert len( after_timestamp_char)  == 1
        assert type(data_header_end_str)   == str
        assert len( data_header_end_str)   == 2

    file_name_c2s = "scrambled.txt"
    file_name_s2c = "NO FILE NAME :C"
    n_datagrams   = int(cmd_str[7:])

    # -- Generate the timestamp sequence ---------------------------------------

    timestamps_c2s          = make_scrambled_timestamp_sequence( n_datagrams )
    timestamps_s2c_expected = get_expected_timestamps_s2c( timestamps_c2s,
                                                           n_msgs_per_pidge,
                                                           n_entries_per_pidges
                                                         )
    
    # -- Write scrambled file --------------------------------------------------

    stop_phrase = "__msg_end__"
    write_scrambled_file( timestamps_c2s, 
                          datagram_sz, 
                          file_name_c2s, 
                          stop_phrase,
                          sample_format,
                          channels
                        )

    # -- (c --> s) transfer of scrambled.txt with force-fed timestamps ---------

    new_cmd_str = 'upload ' + file_name_c2s
    try:
        fo_c2s = open(file_name_c2s, 'rb') # 'rb' --> mode = read only, binary

        i = 0
        # preparing first datagram
        data_header, header_sz = get_header_header_sz( transfer_data_pad_str,
                                                       timestamps_c2s[i],
                                                       sample_format,
                                                       channels,
                                                       after_timestamp_char,
                                                       data_header_end_str
                                                     )
        msg_sz   = datagram_sz - header_sz
        data_msg = fo_c2s.read(msg_sz)
        data     = data_header + data_msg
        # DONE preparing first datagram

        sock.send(bytes(new_cmd_str, 'utf-8'))

        while True: # wait for server's ack

            ack = sock.recv(datagram_sz)

            if ack[:8].decode('utf-8') == ready_flag:

                file_name_s2c = ack[8:].decode('utf-8')
                print('CLIENT: server is ready. Beginning upload')

                while len(data) > header_sz:

                    sock.send(data)
                    print('CLIENT: sent datagram ', timestamps_c2s[i])
                    time.sleep(sleep_time)

                    i += 1 # preparing new header
                    
                    if i >= len(timestamps_c2s): # prevents error on last round
                        i = len(timestamps_c2s) - 1 

                    data_header, header_sz = get_header_header_sz( 
                                            transfer_data_pad_str,
                                                timestamps_c2s[i],
                                                    sample_format,
                                                         channels,
                                             after_timestamp_char,
                                              data_header_end_str)
                    msg_sz   = datagram_sz - header_sz
                    data_msg = fo_c2s.read(msg_sz)
                    data     = data_header + data_msg
                    print('CLIENT: read datagram ', timestamps_c2s[i])

                fo_c2s.close()
                sock.send(bytes(transfer_stop_flag, 'utf-8'))
                print('CLIENT: finished upload')
                break

            else:
                print('CLIENT: ...server is out of sync with the client...')
                break

    except:
        PrintException()
        print('CLIENT: ...file not found somehow...')
        return

    # -- Assess scrambled file for correctness (THIS PROCEDURE WORKS. NO TOUCHY)

    timestamps_s2c = []
    parser_stop   = threading.Event()
    parser_thread = threading.Thread( target = schrodingers_file_parser,
                                      args   = (timestamps_s2c,
                                                parser_stop,
                                                file_name_s2c,
                                                stop_phrase
                                               )
                                    )
    parser_thread.start()
    time.sleep(0.02 * n_datagrams)
    parser_stop.set()
    parser_thread.join()
    print("CLIENT: timestamps_s2c          =", timestamps_s2c)
    print("CLIENT: timestamps_s2c_expected =", timestamps_s2c_expected)
    assert timestamps_s2c == timestamps_s2c_expected
    print("CLIENT: timestamp successful!")


# ~~ Wire audio (c --> s) for playback ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

def client_wire_c2s( sock, 
                     cmd_str,
                     datagram_sz, 
                     chunk                = 1024, 
                     sample_format        = pyaudio.paInt16, 
                     channels             = 2, # 1 --> mono, 2 --> stereo
                     fs                   = 44100,
                     data_pad_str         = '__wire__',
                     stop_flag            = '__were__',
                     after_timestamp_char = '&',
                     data_header_end_str  = '__',
                     sleep_fraction       = 0.2
                   ):
    if DEBUGGING:
        assert type(sock)                 == socket.socket
        assert type(cmd_str)              == str
        assert type(datagram_sz)          == int
        assert type(chunk)                == int
        assert type(sample_format)        == int
        assert type(channels)             == int
        assert type(fs)                   == int
        assert type(data_pad_str)         == str
        assert len( data_pad_str)         == 8
        assert type(stop_flag)            == str
        assert len( stop_flag)            == 8
        assert type(after_timestamp_char) == str
        assert len( after_timestamp_char) == 1
        assert type(data_header_end_str)  == str
        assert len( data_header_end_str)  == 2
        assert type(sleep_fraction)       == float

    sock.send( bytes(cmd_str, 'utf-8') ) # in this case it doesn't care when the
                                         # server is ready, considering that 
                                         # it's just gonna send datagrams over
                                         # anyway
    frames    = bytearray()
    pa        = pyaudio.PyAudio() 
    stream    = pa.open(format            = sample_format,
                        channels          = channels,
                        rate              = fs,
                        input             = True,
                        frames_per_buffer = chunk,
                        stream_callback   = client_callback_out(frames)
                       )
    stream.start_stream()
    print("CLIENT: Wiring audio. To stop wiring, press Ctrl+c")

    timestamp = 0
    while True:
        try:
            if len(frames) > datagram_sz: # there's enough stuff to send
                header, header_sz = get_header_header_sz(data_pad_str, 
                                                         timestamp,
                                                         sample_format,
                                                         channels,
                                                         after_timestamp_char,
                                                         data_header_end_str
                                                        )
                msg_sz = datagram_sz - header_sz
                data = header + frames[:msg_sz]
                del frames[:msg_sz]
                sock.send(data)
                timestamp += 1
            else:
                time.sleep( sleep_fraction * datagram_sz / fs ) # wait for audio

        except KeyboardInterrupt:
            sock.send( bytes(stop_flag, 'utf-8') )
            print("CLIENT: Finished wiring")
            break

        except: 
            PrintException()
            break

    stream.stop_stream()
    stream.close()
    pa.terminate()


# ~~ Compress audio (c --> s --> c) on server for client playback ~~~~~~~~~~~~~~

def client_compress_c2s( c2s_stop,
                         sock,
                         frames,
                         timestamp_out,
                         datagram_sz,
                         sample_format        = pyaudio.paInt16, 
                         channels             = 2, # 1 --> mono, 2 --> stereo
                         fs                   = 44100,
                         data_pad_str         = '__comp__',
                         stop_flag            = '__pmoc__',
                         after_timestamp_char = '&',
                         data_header_end_str  = '__',
                         sleep_fraction       = 0.2
                       ):
    if DEBUGGING:
        assert type(c2s_stop)             == threading.Event
        assert type(sock)                 == socket.socket
        assert type(frames)               == bytearray
        # frames doesn't need to be empty by this point in the script
        assert type(timestamp_out)        == int
        assert type(sample_format)        == int
        assert type(channels)             == int
        assert type(fs)                   == int
        assert type(data_pad_str)         == str
        assert len( data_pad_str)         == 8
        assert type(stop_flag)            == str
        assert len( stop_flag)            == 8
        assert type(after_timestamp_char) == str
        assert len( after_timestamp_char) == 1
        assert type(data_header_end_str)  == str
        assert len( data_header_end_str)  == 2
        assert type(sleep_fraction)       == float

    while not c2s_stop.wait(0): # ie while c2s_stop isn't set in the main thread
        if len(frames) > datagram_sz: # there's enough stuff to send
            header, header_sz = get_header_header_sz(data_pad_str, 
                                                     timestamp_out,
                                                     sample_format,
                                                     channels,
                                                     after_timestamp_char,
                                                     data_header_end_str
                                                    )
            msg_sz = datagram_sz - header_sz
            data = header + frames[:msg_sz]
            del frames[:msg_sz]
            sock.send(data)
            timestamp_out += 1

        else:
            time.sleep( sleep_fraction * datagram_sz / fs ) # wait for audio

    print("CLIENT: stopped  (c --> s) transfer thread successfully") 
    return


def client_compress_s2c_write( pidges,
                               playback_queue,
                               datagram_sz,
                               n_msgs_per_pidge
                             ):
    if DEBUGGING:
        assert type(pidges)           == list
        assert len( pidges)            > 0
        assert type(pidges[0])        == tuple     # pidges entry
        assert type(pidges[0][0])     == int       # lowest_timestamp
        assert type(pidges[0][1])     == bytearray # pidge
        assert type(playback_queue)   == bytearray
        # playback_queue can be empty
        assert type(datagram_sz)      == int
        assert type(n_msgs_per_pidge) == int

    timestamp_pidge = pidges.pop(0)
    audio    = remove_holes_from_timestamp_pidge(timestamp_pidge, 
                                                 datagram_sz,
                                                 n_msgs_per_pidge
                                                )
    playback_queue.extend(audio)


def client_compress_s2c( sock,
                         playback_queue,
                         pidges,
                         minimum_timestamp,
                         timestamp_in,
                         largest_timestamp,
                         datagram_sz,
                         n_msgs_per_pidge,
                         pidges_filled,
                         sample_format        = pyaudio.paInt16,
                         channels             = 2, # 1 --> mono, 2 --> stereo
                         stop_flag            = '__pmoc__',
                         after_timestamp_char = '&',
                         data_header_end_str  = '__'
                       ):
    if DEBUGGING:
        assert type(sock)                 == socket.socket
        assert type(playback_queue)       == bytearray
        # playback_queue can be empty
        assert type(pidges)               == list
        assert len( pidges)                > 0
        assert type(pidges[0])            == tuple     # pidges entry
        assert type(pidges[0][0])         == int       # lowest_timestamp
        assert type(pidges[0][1])         == bytearray # pidge
        assert type(minimum_timestamp)    == int
        assert type(timestamp_in)         == int
        assert type(largest_timestamp)    == int
        assert type(datagram_sz)          == int
        assert type(n_msgs_per_pidge)     == int
        assert type(pidges_filled)        == bool
        assert type(sample_format)        == int
        assert type(channels)             == int
        assert type(stop_flag)            == str
        assert len( stop_flag)            == 8
        assert type(after_timestamp_char) == str
        assert len( after_timestamp_char) == 1
        assert type(data_header_end_str)  == str
        assert len( data_header_end_str)  == 2

    datagram = sock.recv(datagram_sz)

    while datagram[:8].decode('utf-8') != stop_flag:

        header, header_sz = get_header_header_sz_from_datagram(
                                                      datagram,
                                           data_header_end_str)
        timestamp_in = get_timestamp_from_header(header,
                                                 after_timestamp_char,
                                                 data_header_end_str
                                                )

        if   timestamp_in < minimum_timestamp:
            print("CLIENT: Got timestamp lower than minimum. Ignoring.")

        else:
            while timestamp_in > largest_timestamp:
                if not pidges_filled:
                    print('\nCLIENT: Producing audio')
                    pidges_filled = True
                # i) pop oldest pidge from pidges into playback_queue
                client_compress_s2c_write(pidges,
                                          playback_queue,
                                          datagram_sz,
                                          n_msgs_per_pidge
                                         )
                # ii) add new pidge, w/ timestamp of largest_timestamp + 1
                update_pidges( pidges, 
                               largest_timestamp + 1, 
                               n_msgs_per_pidge,
                               datagram_sz,
                               sample_format, 
                               channels
                             )
                # iii) update value of largest_timestamp and minimum_timestamp
                minimum_timestamp += n_msgs_per_pidge
                largest_timestamp += n_msgs_per_pidge

            if not pidges_filled:
                print('\rCLIENT: filling up pidges:', 
                      '%.1f' % (100 * timestamp_in / largest_timestamp),
                      '%', end= ' ')

            client_file_transfer_s2c_copy( datagram_sz,
                                           timestamp_in,
                                           n_msgs_per_pidge,
                                           datagram[header_sz:],
                                           pidges,
                                           sample_format, 
                                           channels
                                         )

        datagram = sock.recv(datagram_sz)

    print("CLIENT: got stop flag. Finished (s --> c) transfer successfully")
    return


def client_compress_c2s2c_dump( pidges,
                                playback_queue,
                                n_msgs_per_pidge,
                                datagram_sz,
                                sample_format = pyaudio.paInt16, 
                                channels      = 2
                              ):
    if DEBUGGING:
        assert type(pidges)           == list
        assert len( pidges)            > 0
        assert type(pidges[0])        == tuple     # pidges entry
        assert type(pidges[0][0])     == int       # lowest_timestamp
        assert type(pidges[0][1])     == bytearray # pidge
        assert type(playback_queue)   == bytearray
        assert type(n_msgs_per_pidge) == int
        assert type(datagram_sz)      == int
        assert type(sample_format)    == int
        assert type(channels)         == int

    idx, last_nonempty_timestamp_pidge = retrieve_last_nonempty_pidges_entry(
                                                                      pidges)
    _, last_nonempty_pidge = last_nonempty_timestamp_pidge
    for i in range(idx):
        timestamp_pidge = pidges.pop(0)
        audio = remove_holes_from_timestamp_pidge(timestamp_pidge,
                                                  datagram_sz,
                                                  n_msgs_per_pidge,
                                                  sample_format,
                                                  channels
                                                 )
        playback_queue.extend(audio)

    audio = remove_holes_from_timestamp_pidge(pidges[0],
                                              datagram_sz,
                                              n_msgs_per_pidge
                                             )
    playback_queue.extend(audio)


def client_compress_c2s2c( sock, 
                           cmd_str,
                           datagram_sz, 
                           n_msgs_per_pidge,
                           n_entries_per_pidges,
                           chunk                = 1024, 
                           sample_format        = pyaudio.paInt16, 
                           channels             = 2, # 1 --> mono, 2 --> stereo
                           fs                   = 44100,
                           data_pad_str         = '__comp__',
                           stop_flag            = '__pmoc__',
                           after_timestamp_char = '&',
                           data_header_end_str  = '__',
                           sleep_fraction       = 0.2
                         ):
    if DEBUGGING:
        assert type(sock)                 == socket.socket
        assert type(cmd_str)              == str
        assert type(datagram_sz)          == int
        assert type(n_msgs_per_pidge)     == int
        assert type(n_entries_per_pidges) == int
        assert type(chunk)                == int
        assert type(sample_format)        == int
        assert type(channels)             == int
        assert type(fs)                   == int
        assert type(data_pad_str)         == str
        assert len( data_pad_str)         == 8
        assert type(stop_flag)            == str
        assert len( stop_flag)            == 8
        assert type(after_timestamp_char) == str
        assert len( after_timestamp_char) == 1
        assert type(data_header_end_str)  == str
        assert len( data_header_end_str)  == 2
        assert type(sleep_fraction)       == float

    sock.send( bytes(cmd_str, 'utf-8') ) # in this case it doesn't care when the
                                         # server is ready, considering that 
                                         # it's just gonna send datagrams over
                                         # anyway
    pa = pyaudio.PyAudio() 

    # -- 1. Prepare (c --> s) audio environment --------------------------------

    print("CLIENT: preparing microphone stream")
    timestamp_out = 0
    frames        = bytearray()
    stream_out    = pa.open(format            = sample_format,
                            channels          = channels,
                            rate              = fs,
                            input             = True,
                            frames_per_buffer = chunk,
                            stream_callback   = client_callback_out(frames)
                           )

    # -- 2. Prepare (s --> c) audio environment --------------------------------

    print("CLIENT: preparing speaker    stream ")
    playback_queue    = bytearray()
    pidges_filled     = False
    minimum_timestamp = 0
    timestamp_in      = 0
    largest_timestamp = n_msgs_per_pidge * n_entries_per_pidges - 1
    pidges            = make_zeropad_pidges( n_msgs_per_pidge,
                                             n_entries_per_pidges,
                                             datagram_sz,
                                             sample_format, 
                                             channels
                                           )
    stream_in = pa.open(format            = sample_format,
                        channels          = channels,
                        rate              = fs,
                        output            = True,
                        frames_per_buffer = chunk,
                        stream_callback   = client_callback_in( playback_queue,
                                                                chunk,
                                                                channels,
                                                                sample_format
                                                              )
                       )

    # -- 3. Run (c --> s) and (s --> c) procedures on separate threads ---------

    print("CLIENT: setting up (c --> s) transfer thread")
    c2s_stop   = threading.Event()
    c2s_thread = threading.Thread( target = client_compress_c2s,
                                   args   = (c2s_stop,
                                             sock,
                                             frames,
                                             timestamp_out,
                                             datagram_sz,
                                             sample_format, 
                                             channels,
                                             fs,
                                             data_pad_str,
                                             stop_flag,
                                             after_timestamp_char,
                                             data_header_end_str,
                                             sleep_fraction
                                            )
                                 )
    print("CLIENT: setting up (s --> c) transfer thread")
    s2c_thread = threading.Thread( target = client_compress_s2c,
                                   args   = (sock,
                                             playback_queue,
                                             pidges,
                                             minimum_timestamp,
                                             timestamp_in,
                                             largest_timestamp,
                                             datagram_sz,
                                             n_msgs_per_pidge,
                                             pidges_filled,
                                             sample_format,
                                             channels,
                                             stop_flag,
                                             after_timestamp_char,
                                             data_header_end_str
                                            ) 
                                 )

    print("CLIENT: starting   (c --> s) transfer thread")
    c2s_thread.start()
    print("CLIENT: starting   (s --> c) transfer thread")
    s2c_thread.start()

    # -- 4. Stall while the I haven't asked to finish the stream ---------------

    print("CLIENT: Compressing. Press Ctrl + c to stop compressing")
    while ( stream_out.is_active()  and 
            stream_in.is_active() ) :
        try:
            time.sleep(0.2)
        except KeyboardInterrupt:
            print("\nCLIENT: stopping (c --> s) transfer thread")
            c2s_stop.set()
            break

    # -- 5. Terminate (c --> s) environment ------------------------------------

    print("CLIENT: stopping microphone stream")
    stream_out.stop_stream()
    stream_out.close()

    sock.send( bytes(stop_flag, 'utf-8') )

    # -- 6. Wait for (s --> c) environment to receive confirmation -------------

    while s2c_thread.is_alive():
        time.sleep(0.1)

    #time.sleep(0.5)

    # -- 7. Terminate (s --> c) environment ------------------------------------

    print("CLIENT: stopping speaker stream")
    client_compress_c2s2c_dump( pidges,
                                playback_queue,
                                n_msgs_per_pidge,
                                datagram_sz,
                                sample_format,
                                channels
                              )
    stream_in.stop_stream()
    stream_in.close()

    # -- 8. Terminate stuff pertaining to both (c --> s) and (s --> c) ---------

    print("CLIENT: terminating PyAudio object and joining threads")
    pa.terminate()
    c2s_thread.join()
    s2c_thread.join()
    return
















