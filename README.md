# A4A PRRUU
![PRRUU](ap.png)

PRRUU _(**P**yaudio **R**elaying and **R**etrieval **U**sing **U**DP)_ is a service that uses Pyaudio to send clients' microphone audio to a server running on a Docker container, process it there, and then send the processed version to be played back client-side. PRRUU is written in python and uses the UDP protocol.

## Contents:
* `server_main.py` is the server per se
* `client_main.py` is the client per se
* `demo_compressor_main.py` is a demo of the compressor that the server applies
* `Dockerfile` and `startup.sh` pertain to the Docker container on which the server will run.
* `ab.png`, `ac.png`, `ap.png`, and `as.txt` are images used for diagnostic tests
* `./modules/` contains most of the functions that the client and server use to run

**Intended usage:** server running on docker container, client(s) running on host(s)

## Requirements:
* portaudio 19.6.0-1.2 or later _(available on ubuntu 22.10)_
* pyaudio
* pydub
* 1.47 GB free space

## Running:

### Running PRRUU locally
1. Open two terminal instances
2. **Terminal 1:** `$ python server_main.py`
3. **Terminal 2:** `$ python client_main.py`
4. **Terminal 2:** `127.0.0.1`
5. **Terminal 2:** `wire`
    * *this is in case you just want to hear audio be transferred from the client to the server and back*
6. **Terminal 2:** `compress`
    * *this is in case you actually want to compress the audio*

### Running compressor demo
`$ python demo_compressor_main.py`

### Running everything in the Docker container in case it doesn't run locally

Pyaudio can be a bit finnicky, so don't be surprised if you have to resort to this option.

#### Docker container -- build
Run the following in the folder containing `Dockerfile` and `startup.sh`

`$ docker build -t <IMAGE NAME GOES HERE> .`

Image takes up 1.47 GB

#### Docker container -- run
`$ docker run -it -p 8888:8888 -p 5555:5555 -v ~/A4A_container_share:/opt/shared_folder --device /dev/snd -e PULSE_SERVER=unix:${XDG_RUNTIME_DIR}/pulse/native -v ${XDG_RUNTIME_DIR}/pulse/native:${XDG_RUNTIME_DIR}/pulse/native -v ~/.config/pulse/cookie:/root/.config/pulse/cookie --group-add $(getent group audio | cut -d: -f3) -e GID=$(id -g) -e UID=$(id -u) -e USER=$USER <IMAGE NAME GOES HERE>`

* Port 8888 → jupyter
* Port 5555 → A4A server bind
* in this example, the folder ~/A4A_container_share on the host machine is connected to the folder /opt/shared_folder on the docker container. This allows to edit the python scripts on the host machine and run them on the docker container

***This command won't let you run this off of multiple machines, only a single one. That's future work***

#### Docker container -- exec
`$ docker start <CONTAINER ID GOES HERE>`

Then, to run as root:

`$ docker exec -it <CONTAINER ID GOES HERE> /bin/bash`

And to run as yourself:

`$ docker exec -it <CONTAINER ID GOES HERE> runuser -u <USERNAME GOES HERE> /bin/bash`

##### Running jupyter

As root:

`# jupyter notebook --ip=172.17.0.2 --port=8888 --allow-root --notebook-dir=/opt/shared_folder`

As user:

`$ jupyter notebook --ip=172.17.0.2 --port=8888 --allow-root --notebook-dir=/opt/shared_folder`