import sys
sys.path.append('./modules')

from debugging import DEBUGGING # <-- this checks modules/debugging.py
if DEBUGGING:
    import numbers
from server_compressor_imports import *

import socket
import subprocess
import os
import time
from datetime import datetime
import pyaudio


# ==============================================================================
# ==  NOMENCLATURE  ============================================================
# ==============================================================================

# datagram --> "packet" sent over socket with UDP. Can be a command, or can be 
#              data. In case of data, the datagram is made up of a header and 
#              of a message. The size of the datagram is fixed

# header   --> UTF-8 part of a "data"-style datagram. Structured like so:
#              __DUMMYTEXT__TIMESTAMP__. The DUMMYTEXT is some 4-character word.
#              The length of the header depends on the timestamp. The larger the
#              timestamp, the larger the size of the header, and consequently
#              the smaller the message

# message  --> The non UTF-8 part of a "data"-style datagram, which contains the
#              actual bytes data to be sent. Its size depends on the size of the
#              header

# pidge    --> bytearray storing the messages of a certain number of datagrams
#              (WHEN EXPORTING to FILE, REMEMBER TO CONVERT BACK TO BYTES OBJ)



# ==============================================================================
# ==  GLOBALS  =================================================================
# ==============================================================================

# ~~ datagram config ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

datagram_sz = 8096

transfer_data_pad_str = '__okay__'
transfer_stop_flag    = '__done__'
wire_data_pad_str     = '__wire__'
wire_stop_flag        = '__were__'
compress_data_pad_str = '__comp__'
compress_stop_flag    = '__pmoc__'
ready_flag            = '__redy__'

after_timestamp_char = '&'
data_header_end_str  = '__'


# ~~ pidge config ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

n_msgs_per_pidge      = 10  # <-- can be one of these: 2, 5, 10, 20, 50, etc...
n_entries_per_pidges  = 5   # how many pidges a "pidges" list will contain


# ~~ pyaudio config  ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

chunk         = 1024            # Each chunk will consist of 1024 samples
sample_format = pyaudio.paInt16 # 16 bits per sample
channels      = 2               # Number of audio channels
fs            = 44100           # Record at 44100 samples per second

if DEBUGGING:
    assert datagram_sz % (pyaudio.get_sample_size(sample_format)*channels) == 0


# ~~ compressor config ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

B          = 1       # number of filterbank channels (scalar)
t_a        = 0.0001  # attack time                   (seconds)
t_r        = 0.3     # release time                  (seconds)
T_C_log    = 6.5     # compression threshold         (log)
T_L_log    = 8       # limiting threshold            (log)
R_b        = 2       # ratio per se of compression   (factor)
truncate   = False
export_all = False

if DEBUGGING:
    assert type(      B)          == int
    assert type(      t_a)        == float
    assert type(      t_r)        == float
    assert isinstance(T_C_log,       numbers.Number)
    assert isinstance(T_L_log,       numbers.Number)
    assert            T_L_log      > T_C_log
    assert isinstance(R_b,           numbers.Number)
    assert            R_b         >= 0
    assert type(      truncate)   == bool
    assert type(      export_all) == bool


# ~~ socket config ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

#socket_IP   = '172.17.0.2' # <-- docker server
socket_IP   = '127.0.0.1' # to bind to all interfaces, set socket_IP = ''
socket_port = 5555


# ~~ timing config ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

normal_sleep_time = 0.1 # time in seconds that client_file_transfer_c2s 
                        # should sleep when not using "annoys <filename>"


# ==============================================================================
# ==  DIAGNOSTIC FUNCTIONS  ====================================================
# ==============================================================================

from diagnostic_functions import assert_fo_in
from diagnostic_functions import assert_fo_out
from diagnostic_functions import PrintException



# ==============================================================================
# ==  MID-LEVEL FUNCTIONS  =====================================================
# ==============================================================================

# ~~ management of uploads from several simultaneous clients ~~~~~~~~~~~~~~~~~~~

from server_mid_level_functions import retrieve_fo_out
from server_mid_level_functions import delete_addr_fo_out_entry_at_idx


# ~~ syncing uploaded data server-side --> headers ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

from server_mid_level_functions import get_header_header_sz
from server_mid_level_functions import get_header_header_sz_from_datagram
from server_mid_level_functions import get_header_sz_from_timestamp
from server_mid_level_functions import get_msg_sz_from_timestamp
from server_mid_level_functions import get_timestamp_from_header


# ~~ syncing uploaded data server-side --> pidges list management ~~~~~~~~~~~~~~

from server_mid_level_functions import make_zeropad_pidges_entry
from server_mid_level_functions import add_zeropad_pidges_addr_tuple_to_list
from server_mid_level_functions import retrieve_pidges
from server_mid_level_functions import retrieve_pidges_entry
from server_mid_level_functions import edit_pidge
from server_mid_level_functions import delete_pidges_addr_tuple_from_list


# ~~ syncing uploaded data server-side --> largest timestamp management ~~~~~~~~

from server_mid_level_functions import add_largest_timestamp_addr_tuple_to_list
from server_mid_level_functions import retrieve_largest_timestamp
from server_mid_level_functions import update_largest_timestamp
from server_mid_level_functions import delete_l_tstp_addr_sublist_from_list


# ~~ dumping pidges ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

from server_mid_level_functions import retrieve_last_nonempty_pidges_entry
from server_mid_level_functions import retrieve_data_from_last_pidge


# ~~ pyaudio -- wire audio ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

from server_mid_level_functions import server_wire_callback

from server_mid_level_functions import wire_add_stream_addr_tuple_to_list
from server_mid_level_functions import retrieve_stream
from server_mid_level_functions import start_the_stream
from server_mid_level_functions import remove_holes_from_timestamp_pidge

from server_mid_level_functions import add_filled_addr_pair_to_list
from server_mid_level_functions import retrieve_filled_addr
from server_mid_level_functions import toggle_filled
from server_mid_level_functions import these_are_filled
from server_mid_level_functions import delete_filled_addr_pair_from_list

from server_mid_level_functions import add_comp_queue_addr_tuple_to_list
from server_mid_level_functions import retrieve_compress_queue
from server_mid_level_functions import delete_queue_addr_tuple_from_list

from server_mid_level_functions import add_timestamp_addr_pair_to_list
from server_mid_level_functions import retrieve_timestamp_out_addr_pair
from server_mid_level_functions import increment_timestamp_out
from server_mid_level_functions import delete_timestamp_addr_pair_from_list

from server_mid_level_functions import add_v_last_boxed_addr_tuple_to_list
from server_mid_level_functions import retrieve_v_last_boxed
from server_mid_level_functions import delete_v_last_addr_tuple_from_list

from server_mid_level_functions import server_compress_callback
from server_mid_level_functions import comp_add_stream_addr_tuple_to_list



# ==============================================================================
# ==  HIGH-LEVEL FUNCTIONS  ====================================================
# ==============================================================================

# ~~ Print current working directory ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

from server_high_level_functions import server_pwd


# ~~ Change directory ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

from server_high_level_functions import server_cd


# ~~ List files in current working directory ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

from server_high_level_functions import server_ls


# ~~ Download file from the server onto the client ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

from server_high_level_functions import server_file_transfer_s2c


# ~~ Open file to begin transfer from client to server ~~~~~~~~~~~~~~~~~~~~~~~~~

from server_high_level_functions import server_file_transfer_c2s_open


# ~~ Pass datagram from client to server into corresponding pidge ~~~~~~~~~~~~~~

from server_high_level_functions import server_file_transfer_c2s_copy


# ~~ Write oldest pidge to file ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

from server_high_level_functions import server_file_transfer_c2s_write


# ~~ Append pidges entry filled with zeros to pidges list ~~~~~~~~~~~~~~~~~~~~~~

from server_high_level_functions import update_pidges_addr_list


# ~~ Dump nonempty pidges to file ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

from server_high_level_functions import server_file_transfer_c2s_dump


# ~~ Close uploaded (c --> s) file ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

from server_high_level_functions import server_file_transfer_c2s_close


# ~~ Wire audio (c --> s) for playback ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

from server_high_level_functions import server_wire_c2s_write


# ~~ Dump nonempty audio pidges for playback ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

from server_high_level_functions import server_wire_c2s_dump


# ~~ Close wiring (c --> s) audio ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

from server_high_level_functions import server_wire_c2s_close


# ~~ Wire audio (c --> s) for compression ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

from server_high_level_functions import server_comp_c2s_write


# ~~ Dump nonempty audio pidges for compression ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

from server_high_level_functions import server_comp_c2s_dump


# ~~ Print client address ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

from server_high_level_functions import server_hello



# ==============================================================================
# ==  PRINCIPAL FUNCTION  ======================================================
# ==============================================================================

def main( socket_IP, 
          socket_port, 
          datagram_sz, 
          n_msgs_per_pidge,
          n_entries_per_pidges,
          normal_sleep_time,
          transfer_data_pad_str,
          transfer_stop_flag,
          wire_data_pad_str,
          wire_stop_flag,
          compress_data_pad_str,
          compress_stop_flag,
          ready_flag,
          after_timestamp_char,
          data_header_end_str,
          chunk,
          sample_format,
          channels,
          fs,
          B,
          t_a,
          t_r,
          T_C_log,
          T_L_log,
          R_b,
          truncate,
          export_all
        ):
    if DEBUGGING:
        assert type(      socket_IP)             == str
        assert len(       socket_IP)              > 0
        assert type(      socket_port)           == int
        assert type(      datagram_sz)           == int
        assert type(      n_msgs_per_pidge)      == int
        assert type(      n_entries_per_pidges)  == int
        assert type(      normal_sleep_time)     == float
        assert type(      transfer_data_pad_str) == str
        assert len(       transfer_data_pad_str) == 8
        assert type(      transfer_stop_flag)    == str
        assert len(       transfer_stop_flag)    == 8
        assert type(      wire_data_pad_str)     == str
        assert len(       wire_data_pad_str)     == 8
        assert type(      wire_stop_flag)        == str
        assert len(       wire_stop_flag)        == 8
        assert type(      compress_data_pad_str) == str
        assert len(       compress_data_pad_str) == 8
        assert type(      compress_stop_flag)    == str
        assert len(       compress_stop_flag)    == 8
        assert type(      ready_flag)            == str
        assert len(       ready_flag)            == 8
        assert type(      chunk)                 == int
        assert type(      after_timestamp_char)  == str
        assert len(       after_timestamp_char)  == 1
        assert type(      data_header_end_str)   == str
        assert len(       data_header_end_str)   == 2
        assert type(      sample_format)         == int
        assert type(      channels)              == int
        assert type(      fs)                    == int
        assert type(      B)                     == int
        assert type(      t_a)                   == float
        assert type(      t_r)                   == float
        assert isinstance(T_C_log,                  numbers.Number)
        assert isinstance(T_L_log,                  numbers.Number)
        assert            T_L_log                 > T_C_log
        assert isinstance(R_b,                      numbers.Number)
        assert            R_b                    >= 0
        assert type(      truncate)              == bool
        assert type(      export_all)            == bool


    # -- Binding socket --------------------------------------------------------

    sock = socket.socket(socket.AF_INET,    # Ipv4 address family
                         socket.SOCK_DGRAM) # datagram socket type (UDP)
    sock.bind( (socket_IP, socket_port) )


    # -- Preparing "environment" for file uploads ------------------------------

    fo_out_addr_list   = [] # list of (fo_out, addr) tuples
    pidges_addr_list   = [] # list of (pidges, addr) tuples
    largest_timestamps = [] # list of [largest_timestamp, addr] pairs


    # -- Preparing "environment" for audio processing --------------------------

    playback_queue           = bytearray()       # wiring --> 1 client, 1 server
    compress_queue_addr_list = []                # list of (queue,  addr) tuples
    timestamp_out_addr_list  = []         # list of [timestamp_out, addr] pairs
    pa                       = pyaudio.PyAudio()
    stream_addr_list         = []                # list of (stream, addr) tuples
    pidges_filled            = False
    filled_addr_list         = []         # list of [pidges_filled, addr] pairs


    # -- Preparing "environment" for compressor --------------------------------

    v_last_boxed_addr_list = [] # list of ([v_last], addr) pairs


    # -- Receiving command and acting on it ------------------------------------

    print('SERVER: Ready for clients')

    while True:

        datagram, addr = sock.recvfrom(datagram_sz) # addr --> client address
        
        # - Check for 2 subfamilies of options:
        # -   
        # -     1. Data --> utf-8 header + non-utf-8 message
        # -         1.1. Upload --> Write datagram into pidge / pidge into file
        # -         1.2. Upload --> Finish up
        # -         1.3. Wire --> Write audio into pidge / pidge into queue
        # -         1.4. Wire --> Finish up
        # -         1.5. Compress --> Write audio into pidge / pidge into queue
        # -         1.6. Compress --> Finish up
        # -         1.7. Smth went wrong
        # -   
        # -     2. Command --> Only contains utf-8 data
        # -         2.1. Quit server
        # -         2.2. Print current working directory
        # -         2.3. Change directory
        # -         2.4. List files in curr directory
        # -         2.5. Download file
        # -         2.6. Open file for upload
        # -         2.7. Wire audio from client to server
        # -         2.8. Compress client audio and resend
        # -         2.9. Print client address
        # -         2.10. Invalid command syntax

        if datagram[:2].decode('utf-8') == '__': # 1. Data: utf8 hdr + bytes msg



            if   datagram[:8].decode('utf-8') == transfer_data_pad_str:

                # 1.1. Upload --> Write datagram into pidge / pidge into file
                header, header_sz = get_header_header_sz_from_datagram(
                                                              datagram,
                                                   data_header_end_str)
                
                print('SERVER: #', addr[1], 'sends me', 
                      datagram_sz - header_sz, 'bytes of data')

                timestamp = get_timestamp_from_header( header,
                                                       after_timestamp_char,
                                                       data_header_end_str
                                                     )
                _, largest_timestamp = retrieve_largest_timestamp(
                                               largest_timestamps,
                                                             addr)

                while timestamp > largest_timestamp: 
                    # i)   pop oldest pidge from list into file
                    server_file_transfer_c2s_write(pidges_addr_list,
                                                   fo_out_addr_list,
                                                   addr,
                                                   datagram_sz,
                                                   n_msgs_per_pidge,
                                                   sample_format, 
                                                   channels
                                                  )
                    # ii)  add new pidge, w/ timestamp of largest_timestamp + 1
                    update_pidges_addr_list(pidges_addr_list,
                                            largest_timestamp + 1,
                                            n_msgs_per_pidge,
                                            datagram_sz,
                                            addr,
                                            sample_format,
                                            channels
                                           )
                    # iii) update value of largest_timestamp
                    new_largest_timestamp = largest_timestamp + n_msgs_per_pidge
                    largest_timestamp     = new_largest_timestamp
                    largest_timestamps    = update_largest_timestamp( 
                                               new_largest_timestamp, 
                                                                addr, 
                                                  largest_timestamps)

                server_file_transfer_c2s_copy(addr,
                                              datagram_sz,
                                              timestamp,  
                                              n_msgs_per_pidge,   
                                              datagram[header_sz:], 
                                              pidges_addr_list,
                                              sample_format,
                                              channels
                                             )



            elif datagram[:8].decode('utf-8') == transfer_stop_flag:

                # 1.2. Upload --> Finish up
                server_file_transfer_c2s_dump(pidges_addr_list,
                                              fo_out_addr_list,
                                              n_msgs_per_pidge,
                                              datagram_sz,
                                              addr,
                                              sample_format,
                                              channels
                                             )
                fo_out_addr_list   = server_file_transfer_c2s_close(addr, 
                                                        fo_out_addr_list)
                pidges_addr_list   = delete_pidges_addr_tuple_from_list(
                                                       pidges_addr_list, 
                                                                   addr)
                largest_timestamps = delete_l_tstp_addr_sublist_from_list(
                                                       largest_timestamps,
                                                                     addr)
                print('SERVER: #', addr[1], "'s upload successful!!!")



            elif datagram[:8].decode('utf-8') == wire_data_pad_str:

                # 1.3. Wire --> Write audio into pidge / pidge into queue
                header, header_sz = get_header_header_sz_from_datagram(
                                                              datagram,
                                                   data_header_end_str)
                timestamp = get_timestamp_from_header( header,
                                                       after_timestamp_char,
                                                       data_header_end_str
                                                     )
                _, largest_timestamp = retrieve_largest_timestamp(
                                               largest_timestamps,
                                                             addr)
                while timestamp > largest_timestamp: 
                    if not pidges_filled:
                        print('\nSERVER: Wiring audio')
                        pidges_filled = True
                    # i)   pop oldest pidge from list into playback_queue
                    server_wire_c2s_write(pidges_addr_list,
                                          playback_queue,
                                          addr,
                                          datagram_sz,
                                          n_msgs_per_pidge,
                                          sample_format,
                                          channels
                                         )
                    # ii)  add new pidge, w/ timestamp of largest_timestamp + 1
                    update_pidges_addr_list(pidges_addr_list,
                                            largest_timestamp + 1,
                                            n_msgs_per_pidge,
                                            datagram_sz,
                                            addr,
                                            sample_format,
                                            channels
                                           )
                    # iii) update value of largest_timestamp
                    new_largest_timestamp = largest_timestamp + n_msgs_per_pidge
                    largest_timestamp     = new_largest_timestamp
                    largest_timestamps    = update_largest_timestamp( 
                                               new_largest_timestamp, 
                                                                addr, 
                                                  largest_timestamps)

                if not pidges_filled:
                    print('\rSERVER: filling up pidges:', 
                          '%.1f' % (100 * timestamp / largest_timestamp),
                          '%', end= ' ')

                server_file_transfer_c2s_copy(addr,
                                              datagram_sz,
                                              timestamp,  
                                              n_msgs_per_pidge,   
                                              datagram[header_sz:], 
                                              pidges_addr_list,
                                              sample_format,
                                              channels
                                             )



            elif datagram[:8].decode('utf-8') == wire_stop_flag:

                # 1.4. Wire --> Finish up
                server_wire_c2s_dump(pidges_addr_list,
                                     stream_addr_list,
                                     playback_queue,
                                     n_msgs_per_pidge,
                                     datagram_sz,
                                     addr,
                                     sample_format,
                                     channels
                                    )
                stream_addr_list   = server_wire_c2s_close(addr, 
                                                           stream_addr_list
                                                          )
                pidges_addr_list   = delete_pidges_addr_tuple_from_list(
                                                       pidges_addr_list, 
                                                                   addr)
                largest_timestamps = delete_l_tstp_addr_sublist_from_list(
                                                       largest_timestamps,
                                                                     addr)
                playback_queue = bytearray() # reset playback_queue
                pidges_filled  = False
                print('SERVER: Finished wiring audio from client')



            elif datagram[:8].decode('utf-8') == compress_data_pad_str:

                # 1.5. Compress --> Write audio into pidge / pidge into queue
                header, header_sz = get_header_header_sz_from_datagram(
                                                              datagram,
                                                   data_header_end_str)
                timestamp = get_timestamp_from_header( header,
                                                       after_timestamp_char,
                                                       data_header_end_str
                                                     )
                _, largest_timestamp = retrieve_largest_timestamp(
                                               largest_timestamps,
                                                             addr)
                while timestamp > largest_timestamp: 
                    if not these_are_filled(filled_addr_list, addr):
                        print('\nSERVER: Compressing audio')
                        toggle_filled(filled_addr_list, addr)
                    # i)   pop oldest pidge from list into playback_queue
                    server_comp_c2s_write(pidges_addr_list,
                                          compress_queue_addr_list,
                                          addr,
                                          datagram_sz,
                                          n_msgs_per_pidge
                                         )
                    # ii)  add new pidge, w/ timestamp of largest_timestamp + 1
                    update_pidges_addr_list(pidges_addr_list,
                                            largest_timestamp + 1,
                                            n_msgs_per_pidge,
                                            datagram_sz,
                                            addr,
                                            sample_format,
                                            channels
                                           )
                    # iii) update value of largest_timestamp
                    new_largest_timestamp = largest_timestamp + n_msgs_per_pidge
                    largest_timestamp     = new_largest_timestamp
                    largest_timestamps    = update_largest_timestamp( 
                                               new_largest_timestamp, 
                                                                addr, 
                                                  largest_timestamps)

                if not these_are_filled(filled_addr_list, addr):
                    print('\rSERVER: filling up pidges:', 
                          '%.1f' % (100 * timestamp / largest_timestamp),
                          '%', end= ' ')

                server_file_transfer_c2s_copy(addr,
                                              datagram_sz,
                                              timestamp,  
                                              n_msgs_per_pidge,   
                                              datagram[header_sz:], 
                                              pidges_addr_list,
                                              sample_format,
                                              channels
                                             )



            elif datagram[:8].decode('utf-8') == compress_stop_flag:

                # 1.6. Compress --> Finish up
                server_comp_c2s_dump(pidges_addr_list,
                                     stream_addr_list,
                                     compress_queue_addr_list,
                                     n_msgs_per_pidge,
                                     datagram_sz,
                                     addr
                                    )
                stream_addr_list   = server_wire_c2s_close(addr, 
                                                           stream_addr_list
                                                          )
                pidges_addr_list   = delete_pidges_addr_tuple_from_list(
                                                       pidges_addr_list, 
                                                                   addr)
                largest_timestamps = delete_l_tstp_addr_sublist_from_list(
                                                       largest_timestamps,
                                                                     addr)
                v_last_boxed_addr_list = delete_v_last_addr_tuple_from_list(
                                                     v_last_boxed_addr_list,
                                                                       addr)
                compress_queue_addr_list = delete_queue_addr_tuple_from_list(
                                                    compress_queue_addr_list,
                                                                        addr)
                timestamp_out_addr_list = delete_timestamp_addr_pair_from_list(
                                                       timestamp_out_addr_list,
                                                                          addr)
                filled_addr_list = delete_filled_addr_pair_from_list(
                                                    filled_addr_list,
                                                                addr)
                sock.sendto(bytes(compress_stop_flag, 'utf-8'), addr)
                print('SERVER: Finished compressing audio from client')

            else:
                print("SERVER: smth went wrong interpreting non utf-8 data")
                pass

        else: # 2. Command --> Does not contain non-utf-8 data

            cmd_str = datagram.decode('utf-8')

            if  (cmd_str == 'quit' or 
                 cmd_str == 'exit' ):

                # 2.1. Quit server
                break # --> quits server

            elif cmd_str == 'pwd':

                # 2.2. Print current working directory
                server_pwd(sock, addr)

            elif (cmd_str[:2]  == 'cd' and 
                  cmd_str[2:3] == ' '  and 
                  cmd_str[3:]  != '') :

                # 2.3. Change directory
                server_cd(sock, addr, cmd_str)

            elif cmd_str == 'ls' :

                # 2.4. List files in curr directory
                server_ls(sock, addr)

            elif (cmd_str[:8]  == 'download' and 
                  cmd_str[8:9] == ' '        and 
                  cmd_str[9:]  != '') :

                # 2.5. Download file
                server_file_transfer_s2c(sock, 
                                         addr, 
                                         cmd_str, 
                                         datagram_sz,
                                         normal_sleep_time,
                                         transfer_data_pad_str,
                                         sample_format,
                                         channels,
                                         after_timestamp_char,
                                         data_header_end_str
                                        )

            elif (cmd_str[:6]  == 'upload' and 
                  cmd_str[6:7] == ' '      and 
                  cmd_str[7:]  != '') :

                # 2.6. Open file for upload
                fo_out_addr_list = server_file_transfer_c2s_open( sock,
                                                                  addr,
                                                               cmd_str,
                                                      fo_out_addr_list,
                                                            ready_flag)
                pidges_addr_list = add_zeropad_pidges_addr_tuple_to_list(
                                                        n_msgs_per_pidge,
                                                    n_entries_per_pidges,
                                                             datagram_sz,
                                                                    addr,
                                                        pidges_addr_list)
                largest_timestamps = add_largest_timestamp_addr_tuple_to_list(
                                                             n_msgs_per_pidge, 
                                                         n_entries_per_pidges,
                                                                         addr,
                                                           largest_timestamps)

            elif cmd_str == 'wire':

                # 2.7. Wire audio from client to server
                stream_addr_list = wire_add_stream_addr_tuple_to_list(
                                                     stream_addr_list,
                                                                 addr,
                                                                   pa,
                                                       playback_queue,
                                                                chunk,
                                                        sample_format,
                                                             channels,
                                                                   fs)
                pidges_addr_list = add_zeropad_pidges_addr_tuple_to_list(
                                                        n_msgs_per_pidge,
                                                    n_entries_per_pidges,
                                                             datagram_sz,
                                                                    addr,
                                                        pidges_addr_list)
                largest_timestamps = add_largest_timestamp_addr_tuple_to_list(
                                                             n_msgs_per_pidge, 
                                                         n_entries_per_pidges,
                                                                         addr,
                                                           largest_timestamps)
                start_the_stream(stream_addr_list, addr)

            elif cmd_str == 'compress':

                # 2.8. Compress client audio and resend
                filled_addr_list = add_filled_addr_pair_to_list(
                                               filled_addr_list,
                                                           addr)
                compress_queue_addr_list = add_comp_queue_addr_tuple_to_list(
                                                    compress_queue_addr_list,
                                                                        addr)
                v_last_boxed_addr_list = add_v_last_boxed_addr_tuple_to_list(
                                                      v_last_boxed_addr_list,
                                                                        addr)
                timestamp_out_addr_list = add_timestamp_addr_pair_to_list(
                                                  timestamp_out_addr_list,
                                                                     addr)
                stream_addr_list = comp_add_stream_addr_tuple_to_list(
                                                     stream_addr_list,
                                             compress_queue_addr_list,
                                              timestamp_out_addr_list,
                                               v_last_boxed_addr_list,
                                                                 sock,
                                                                 addr,
                                                          datagram_sz,
                                                                   pa,
                                                                chunk,
                                                        sample_format,
                                                             channels,
                                                                   fs,
                                                                    B,
                                                                  t_a,  
                                                                  t_r,     
                                                              T_C_log,       
                                                              T_L_log,    
                                                                  R_b,
                                                compress_data_pad_str,
                                                 after_timestamp_char,
                                                  data_header_end_str)
                pidges_addr_list = add_zeropad_pidges_addr_tuple_to_list(
                                                        n_msgs_per_pidge,
                                                    n_entries_per_pidges,
                                                             datagram_sz,
                                                                    addr,
                                                        pidges_addr_list)
                largest_timestamps = add_largest_timestamp_addr_tuple_to_list(
                                                             n_msgs_per_pidge, 
                                                         n_entries_per_pidges,
                                                                         addr,
                                                           largest_timestamps)
                start_the_stream(stream_addr_list, addr)

            elif cmd_str == 'hello':

                # 2.9. Print client address
                server_hello(sock, addr)

            else:

                # 2.10. Invalid command syntax
                print('SERVER: invalid command syntax')
                pass

    # -- Terminating pyaudio session -------------------------------------------

    pa.terminate()



# ==============================================================================
# ==  SCRIPT PROCEDURE  ========================================================
# ==============================================================================

main( socket_IP, 
      socket_port, 
      datagram_sz, 
      n_msgs_per_pidge,
      n_entries_per_pidges,
      normal_sleep_time,
      transfer_data_pad_str,
      transfer_stop_flag,
      wire_data_pad_str,
      wire_stop_flag,
      compress_data_pad_str,
      compress_stop_flag,
      ready_flag,
      after_timestamp_char,
      data_header_end_str,
      chunk,
      sample_format,
      channels,
      fs,
      B,
      t_a,
      t_r,
      T_C_log,
      T_L_log,
      R_b,
      truncate,
      export_all
    )
