import sys
sys.path.append('./modules')

from debugging import DEBUGGING # <-- this checks modules/debugging.py

import socket
import time
import random
import pyaudio



# ==============================================================================
# ==  NOMENCLATURE  ============================================================
# ==============================================================================

# datagram --> "packet" sent over socket with UDP. Can be a command, or can be 
#              data. In case of data, the datagram is made up of a header and 
#              of a message. The size of the datagram is fixed

# header   --> UTF-8 part of a "data"-style datagram. Structured like so:
#              __DUMMYTEXT__TIMESTAMP__. The DUMMYTEXT is some 4-character word.
#              The length of the header depends on the timestamp. The larger the
#              timestamp, the larger the size of the header, and consequently
#              the smaller the message

# message  --> The non UTF-8 part of a "data"-style datagram, which contains the
#              actual bytes data to be sent. Its size depends on the size of the
#              header

# pidge    --> bytearray storing the messages of a certain number of datagrams
#              (WHEN EXPORTING to FILE, REMEMBER TO CONVERT BACK TO BYTES OBJ)



# ==============================================================================
# ==  GLOBALS  =================================================================
# ==============================================================================

# ~~ datagram config ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

datagram_sz = 8096

transfer_data_pad_str = '__okay__'
transfer_stop_flag    = '__done__'
wire_data_pad_str     = '__wire__'
wire_stop_flag        = '__were__'
compress_data_pad_str = '__comp__'
compress_stop_flag    = '__pmoc__'
ready_flag            = '__redy__'

after_timestamp_char = '&'
data_header_end_str  = '__'


# ~~ pidge config ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

n_msgs_per_pidge      = 10 # <-- can be one of these: 2, 5, 10, 20, 50, etc.
n_entries_per_pidges  = 5  # <-- how many pidges a "pidges" list will contain


# ~~ pyaudio config  ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

chunk         = 1024            # Each chunk will consist of 1024 samples
sample_format = pyaudio.paInt16 # 16 bits per sample
channels      = 2               # Number of audio channels
fs            = 44100           # Record at 44100 samples per second

if DEBUGGING:
    assert datagram_sz % (pyaudio.get_sample_size(sample_format)*channels) == 0


# ~~ socket config ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

print('CLIENT: Enter the client IP address i.e. 192.168.1.102')
print('If server is your Local computer then you can enter 127.0.0.1')
print('For local docker server --> try 172.17.0.2')
socket_IP   = input("CLIENT: Server IP address: ") # input() == Py2 raw_input()
socket_port = 5555


# ~~ timing config  ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

normal_sleep_time = 0.1 # time in seconds that client_file_transfer_c2s should
                        # sleep when not using "annoys <filename>"


# ==============================================================================
# ==  DIAGNOSTIC FUNCTIONS  ====================================================
# ==============================================================================

from diagnostic_functions import assert_fo_in
from diagnostic_functions import assert_fo_out
from diagnostic_functions import PrintException



# ==============================================================================
# ==  MID-LEVEL FUNCTIONS  =====================================================
# ==============================================================================

from client_mid_level_functions import get_header_header_sz
from client_mid_level_functions import get_header_header_sz_from_datagram
from client_mid_level_functions import get_header_sz_from_timestamp
from client_mid_level_functions import get_msg_sz_from_timestamp
from client_mid_level_functions import get_timestamp_from_header


# ~~ downloading ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

from client_mid_level_functions import retrieve_pidges_entry
from client_mid_level_functions import make_zeropad_pidges_entry
from client_mid_level_functions import make_zeropad_pidges
from client_mid_level_functions import update_pidges
from client_mid_level_functions import edit_pidge


# ~~ dumping pidges ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

from client_mid_level_functions import retrieve_last_nonempty_pidges_entry
from client_mid_level_functions import retrieve_data_from_last_pidge


# ~~ scrambling ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

from client_mid_level_functions import make_scrambled_timestamp_sequence
from client_mid_level_functions import get_expected_timestamps_s2c
from client_mid_level_functions import write_msg_line_for_scrambled_file
from client_mid_level_functions import write_msg_for_scrambled_file
from client_mid_level_functions import write_scrambled_file
from client_mid_level_functions import schrodingers_file_parser


# ~~ pyaudio ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

from client_mid_level_functions import client_callback_out
from client_mid_level_functions import client_callback_in
from client_mid_level_functions import remove_holes_from_timestamp_pidge



# ==============================================================================
# ==  HIGH-LEVEL FUNCTIONS  ====================================================
# ==============================================================================

# ~~ Navigate the filesystem ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

from client_high_level_functions import client_ls_pwd_cd


# ~~ Download file from the server onto the client ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

from client_high_level_functions import client_file_transfer_s2c_write
from client_high_level_functions import client_file_transfer_s2c_copy
from client_high_level_functions import client_file_transfer_s2c_dump
from client_high_level_functions import client_file_transfer_s2c


# ~~ Upload file from the client onto the server ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

from client_high_level_functions import client_file_transfer_c2s


# ~~ Upload (c --> s) the same file several times at random intervals ~~~~~~~~~~

from client_high_level_functions import client_annoy


# ~~ Upload (c --> s) a scrambled file to server ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

from client_high_level_functions import client_scramble


# ~~ Wire audio (c --> s) for playback ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

from client_high_level_functions import client_wire_c2s

# ~~ Compress audio (c --> s --> c) on server for client playback ~~~~~~~~~~~~~~

from client_high_level_functions import client_compress_c2s
from client_high_level_functions import client_compress_s2c
from client_high_level_functions import client_compress_c2s2c_dump
from client_high_level_functions import client_compress_c2s2c



# ==============================================================================
# ==  PRINCIPAL FUNCTION  ======================================================
# ==============================================================================

def main( socket_IP, 
          socket_port,
          datagram_sz,
          n_msgs_per_pidge, 
          n_entries_per_pidges,
          normal_sleep_time,
          transfer_data_pad_str,
          transfer_stop_flag,
          wire_data_pad_str,
          wire_stop_flag,
          compress_data_pad_str,
          compress_stop_flag,
          ready_flag,
          after_timestamp_char,
          data_header_end_str,
          chunk,
          sample_format,
          channels,
          fs
        ):
    if DEBUGGING:
        assert type(socket_IP)             == str
        assert len( socket_IP)              > 0
        assert type(socket_port)           == int
        assert type(datagram_sz)           == int
        assert type(n_msgs_per_pidge)      == int
        assert type(n_entries_per_pidges)  == int
        assert type(normal_sleep_time)     == float
        assert type(transfer_data_pad_str) == str
        assert len( transfer_data_pad_str) == 8
        assert type(transfer_stop_flag)    == str
        assert len( transfer_stop_flag)    == 8
        assert type(wire_data_pad_str)     == str
        assert len( wire_data_pad_str)     == 8
        assert type(wire_stop_flag)        == str
        assert len( wire_stop_flag)        == 8
        assert type(compress_data_pad_str) == str
        assert len( compress_data_pad_str) == 8
        assert type(compress_stop_flag)    == str
        assert len( compress_stop_flag)    == 8
        assert type(ready_flag)            == str
        assert len( ready_flag)            == 8
        assert type(after_timestamp_char)  == str
        assert len( after_timestamp_char)  == 1
        assert type(data_header_end_str)   == str
        assert len( data_header_end_str)   == 2
        assert type(chunk)                 == int
        assert type(sample_format)         == int
        assert type(channels)              == int
        assert type(fs)                    == int


    # -- Socket setup ----------------------------------------------------------

    sock = socket.socket(socket.AF_INET,     # Ipv4 address family
                         socket.SOCK_DGRAM)  # datagram socket type (UDP)


    # -- Obtaining IP address and connect to socket ----------------------------

    wrong_IP_count = 0

    while True:

        try:
            sock.connect( (socket_IP, socket_port) )
            break

        except:
            if wrong_IP_count < 2:
                socket_IP = input("CLIENT: Enter a valid IP address: ")
            elif wrong_IP_count == 3:
                socket_IP = input("CLIENT: Enter a -> valid <- IP address: ")
            elif wrong_IP_count == 4:
                socket_IP = input("CLIENT: Enter VALID IP address please: ")
            elif wrong_IP_count == 5:
                socket_IP = input("CLIENT: Instructions are above: ")
            elif wrong_IP_count == 6:
                socket_IP = input("CLIENT: Valid. IP. Address. Please: ")
            elif wrong_IP_count == 7:
                socket_IP = input("CLIENT: You can't be serious: ")
            elif wrong_IP_count > 8:
                socket_IP = input("CLIENT: Just write some IP address please: ")

            wrong_IP_count += 1


    # -- Selecting command -----------------------------------------------------

    sock.send( bytes('hello', 'utf-8') )
    hello = sock.recv(datagram_sz)
    print(hello.decode('utf-8'))

    help = ("CLIENT: Enter:\n"                                             + 
            "\t'ls' for list\n"                                            + 
            "\t'pwd' for current working directory\n"                      + 
            "\t'cd <directory name>' to change directory\n"                + 
            "\t'download <file name>' to download a file\n"                + 
            "\t'upload <file name>' to upload a file\n"                    + 
            "\t'annoys <file name>' to repeatedly upload a file\n"         + 
            "\t'scrmbl <num>' uploads scrambled file w/ <num> datagrams\n" +
            "\t'wire' to play back client audio on the server \n"          +
            "\t'compress' --> server compresses, client plays \n"          +
            "\t'quit' or 'exit' to Exit")
    print(help)

    while True:

        cmd_str = input('CLIENT: Enter a command: ')

        # - Check for 9 options:
        # -   
        # -     1. Quit server and client
        # -     2. Navigate filesystem
        # -         2a. list files in curr. dir
        # -         2b. current working directory
        # -         2c. change directory
        # -     3. Download file
        # -     4. Upload file
        # -     5. Upload the same file several times at random intervals
        # -     6. Upload a scrambled file to server
        # -     7. Wire audio from client to server for playback
        # -     8. Compress audio on server to play back on client
        # -     9. Invalid command syntax

        if  (cmd_str == 'quit' or 
             cmd_str == 'exit'):

            # 1. Quit server and client
            sock.send( bytes('quit', 'utf-8') ) # --> causes server to quit
            break                               # --> quits client

        elif ( cmd_str == 'ls'  or        # 2a. list files in curr. dir
               cmd_str == 'pwd' or        # 2b. current working directory
               ( cmd_str[:2]  == 'cd' and # 2c. change directory
                 cmd_str[2:3] == ' '  and 
                 cmd_str[3:]  != '' )
             ):

            # 2. Navigate the filesystem
            sock.send( bytes(cmd_str, 'utf-8') )
            client_ls_pwd_cd(sock, datagram_sz)

        elif ( cmd_str[:8]  == 'download' and 
               cmd_str[8:9] == ' '        and 
               cmd_str[9:]  != '' ):

            # 3. Download file
            sock.send( bytes(cmd_str, 'utf-8') )
            client_file_transfer_s2c(sock, 
                                     cmd_str, 
                                     datagram_sz,
                                     n_msgs_per_pidge, 
                                     n_entries_per_pidges,
                                     sample_format, 
                                     channels,
                                     after_timestamp_char,
                                     data_header_end_str
                                    )

        elif ( cmd_str[:6]  == 'upload' and 
               cmd_str[6:7] == ' '      and 
               cmd_str[7:]  != '' ):

            # 4. Upload file
            file_name_out = client_file_transfer_c2s(sock, 
                                                     cmd_str, 
                                                     datagram_sz, 
                                                     normal_sleep_time,
                                                     transfer_data_pad_str,
                                                     transfer_stop_flag,
                                                     ready_flag,
                                                     sample_format,
                                                     channels,
                                                     after_timestamp_char,
                                                     data_header_end_str
                                                    )
            print("CLIENT: Uploaded file called", file_name_out)

        elif ( cmd_str[:6]  == 'annoys' and 
               cmd_str[6:7] == ' '      and 
               cmd_str[7:]  != '' ):

            # 5. Upload the same file several times at random intervals
            client_annoy(sock, 
                         cmd_str, 
                         datagram_sz,
                         transfer_data_pad_str,
                         transfer_stop_flag,
                         ready_flag,
                         sample_format,
                         channels,
                         after_timestamp_char,
                         data_header_end_str
                        )

        elif ( cmd_str[:6]  == 'scrmbl' and 
               cmd_str[6:7] == ' '      and 
               cmd_str[7:]  != '' ):

            # 6. Upload a scrambled file to server
            client_scramble(sock, 
                            cmd_str, 
                            datagram_sz, 
                            n_msgs_per_pidge,
                            n_entries_per_pidges,
                            normal_sleep_time,
                            transfer_data_pad_str,
                            transfer_stop_flag,
                            ready_flag,
                            sample_format,
                            channels,
                            after_timestamp_char,
                            data_header_end_str
                           )

        elif ( cmd_str == 'wire' ):

            # 7. Wire audio from client to server for playback
            client_wire_c2s(sock, 
                            cmd_str,
                            datagram_sz, 
                            chunk, 
                            sample_format, 
                            channels, 
                            fs,
                            wire_data_pad_str,
                            wire_stop_flag,
                            after_timestamp_char,
                            data_header_end_str,
                            sleep_fraction = 0.2
                           )

        elif ( cmd_str == 'compress' ):

            # 8. Compress audio on server to play back on client
            client_compress_c2s2c(sock, 
                                  cmd_str,
                                  datagram_sz, 
                                  n_msgs_per_pidge,
                                  n_entries_per_pidges,
                                  chunk, 
                                  sample_format, 
                                  channels, 
                                  fs,
                                  compress_data_pad_str,
                                  compress_stop_flag,
                                  after_timestamp_char,
                                  data_header_end_str,
                                  sleep_fraction = 0.2
                                 )

        else:
            # 9. Invalid command syntax
            print(help)


# ==============================================================================
# ==  SCRIPT PROCEDURE  ========================================================
# ==============================================================================

main( socket_IP, 
      socket_port,
      datagram_sz,
      n_msgs_per_pidge, 
      n_entries_per_pidges,
      normal_sleep_time,
      transfer_data_pad_str,
      transfer_stop_flag,
      wire_data_pad_str,
      wire_stop_flag,
      compress_data_pad_str,
      compress_stop_flag,
      ready_flag,
      after_timestamp_char,
      data_header_end_str,
      chunk,
      sample_format,
      channels,
      fs
    )
