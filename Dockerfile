# FROM ubuntu:22.04 <-- this version still uses libportaudio 19.6.0-1.1
FROM ubuntu:22.10

# Update all packages
RUN apt-get -y update && apt-get -y upgrade && apt-get -y dist-upgrade

# Install packages
# Python
RUN apt-get install -y software-properties-common && apt-get -y update
RUN echo "alias python=python3" >> /etc/bash.bashrc
RUN apt-get install -y python3-pip

# ALSA
RUN apt-get -y update && apt-get -y install alsa-base libasound2-dev alsa-utils

# Pulseaudio
RUN apt-get -y update && apt-get -y install pulseaudio

# Portaudio
RUN apt-get -y update && apt-get -y install portaudio19-dev libportaudio2 libportaudiocpp0

# ffmpeg
RUN apt-get -y update && apt-get -y install ffmpeg

# install pyaudio
RUN pip install --upgrade pip && pip install pyaudio

# install some python amenities
RUN pip install numpy matplotlib jupyter

# install pydub
RUN pip install pydub

# install net-tools
RUN apt-get -y update && apt-get -y install net-tools

COPY startup.sh /root/
ENTRYPOINT ["/root/startup.sh"]